Silicon Coppélia
================

Silicon Coppélia is a software for modeling affective decision making.

## Usage

The software is implemented as a model based on the
[Ptolemy](http://ptolemy.eecs.berkeley.edu/) framework.

From the command line the embedded Ptolemy graphical user interface can be
started with the command

`./gradlew run`

from the repository root. The model is contained in the 'sico/examples/' folder and
can be loaded via the 'File' menu in the user interface.

Ptolemy provides further possibilities to execute the model, please refer to the
[Ptolemy documentation](http://ptolemy.eecs.berkeley.edu/ptolemyII/ptIIfaq.htm#invoking%20Ptolemy%20II?)
for further details. Note that in any case Java actors need to be provided to
the Ptolemy classpath (see also the Building section).

In addition to the complete model the software provides actors that implement the
different components of the model. These can be found in the 'Silicon Coppélia'
menu point in the graph editor and can be used as building blocks for new
(graphical) models in Ptolemy.

The provided model can be edited in the graphical user interface of Ptolemy.
Some parts of the model that are defined in Java code are not directly editable.
To implement changes in these parts, they need to be copied first. This can be
accomplished by adding a new 'Composite actor' (from the Utilities menu) and
copying the content that needs to be edited into this new actor. Alternatively,
the actor to be edited can be exported as a design pattern into a new xml file.


## Documentation

Documentation of the Silicon Coppélia model can be found in the doc/ folder.

The software provides Javadoc that is also available in the graphical editor
via the Ptolemy documentation system.


## Building

The software can be built from the command line using Gradle.

A self contained version can be built using the

`./gradlew distZip`

command. This provides a zip archive including an embedded version of Ptolemy
and the Silicon Coppélia model. The archive can be extracted in any folder and
can be run with the included startup scripts by specifying the custom
'configs/configuration.xml' file in the '-configuration' command line parameter,
e.g.

`./bin/sico -configuration configs/configuration.xml`

In addition, a jar archive can be built that can be used as extension to an
existing Ptolemy installation. The jar archive can be created with the command

`./gradlew jar`

To use the actors provided in the created jar archive it needs to be added to
the Ptolemy class path and the actors must be provided to the Ptolemy
configuration, either via a command line argument, as described above, or by
editing the Ptolemy configuration files.

The build also generates Ptolemy specific documentation files. This is done
by the gradle task 'ptDoc', which is automatically executed for the 'run',
'jar' and 'distZip' tasks.


## Integration with external data sources

The Silicon Coppélia model requires external data providing the input to the
model and parameters from the belief system of the agent. This data can be
currently provided either from data files or from the SELEMCA software. Data access
is provided by 'Connector' actors, which provide a parameter to choose between
the two options. In addition the directory containing the data files and the
URL to the REST endpoint to the SELEMCE software can be configured.

The name and format of the data files is described in the actor documentation
and examples can be found in the 'sico/data' folder.


## Development

To support the Ptolemy documentation and export system a couple of conventions
need to be followed when extending or modifying the source code:

- Entities of actors, as e.g. ports or other actors that are part of a composite
actor, must be stored in public non-final fields.

- The name of these entities must match the field name.

- Actors must provide a single argument constructor taking a Workspace as
argument and a two argument constructor taking a CompositeEntity and a String
as name argument.

- To support cloning of actors, which is necessary for exporting, the
clone(Workspace) method must be overridden to copy private fields of an actor
when needed.


## Testing

Test for the software can be executed using Gradle by executing the command

`./gradlew test`

Besides unit test, this also includes integration tests that test the execution
of the implemented actors in Ptolemy and if the provided models executes
without exception. The integration tests depend on example data included in the
'data/' and 'examples' directory.


## Known issues

- When copying or exporting parts of the model connections are not drawn. They
must be added manually again.


## Links

* [Source Code](https://bitbucket.org/marrs/silicon-copp-lia)
* [Ptolemy](http://ptolemy.eecs.berkeley.edu/)
* [Gradle](https://gradle.org/)


## License