package org.bitbucket.marrs.sico.algo.compare;

import static junit.framework.Assert.assertEquals;

import java.util.stream.Stream;

import org.bitbucket.marrs.sico.algo.util.RecursiveMeanCollector;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableList;

public class RecursiveMeanCollectorTest {

    private static final double DELTA = 1e-15;

    private RecursiveMeanCollector weightMeanCollector;

    @Before
    public void setupCollector() {
        weightMeanCollector = new RecursiveMeanCollector();
    }

    @Test
    public void emptyStream() {
        Stream<Double> emptyStream = ImmutableList.<Double>of().stream();

        assertEquals(Double.NaN, emptyStream.collect(weightMeanCollector));
    }

    @Test
    public void singleElementStream() {
        Stream<Double> singleElementStream = ImmutableList.of(1.0).stream();

        assertEquals(1.0, singleElementStream.collect(weightMeanCollector));
    }

    @Test
    public void positiveElementsStream() {
        Stream<Double> singleElementStream = ImmutableList.of(0.3, 1.0, 0.1, 0.8).stream();

        assertEquals(0.75, singleElementStream.collect(weightMeanCollector), DELTA);
    }

    @Test
    public void negativeElementsStream() {
        Stream<Double> singleElementStream = ImmutableList.of(-0.3, -1.0, -0.1, -0.8).stream();

        assertEquals(-0.75, singleElementStream.collect(weightMeanCollector), DELTA);
    }

    @Test
    public void mixedElementsStream() {
        Stream<Double> singleElementStream = ImmutableList.of(-0.3, 0.3, -0.75, 0.7, -0.2).stream();

        assertEquals(-0.1, singleElementStream.collect(weightMeanCollector), DELTA);
    }

    @Test
    public void mixedElementsWithNullsStream() {
        Stream<Double> singleElementStream = ImmutableList.of(0.0, -0.3, 0.0, 0.0, 0.3, -0.75, 0.0, 0.7, 0.0, -0.2, 0.0).stream();

        assertEquals(-0.1, singleElementStream.collect(weightMeanCollector), DELTA);
    }
}
