package org.bitbucket.marrs.sico.actor.lib.test;

import java.util.Collection;
import java.util.Optional;

import org.junit.Before;

import ptolemy.actor.CompositeActor;
import ptolemy.actor.Director;
import ptolemy.actor.IOPort;
import ptolemy.actor.Manager;
import ptolemy.actor.TypedActor;
import ptolemy.actor.TypedCompositeActor;
import ptolemy.actor.TypedIORelation;
import ptolemy.actor.lib.Const;
import ptolemy.actor.lib.Discard;
import ptolemy.data.Token;
import ptolemy.domains.sdf.kernel.SDFDirector;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.KernelException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;


public abstract class ActorBaseTest {

    private static final String TEST_MANAGER = "test_manager";
    private static final String TEST_DIRECTOR = "test_director";
    private static final String TEST_SOURCE_CONNECTION_SUFF = "_link_test_source";
    private static final String TEST_SOURCE_SUFF = "_test_source";
    private static final String TEST_SINK_CONNECTION_SUFF = "_link_test_sink";
    private static final String TEST_SINK_SUFF = "_test_sink";

    private CompositeActor container;
    private Workspace workspace;
    private Manager manager;
    private SDFDirector director;

    @Before
    public void setupContainer() throws IllegalActionException, NameDuplicationException {
        workspace = new Workspace();
        container = new TypedCompositeActor(workspace);
        manager = new Manager(workspace, TEST_MANAGER);
        container.setManager(manager);

        director = new SDFDirector(container, TEST_DIRECTOR);
    }

    protected void feedConstantInput(TypedActor actor, String portName, String expression) throws IllegalActionException, NameDuplicationException {
        IOPort port = findPort(portName, actor.inputPortList());

        feedConstantInput(actor, port, expression);
    }

    protected void feedConstantInput(TypedActor actor, String portName, Token token) throws IllegalActionException, NameDuplicationException {
        IOPort port = findPort(portName, actor.inputPortList());

        feedConstantInput(actor, port, token);
    }

    protected void feedConstantInput(TypedActor actor, IOPort port, String expression) throws IllegalActionException, NameDuplicationException {
        feedValueInput(actor, port, expression);
    }

    protected void feedConstantInput(TypedActor actor, IOPort port, Token value) throws IllegalActionException, NameDuplicationException {
        feedValueInput(actor, port, value);
    }

    private void feedValueInput(TypedActor actor, IOPort port, Object value) throws IllegalActionException, NameDuplicationException {
        String portName = port.getName();

        Const testInput = new Const(container, portName + TEST_SOURCE_SUFF);
        if (value instanceof Token) {
            testInput.value.setToken((Token) value);
        } else {
            testInput.value.setExpression((String) value);
        }

        TypedIORelation connection = new TypedIORelation(container, portName + TEST_SOURCE_CONNECTION_SUFF);
        testInput.output.link(connection);

        port.link(connection);
    }

    protected <T extends Token> PortListener<T> terminateOutput(TypedActor actor, String portName) throws IllegalActionException, NameDuplicationException {
        IOPort port = findPort(portName, actor.outputPortList());

        return terminateOutput(actor, port);
    }

    protected <T extends Token> PortListener<T> terminateOutput(TypedActor actor, IOPort port) throws IllegalActionException, NameDuplicationException {
        String portName = port.getName();

        Discard sink = new Discard(container, portName + TEST_SINK_SUFF);

        TypedIORelation connection = new TypedIORelation(container, portName + TEST_SINK_CONNECTION_SUFF);
        sink.input.link(connection);
        port.link(connection);

        PortListener<T> listener = new PortListener<>();
        sink.input.addIOPortEventListener(listener);

        return listener;
    }

    private IOPort findPort(String name, Collection<IOPort> ports) {
        Optional<IOPort> port = ports.stream()
                .filter(p -> p.getName().equals(name))
                .findFirst();

        return port.orElseThrow(() -> new IllegalArgumentException("No port with name: " + name));
    }

    protected void allowDisconnectedGraphs() {
        director.allowDisconnectedGraphs.setExpression("true");
    }

    protected void executeModel() throws KernelException {
        manager.execute();
    }

    protected Director getDirector() {
        return director;
    }

    protected Manager getManager() {
        return manager;
    }

    protected Workspace getWorkspace() {
        return workspace;
    }

    protected CompositeActor getContainer() {
        return container;
    }
}