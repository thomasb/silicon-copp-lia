package org.bitbucket.marrs.sico.itest;

import java.io.InputStream;

import org.bitbucket.marrs.sico.actor.lib.test.ActorBaseTest;
import org.junit.Test;

import ptolemy.actor.CompositeActor;
import ptolemy.moml.MoMLParser;


public final class ModelExecutionTest extends ActorBaseTest {

    @Test
    public void modelExecutes() throws Exception {
        InputStream momlStream = getClass().getResourceAsStream("/silicon-coppelia.xml");
        CompositeActor model = (CompositeActor) new MoMLParser(getWorkspace()).parse(null, "test model", momlStream);
        model.setManager(getManager());

        executeModel();
    }

    @Test
    public void modelMonitoredExecutes() throws Exception {
        InputStream momlStream = getClass().getResourceAsStream("/silicon-coppelia-monitored.xml");
        CompositeActor model = (CompositeActor) new MoMLParser(getWorkspace()).parse(null, "test model", momlStream);
        model.setManager(getManager());

        executeModel();
    }
}
