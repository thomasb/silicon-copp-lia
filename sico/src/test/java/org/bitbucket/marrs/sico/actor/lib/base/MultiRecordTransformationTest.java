package org.bitbucket.marrs.sico.actor.lib.base;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static org.bitbucket.marrs.sico.actor.lib.test.RecordMatcher.hasEntries;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.Collection;
import java.util.List;

import org.bitbucket.marrs.sico.actor.lib.test.ActorBaseTest;
import org.bitbucket.marrs.sico.actor.lib.test.PortListener;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import ptolemy.actor.TypedIOPort.RunTimeTypeCheckException;
import ptolemy.data.DoubleToken;
import ptolemy.data.RecordToken;
import ptolemy.data.StringToken;
import ptolemy.data.Token;
import ptolemy.data.type.BaseType;
import ptolemy.data.type.Type;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.KernelException;
import ptolemy.kernel.util.NameDuplicationException;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;


@RunWith(MockitoJUnitRunner.class)
public class MultiRecordTransformationTest extends ActorBaseTest {

    private static final String CONTAINER_NAME = "testContainer";

    private MultiRecordTransformation<StringToken> createTestTransformation(List<String> inputNames) throws IllegalActionException, NameDuplicationException {
        MultiRecordTransformation<StringToken> recordTransformation = new TestTransformation(getContainer(),
                CONTAINER_NAME,
                inputNames,
                BaseType.STRING,
                BaseType.STRING);

        return recordTransformation;
    }

    @Test
    public void outputPortIsFed() throws IllegalActionException, NameDuplicationException {
        MultiRecordTransformation<?> recordTransformation = createTestTransformation(emptyList());
        PortListener<RecordToken> listener = new PortListener<>();
        recordTransformation.getOutput().addIOPortEventListener(listener);

        recordTransformation.fire();

        assertThat(listener.getToken(), hasEntries(emptyMap()));
    }

    @Test
    public void outputAggregatePortIsFed() throws IllegalActionException, NameDuplicationException {
        MultiRecordTransformation<?> recordTransformation = createTestTransformation(emptyList());
        PortListener<RecordToken> listener = new PortListener<>();
        recordTransformation.getOutputAggregate().addIOPortEventListener(listener);

        recordTransformation.fire();

        assertEquals(listener.getToken(), new StringToken(""));
    }

    @Test
    public void inputsAreTransformed() throws KernelException {
        MultiRecordTransformation<?> recordTransformation = createTestTransformation(asList("input_1", "input_2"));

        feedConstantInput(recordTransformation, "input_1",
                "{\"first\" = \"a\", \"second\" = \"c\", \"one\" = \"e\"}");
        feedConstantInput(recordTransformation, "input_2",
                "{\"first\" = \"b\", \"second\" = \"d\", \"two\" = \"f\"}");

        PortListener<RecordToken> outputListener = terminateOutput(recordTransformation, recordTransformation.getOutput());
        terminateOutput(recordTransformation, recordTransformation.getOutputAggregate());

        executeModel();

        assertThat(outputListener.getToken(), hasEntries(ImmutableMap.of(
                "first", new StringToken("ab"),
                "second", new StringToken("cd"),
                "one", new StringToken("e"),
                "two", new StringToken("f"))));
    }

    @Test
    public void inputTotalIsCalculated() throws KernelException {
        MultiRecordTransformation<?> recordTransformation = createTestTransformation(asList("input_1", "input_2"));

        feedConstantInput(recordTransformation, "input_1",
                "{\"first\" = \"a\", \"second\" = \"c\", \"one\" = \"e\"}");
        feedConstantInput(recordTransformation, "input_2",
                "{\"first\" = \"b\", \"second\" = \"d\", \"two\" = \"f\"}");

        terminateOutput(recordTransformation, recordTransformation.getOutput());
        PortListener<?> outputTotalListener = terminateOutput(recordTransformation, recordTransformation.getOutputAggregate());

        executeModel();

        assertEquals(new StringToken("abcdef"), outputTotalListener.getToken());
    }

    @Test
    public void inputContainsNilTokens() throws KernelException {
        MultiRecordTransformation<?> recordTransformation = createTestTransformation(asList("input_1", "input_2"));

        feedConstantInput(recordTransformation, "input_1",
                new RecordToken(ImmutableMap.of("first", Token.NIL)));
        feedConstantInput(recordTransformation, "input_2",
                new RecordToken(ImmutableMap.of("first", Token.NIL)));

        PortListener<RecordToken> outputListener = terminateOutput(recordTransformation, recordTransformation.getOutput());
        PortListener<?> outputTotalListener = terminateOutput(recordTransformation, recordTransformation.getOutputAggregate());

        executeModel();

        assertThat(outputListener.getToken(), hasEntries(ImmutableMap.of("first", new StringToken(""))));
        assertEquals(new StringToken(""), outputTotalListener.getToken());
    }

    @Test(expected=RunTimeTypeCheckException.class)
    public void wrongValueTypeThrows() throws KernelException {
        MultiRecordTransformation<StringToken> recordTransformation = new TestTransformation(getContainer(),
                CONTAINER_NAME,
                ImmutableList.of("input_1"),
                BaseType.DATE,
                BaseType.STRING);

        feedConstantInput(recordTransformation, "input_1", new RecordToken(ImmutableMap.of("a", new DoubleToken(1.0))));

        terminateOutput(recordTransformation, recordTransformation.getOutput());
        terminateOutput(recordTransformation, recordTransformation.getOutputAggregate());

        executeModel();
    }

    @Test(expected=RunTimeTypeCheckException.class)
    public void wrongAggregateTypeThrows() throws KernelException {
        MultiRecordTransformation<StringToken> recordTransformation = new TestTransformation(getContainer(),
                CONTAINER_NAME,
                ImmutableList.of("input_1"),
                BaseType.STRING,
                BaseType.DATE);

        feedConstantInput(recordTransformation, "input_1", "{\"first\" = \"a\"}");

        terminateOutput(recordTransformation, recordTransformation.getOutput());
        terminateOutput(recordTransformation, recordTransformation.getOutputAggregate());

        executeModel();
    }

    private static class TestTransformation extends MultiRecordTransformation<StringToken> {

        private static final StringToken EMPTY_STRING = new StringToken("");

        public TestTransformation(CompositeEntity container,
                String name,
                List<String> inputNames,
                Type aggregateOutputType,
                Type valueType) throws IllegalActionException, NameDuplicationException {
            super(container, name, inputNames, aggregateOutputType, valueType);
        }

        @Override
        protected Transformation<Token, StringToken> getTransformation() throws IllegalActionException {
            return (label, args) -> (StringToken) args.stream()
                    .filter(t -> !t.isNil())
                    .reduce(EMPTY_STRING, (t, u) -> append(t, u));
        }

        private StringToken append(Token u, Token v) {
            try {
                return (StringToken) u.add(v);
            } catch (IllegalActionException e) {
                throw new RuntimeException("Append failed: ", e);
            }
        }

        @Override
        protected Token aggregateOutput(Collection<StringToken> outputs) throws IllegalActionException {
            return outputs.stream()
                    .sorted((t, u) -> t.stringValue().compareTo(u.stringValue()))
                    .reduce(EMPTY_STRING, (t, u) -> append(t, u));
        }
    }
}
