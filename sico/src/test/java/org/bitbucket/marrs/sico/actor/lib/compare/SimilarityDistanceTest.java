package org.bitbucket.marrs.sico.actor.lib.compare;

import static junit.framework.Assert.assertEquals;
import static org.bitbucket.marrs.sico.data.type.FuzzyWeightType.FUZZY_WEIGHT;

import org.bitbucket.marrs.sico.actor.lib.test.ActorBaseTest;
import org.bitbucket.marrs.sico.actor.lib.test.PortListener;
import org.junit.Test;

import ptolemy.data.RecordToken;
import ptolemy.data.Token;
import ptolemy.kernel.util.KernelException;

public class SimilarityDistanceTest extends ActorBaseTest {

    @Test
    public void actorExecutes() throws KernelException {
        SimilarityDistance similarity = new SimilarityDistance(getContainer(), "testSimilarity");

        feedConstantInput(similarity, similarity.ethicsInput, "{ \"feature\" = { \"indicative\" = 1.0, \"counter-indicative\" = 1.0 } }");
        feedConstantInput(similarity, similarity.affordancesInput, "{ \"feature\" = { \"indicative\" = 1.0, \"counter-indicative\" = 1.0 } }");
        feedConstantInput(similarity, similarity.aestheticsInput, "{ \"feature\" = { \"indicative\" = 1.0, \"counter-indicative\" = 1.0 } }");
        feedConstantInput(similarity, similarity.epistemicsInput, "{ \"feature\" = { \"indicative\" = 1.0, \"counter-indicative\" = 1.0 } }");

        feedConstantInput(similarity, similarity.ethicsInputSelf, "{ \"feature\" = { \"indicative\" = 1.0, \"counter-indicative\" = 1.0 } }");
        feedConstantInput(similarity, similarity.affordancesInputSelf, "{ \"feature\" = { \"indicative\" = 1.0, \"counter-indicative\" = 1.0 } }");
        feedConstantInput(similarity, similarity.aestheticsInputSelf, "{ \"feature\" = { \"indicative\" = 1.0, \"counter-indicative\" = 1.0 } }");
        feedConstantInput(similarity, similarity.epistemicsInputSelf, "{ \"feature\" = { \"indicative\" = 1.0, \"counter-indicative\" = 1.0 } }");

        feedConstantInput(similarity, similarity.simmilarityWeightInput, "{ \"ethicsIndicative\" = 1.0, \"ethicsCounter\" = 1.0, "
                + "\"affordancesIndicative\" = 1.0, \"affordancesCounter\" = 1.0, "
                + "\"aestheticsIndicative\" = 1.0, \"aestheticsCounter\" = 1.0, "
                + "\"epistemicsIndicative\" = 1.0, \"epistemicsCounter\"= 1.0 }");
        feedConstantInput(similarity, similarity.dissimmilarityWeightInput, "{ \"ethicsIndicative\" = 1.0, \"ethicsCounter\" = 1.0, "
                + "\"affordancesIndicative\" = 1.0, \"affordancesCounter\" = 1.0, "
                + "\"aestheticsIndicative\" = 1.0, \"aestheticsCounter\" = 1.0, "
                + "\"epistemicsIndicative\" = 1.0, \"epistemicsCounter\"= 1.0 }");

        PortListener<Token> output = terminateOutput(similarity, similarity.output);
        PortListener<Token> outputAggregate = terminateOutput(similarity, similarity.outputAggregate);

        executeModel();

        RecordToken outputRecord = (RecordToken) output.getToken();
        assertEquals(1.0, FUZZY_WEIGHT.convert(outputRecord.get("feature")).getPositiveWeight());
        assertEquals(0.0, FUZZY_WEIGHT.convert(outputRecord.get("feature")).getNegativeWeight());

        RecordToken aggregateOutputRecord = (RecordToken) outputAggregate.getToken();
        assertEquals(1.0, FUZZY_WEIGHT.convert(aggregateOutputRecord).getPositiveWeight());
        assertEquals(0.0, FUZZY_WEIGHT.convert(aggregateOutputRecord).getNegativeWeight());
    }
}
