package org.bitbucket.marrs.sico.actor.lib.compare;

import static org.bitbucket.marrs.sico.data.type.FuzzyWeightType.FUZZY_WEIGHT;
import static org.junit.Assert.assertTrue;

import org.bitbucket.marrs.sico.actor.lib.encode.LookupDomainEncoder;
import org.bitbucket.marrs.sico.actor.lib.test.ActorBaseTest;
import org.bitbucket.marrs.sico.actor.lib.test.PortListener;
import org.junit.Test;

import ptolemy.data.RecordToken;
import ptolemy.data.Token;
import ptolemy.kernel.util.KernelException;

public class SimilarityTest extends ActorBaseTest {

    @Test
    public void test() throws KernelException {
        Similarity similarity = new Similarity(getContainer(), "testSimilarity");
        ((LookupDomainEncoder) similarity.selfEncoding.ethicsEncoder).fileNameParameter.setExpression("examples/lookup_example.json");
        ((LookupDomainEncoder) similarity.selfEncoding.affordancesEncoder).fileNameParameter.setExpression("examples/lookup_example.json");
        ((LookupDomainEncoder) similarity.selfEncoding.aestheticsEncoder).fileNameParameter.setExpression("examples/lookup_example.json");
        ((LookupDomainEncoder) similarity.selfEncoding.epistemicsEncoder).fileNameParameter.setExpression("examples/lookup_example.json");

        feedConstantInput(similarity, similarity.featuresInput, "{ \"Money\" = 1.0 }");

        feedConstantInput(similarity, similarity.ethicsInput, "{ \"Money\" = { \"indicative\" = 1.0, \"counter-indicative\" = 0.0 } }");
        feedConstantInput(similarity, similarity.affordancesInput, "{ \"Money\" = { \"indicative\" = 1.0, \"counter-indicative\" = 0.0 } }");
        feedConstantInput(similarity, similarity.aestheticsInput, "{ \"Money\" = { \"indicative\" = 1.0, \"counter-indicative\" = 0.0 } }");
        feedConstantInput(similarity, similarity.epistemicsInput, "{ \"Money\" = { \"indicative\" = 1.0, \"counter-indicative\" = 0.0 } }");

        PortListener<Token> output = terminateOutput(similarity, similarity.similarityOutput);
        PortListener<Token> outputTotal = terminateOutput(similarity, similarity.similarityOutputTotal);

        allowDisconnectedGraphs();
        executeModel();

        RecordToken outputRecord = (RecordToken) output.getToken();
        assertTrue(FUZZY_WEIGHT.convert(outputRecord.get("Money")).getPositiveWeight() > 0.0);
        assertTrue(FUZZY_WEIGHT.convert(outputRecord.get("Money")).getNegativeWeight() > 0.0);

        RecordToken aggregateOutputRecord = (RecordToken) outputTotal.getToken();
        assertTrue(FUZZY_WEIGHT.convert(aggregateOutputRecord).getPositiveWeight() > 0.0);
        assertTrue(FUZZY_WEIGHT.convert(aggregateOutputRecord).getNegativeWeight() > 0.0);
    }
}
