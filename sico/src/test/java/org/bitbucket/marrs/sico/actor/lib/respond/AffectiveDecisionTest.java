package org.bitbucket.marrs.sico.actor.lib.respond;

import static junit.framework.Assert.assertEquals;

import org.bitbucket.marrs.sico.actor.lib.test.ActorBaseTest;
import org.bitbucket.marrs.sico.actor.lib.test.PortListener;
import org.junit.Test;

import ptolemy.data.StringToken;
import ptolemy.data.Token;
import ptolemy.kernel.util.KernelException;

public final class AffectiveDecisionTest extends ActorBaseTest {

    @Test
    public void actorExecutes() throws KernelException {
        AffectiveDecision decisionContainer = new AffectiveDecision(getContainer(), "testADContainer");

        feedConstantInput(decisionContainer, decisionContainer.useIntentionsInput, "{ \"feature\" = { \"indicative\" = 1.0, \"counter-indicative\" = 0.0 } }");
        feedConstantInput(decisionContainer, decisionContainer.tradeoffInput, "{ \"feature\" = 1.0 }");
        feedConstantInput(decisionContainer, decisionContainer.actionsInput, "{ \"feature\" = \"test\" }");

        PortListener<Token> output = terminateOutput(decisionContainer, decisionContainer.actionOutput);

        executeModel();

        assertEquals("test", StringToken.convert(output.getToken()).stringValue());
    }
}
