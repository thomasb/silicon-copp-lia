package org.bitbucket.marrs.sico.actor.lib.connect;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.bitbucket.marrs.sico.rest.RestGet;
import org.bitbucket.marrs.sico.selemca.entity.Association;
import org.bitbucket.marrs.sico.selemca.entity.AssociationMeta;
import org.bitbucket.marrs.sico.selemca.entity.ConceptMeta;

import ptolemy.kernel.util.IllegalActionException;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

final class EpistemicsDataSource implements DataSource {

    private static final Gson GSON = new Gson();
    private static final Type CONCEPT_META_LIST_TYPE = new TypeToken<List<ConceptMeta>>() {/**/}.getType();
    private static final Type ASSOCIATION_LIST_TYPE = new TypeToken<List<Association>>() {/**/}.getType();
    private static final Type ASSOCIATION_META_LIST_TYPE = new TypeToken<List<AssociationMeta>>() {/**/}.getType();

    private static final String ACTION_TYPE_RELATION = "action_type";
    private static final String GOAL_TYPE_RELATION = "goal_ambition";
    private static final String TRANSITION_WEIGTH_RELATION = "transition_weight";

    private static final String REST_CONCEPTMETA = "conceptmeta";
    private static final String REST_ASSOCIATION = "association";
    private static final String REST_ASSOCIATIONMETA = "associationmeta";

    private static final String REST_PARAMETER_CONCEPT_ID = "conceptId";
    private static final String REST_PARAMETER_CONCEPT_1_ID = "concept1Id";
    private static final String REST_PARAMETER_CONCEPT_2_ID = "concept2Id";
    private static final String REST_PARAMETER_VALUE_SELF = "self";

    private RestGet conceptMetaRestGet;
    private RestGet associationsRestGet;
    private RestGet associationMetaRestGet;

    EpistemicsDataSource(String baseUrl) throws URISyntaxException {
        conceptMetaRestGet = createRestGet(REST_CONCEPTMETA, baseUrl);
        associationsRestGet = createRestGet(REST_ASSOCIATION, baseUrl);
        associationMetaRestGet = createRestGet(REST_ASSOCIATIONMETA, baseUrl);
    }

    private static RestGet createRestGet(String endPoint, String baseUrl) throws URISyntaxException {
        URI restEndpoint = new URI(baseUrl).resolve(endPoint);

        return new RestGet(restEndpoint);
    }

    @Override
    public List<Association> queryActions(String featureName) throws IllegalActionException {
        return queryAssociations(featureName);
    }

    @Override
    public Optional<ConceptMeta> queryActionMeta(String actionName) throws IllegalActionException {
        return queryConceptMeta(actionName).stream()
                .filter(m -> m.getRelation().equalsIgnoreCase(ACTION_TYPE_RELATION))
                .findFirst();
    }

    @Override
    public List<Association> queryGoals(String actionName) throws IllegalActionException {
        return queryAssociations(actionName);
    }

    @Override
    public AssociationMeta queryActionGoalMeta(String actionName, String goalName) throws IllegalActionException {
        return queryAssociationsMeta(actionName, goalName).stream()
                .filter(m -> m.getRelation().equalsIgnoreCase(TRANSITION_WEIGTH_RELATION))
                .findFirst()
                .orElseThrow(() -> new IllegalActionException("No metadata with relation " + TRANSITION_WEIGTH_RELATION + " for " + actionName + ", " + goalName));
    }

    @Override
    public Optional<ConceptMeta> queryGoalMeta(String goalName) throws IllegalActionException {
        return queryConceptMeta(goalName).stream()
                .filter(m -> m.getRelation().equalsIgnoreCase(GOAL_TYPE_RELATION))
                .findFirst();
    }

    @Override
    public List<ConceptMeta> querySelf() throws IllegalActionException {
        return queryConceptMeta(REST_PARAMETER_VALUE_SELF);
    }

    private List<ConceptMeta> queryConceptMeta(String conceptId) throws IllegalActionException {
        String conceptMetaJson = "";
        try {
            conceptMetaJson = conceptMetaRestGet.get(ImmutableMap.of(REST_PARAMETER_CONCEPT_ID, conceptId));

            return GSON.fromJson(conceptMetaJson, CONCEPT_META_LIST_TYPE);
        } catch (JsonSyntaxException e) {
            throw new IllegalActionException("Could not parse data for self from Json: " + conceptMetaJson + ": " + e.getMessage());
        } catch (IOException e) {
            throw new IllegalActionException("Could not retrieve data from " + conceptMetaRestGet.getBaseUrl() + ": " + e.getMessage());
        }
    }

    private List<Association> queryAssociations(String conceptId) throws IllegalActionException {
        String associationsJson = "";
        try {
            associationsJson = associationsRestGet.get(ImmutableMap.of(REST_PARAMETER_CONCEPT_ID, conceptId));

            return GSON.fromJson(associationsJson, ASSOCIATION_LIST_TYPE);
        } catch (JsonSyntaxException e) {
            throw new IllegalActionException("Could not parse association data for " + conceptId + " from Json: " + associationsJson + ": " + e.getMessage());
        } catch (IOException e) {
            throw new IllegalActionException("Could not retrieve data for " + conceptId  + " from " + conceptMetaRestGet.getBaseUrl() + ": " + e.getMessage());
        }
    }

    private List<AssociationMeta> queryAssociationsMeta(String concept1Id, String concept2Id) throws IllegalActionException {
        String associationsMetaJson = "";
        try {
            associationsMetaJson = associationMetaRestGet.get(ImmutableMap.of(
                REST_PARAMETER_CONCEPT_1_ID, concept1Id,
                REST_PARAMETER_CONCEPT_2_ID, concept2Id));

            return GSON.fromJson(associationsMetaJson, ASSOCIATION_META_LIST_TYPE);
        } catch (JsonSyntaxException e) {
            throw new IllegalActionException("Could not parse data for " + concept1Id + "' " + concept2Id + " from Json: " + associationsMetaJson + ": " + e.getMessage());
        } catch (IOException e) {
            throw new IllegalActionException("Could not retrieve data for " + concept1Id + "' " + concept2Id + ": " + e.getMessage());
        }
    }

}
