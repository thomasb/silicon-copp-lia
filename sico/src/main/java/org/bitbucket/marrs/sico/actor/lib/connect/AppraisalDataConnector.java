package org.bitbucket.marrs.sico.actor.lib.connect;

import static java.util.stream.Collectors.toList;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordInputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordOutputPort;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.bitbucket.marrs.sico.selemca.entity.Association;
import org.bitbucket.marrs.sico.selemca.entity.AssociationData;
import org.bitbucket.marrs.sico.selemca.entity.AssociationMeta;
import org.bitbucket.marrs.sico.selemca.entity.ConceptMeta;

import ptolemy.actor.TypedIOPort;
import ptolemy.data.RecordToken;
import ptolemy.data.StringToken;
import ptolemy.data.Token;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

import com.google.gson.Gson;

/**
 * {@code AppraisalDataConnector} provides data necessary for the appraisal
 * process.
 * <p>
 * The data can be either retrieved from a file or from an epistemics engine.
 * <p>
 * If the data is retrieved from files, the following files must be present in
 * the data directory:
 * <ul>
 *      <li>actions.json</li>
 *      <li>action_meta.json</li>
 *      <li>action_goals.json</li>
 *      <li>action_goals_meta.json</li>
 *      <li>goal_meta.json</li>
 * </ul>
 */
public final class AppraisalDataConnector extends LinearModelDataConnector {

    private static final AssociationMeta EMPTY_METADATA = new AssociationMeta(null, null, "0.0", "0.0");

    /**
     * Input for the features. The type of the port is {@link BaseType#RECORD}
     * with values of type {@link BaseType#DOUBLE}.
     */
    public TypedIOPort featuresInput;

    /**
     * Output port for feature - action associations. The output is of type
     * {@link BaseType#RECORD} where the keys are feature names and the values
     * are arrays of JSON strings that describe the associations of features
     * with actions. The JSON strings contain a JSON object describing the
     * feature in the property "concept1", a JSON object describing the action
     * in the property "concept2" and the truth value of the association in the
     * property "truthValue".
     */
    public TypedIOPort featureToActionOutput;

    /**
     * Output port for data about the actions. The output is of type
     * {@link BaseType#RECORD} where the keys are action names and the values
     * are JSON strings that hold data about the actions. The JSON strings
     * contain a JSON object describing the action in the property "concept",
     * the name of the data in the property "relation" and a string
     * representation of the value of the data in the property "value".
     */
    public TypedIOPort actionOutput;

    /**
     * Output port for action - goal associations. The output is of type
     * {@link BaseType#RECORD} where the keys are action names and the values
     * are arrays of JSON string tuples that describe the associations of
     * actions with the goals.
     * <p>
     * The first entry in the tuple is a JSON object that contains a JSON
     * object describing the action in the property "concept1", a JSON object
     * describing the goal in the property "concept2" and the truth value of
     * the association in the property "truthValue".
     * <p>
     * The second entry in the tuple is a JSON object that contains a JSON
     * object describing data related to the association. It contains a JSON
     * object describing the action in the property "concept1", a JSON object
     * describing the goal in the property "concept2", the name of the data in
     * the property "relation" and a string representation of the value of the
     * data in the property "value".
     */
    public TypedIOPort actionToGoalOutput;

    /**
     * Output port for data about the goals. The output is of type
     * {@link BaseType#RECORD} where the keys are goal names and the values
     * are arrays of JSON strings that hold data about the goals. The JSON
     * strings contain a JSON object describing the goal in the property
     * "concept", the name of the data in the property "relation" and a string
     * representation of the value of the data in the property "value".
     */
    public TypedIOPort goalOutput;

    public AppraisalDataConnector(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace);
        init();
    }

    public AppraisalDataConnector(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        super(container, name);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        this.featuresInput = createRecordInputPort(this, "featuresInput");

        this.featureToActionOutput = createRecordOutputPort(this, "featureToActionOutput");
        this.actionOutput = createRecordOutputPort(this, "actionOutput");
        this.actionToGoalOutput = createRecordOutputPort(this, "actionToGoalOutput");
        this.goalOutput = createRecordOutputPort(this, "goalOutput");
    }

    @Override
    public void fire() throws IllegalActionException {
        super.fire();

        RecordToken featuresRecord = (RecordToken) featuresInput.get(0);

        Map<String, List<AssociationData>> featureActionsMap = new HashMap<>();
        Map<String, ConceptMeta> actionsMap = new HashMap<>();
        Map<String, List<AssociationData>> actionGoalsMap = new HashMap<>();
        Map<String, ConceptMeta> goalsMap = new HashMap<>();

        for (String featureName : featuresRecord.labelSet()) {
            List<Association> actionLinks = getDataSource().queryActions(featureName);
            if (actionLinks.isEmpty()) {
                continue;
            }

            Iterator<Association> actionLinkItr = actionLinks.iterator();
            while (actionLinkItr.hasNext()) {
                Association actionLink = actionLinkItr.next();

                String actionName = getAssociate(actionLink, featureName);
                if (actionsMap.keySet().contains(actionName)) {
                    continue;
                }

                Optional<ConceptMeta> actionMeta = getDataSource().queryActionMeta(actionName);
                if (!actionMeta.isPresent()) {
                    actionLinkItr.remove();
                    continue;
                }

                actionsMap.put(actionName, actionMeta.get());

                List<Association> goalLinks = getDataSource().queryGoals(actionName);
                Map<String, AssociationMeta> actionGoalsMetaMap = new HashMap<>();
                for (Association goalLink : goalLinks) {
                    String goalName = getAssociate(goalLink, actionName);
                    Optional<ConceptMeta> goalMeta = getDataSource().queryGoalMeta(goalName);
                    if (!goalMeta.isPresent()) {
                        continue;
                    }

                    AssociationMeta actionGoalMeta = getDataSource().queryActionGoalMeta(actionName, goalName);
                    actionGoalsMetaMap.put(goalName, actionGoalMeta);
                    goalsMap.put(goalName, goalMeta.get());
                }

                List<AssociationData> actionGoalLinkData = createAssociationData(goalLinks, actionGoalsMetaMap, actionName);
                actionGoalsMap.put(actionName, actionGoalLinkData);
            }
            featureActionsMap.put(featureName, actionLinks.stream().map(a -> new AssociationData(a, EMPTY_METADATA)).collect(toList()));
        }

        featureToActionOutput.send(0, toRecord(featureActionsMap));
        actionOutput.send(0, toRecord(actionsMap));
        actionToGoalOutput.send(0, toRecord(actionGoalsMap));
        goalOutput.send(0, toRecord(goalsMap));
    }

    protected String getAssociate(Association association, String featureName) {
        String name1 = association.getConcept1().getName();

        return name1.equals(featureName) ? association.getConcept2().getName() : name1;
    }

    private List<AssociationData> createAssociationData(List<Association> goalLinks,
            Map<String, AssociationMeta> actionGoalsMetaMap,
            String actionName) {
        return goalLinks.stream()
                .filter(a -> actionGoalsMetaMap.containsKey(getAssociate(a, actionName)))
                .map(a -> new AssociationData(a, actionGoalsMetaMap.get(getAssociate(a, actionName))))
                .collect(toList());
    }

    private RecordToken toRecord(Map<String, ?> map) throws IllegalActionException {
        Map<String, Token> recordMap = map.entrySet().stream()
                .collect(Collectors.toMap(Entry::getKey, this::valueToToken));

        return new RecordToken(recordMap);
    }

    private Token valueToToken(Entry<String, ?> entry) {
        Gson gson = new Gson();
        Object value = entry.getValue();

        return new StringToken(gson.toJson(value));
    }
}
