package org.bitbucket.marrs.sico.actor.lib.util;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.stream.Collectors.toMap;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Function;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.google.gson.Gson;

public final class JsonFileLookup<T, A> {

    private static final Gson GSON = new Gson();

    private final URI filePath;
    private final Type valueType;
    private final Function<A, T> valueTransformation;

    private Map<String, T> lookupMap;

    private JsonFileLookup(URI filePath, Function<A, T> valueTransformation, Type valueType) {
        this.filePath = filePath;
        this.valueType = valueType;
        this.valueTransformation = valueTransformation;
    }

    public static <T, A> JsonFileLookup<T, A> create(URI filePath,
        Function<A, T> valueTransformation,
        Type conversionType) {
        Function<A, T> transformation = checkNotNull(valueTransformation, "valueTransformation must not be null");

        return new JsonFileLookup<>(filePath, transformation, conversionType);
    }

    public static <T> JsonFileLookup<T, T> create(URI filePath, Type type) {
        return new JsonFileLookup<>(filePath, null, type);
    }

    public Optional<T> get(String key) {
        return Optional.ofNullable(getLookupMap().get(key));
    }

    private Map<String, T> getLookupMap() {
        if (lookupMap == null) {
            cacheFromFile();
        }

        return lookupMap;
    }

    @SuppressWarnings("unchecked")
    private void cacheFromFile() {
        File lookupDataFile = new File(filePath);

        String lookupJSON;
        try {
            lookupJSON = Files.toString(lookupDataFile, Charsets.UTF_8);
        } catch (IOException e) {
            throw new JsonFileException("Could not read file \"" + filePath + "\". Please specify a file containing JSON lookup data.", e);
        }

        Map<String, A> rawLookupMap = GSON.fromJson(lookupJSON, valueType);
        if (valueTransformation != null) {
            lookupMap = rawLookupMap.entrySet().stream()
                    .collect(toMap(Entry::getKey, this::transformValue));
        } else {
            // No value transformation implies A = T
            lookupMap = (Map<String, T>) rawLookupMap;
        }
    }

    private T transformValue(Entry<String, A> entry) {
        return valueTransformation.apply(entry.getValue());
    }

    public static class JsonFileException extends RuntimeException {

        public JsonFileException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}