package org.bitbucket.marrs.sico.selemca.entity;

import java.io.Serializable;

/**
 * Metadata for a concept. Defined as a tripple: concept - relation - value
 * There can be any number of metadata items for a concept.
 */
public class ConceptMeta implements Serializable {
    private Concept concept;
    private String relation;
    private String value;

    public ConceptMeta() {
        // Support instantiation by reflection
    }

    public ConceptMeta(Concept concept, String relation, String value) {
        this.concept = concept;
        this.relation = relation;
        this.value = value;
    }

    public Concept getConcept() {
        return concept;
    }

    public String getRelation() {
        return relation;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s", concept, relation, value);
    }
}
