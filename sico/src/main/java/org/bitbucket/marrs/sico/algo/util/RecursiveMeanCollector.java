package org.bitbucket.marrs.sico.algo.util;

import static java.util.stream.Collector.Characteristics.UNORDERED;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

import com.google.common.collect.ImmutableSet;

public final class RecursiveMeanCollector implements Collector<Double, List<Double>, Double> {

    @Override
    public Supplier<List<Double>> supplier() {
        return ArrayList::new;
    }

    @Override
    public BiConsumer<List<Double>, Double> accumulator() {
        return (accumulator, value) -> {
                accumulator.add(value);
            };
    }

    @Override
    public BinaryOperator<List<Double>> combiner() {
        return (l1, l2) -> {
                List<Double> combined = new ArrayList<>();
                combined.addAll(l1);
                combined.addAll(l2);

                return combined;
            };
    }

    @Override
    public Function<List<Double>, Double> finisher() {
        return l -> RecursiveMean.calculateMean(l);
    }

    @Override
    public Set<java.util.stream.Collector.Characteristics> characteristics() {
        return ImmutableSet.of(UNORDERED);
    }

}
