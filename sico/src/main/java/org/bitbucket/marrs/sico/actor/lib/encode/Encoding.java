package org.bitbucket.marrs.sico.actor.lib.encode;

import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordInputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordOutputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createTypedOutputPort;
import static org.bitbucket.marrs.sico.data.type.FuzzyWeightType.FUZZY_WEIGHT;

import org.bitbucket.marrs.sico.actor.lib.base.DomainPort;
import org.bitbucket.marrs.sico.data.type.FuzzyWeightType;

import ptolemy.actor.TypedCompositeActor;
import ptolemy.actor.TypedIOPort;
import ptolemy.actor.TypedIORelation;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

/**
 * Actor for the encoding of the features in the appraisal domains.
 * <p>
 * The actor creates an encoding of the input features in the appraisal domains
 * "Ethics", "Affordances", "Aesthetics", "Epistemics". For each domain an
 * indicative and a counter-indicative weight is created.
 */
public final class Encoding extends TypedCompositeActor {

    private static final String FEATURES_IN = "featuresInput";
    private static final String FEATURES_IN_DISP = "Features";
    private static final String FEATURE_RELATION = "featureRelation";

    private static final String ETHICS_ENCODER = "ethics";
    private static final String ETHICS_ENCODER_DISP = "Ethics";
    private static final String AFFORDANCES_ENCODER = "affordances";
    private static final String AFFORDANCES_ENCODER_DISP = "Affordances";
    private static final String AESTHETICS_ENCODER = "aesthetics";
    private static final String AESTHETICS_ENCODER_DISP = "Aesthetics";
    private static final String EPISTEMICS_ENCODER = "epistemics";
    private static final String EPISTEMICS_ENCODER_DISP = "Epistemics";

    /**
     * Input for the features. The input expects a {@link BaseType#RECORD} with
     * named features.
     */
    public TypedIOPort featuresInput;

    /**
     * Output for the encoded ethics values for of features. The output is of
     * type {@link BaseType#RECORD}, where the values are of type
     * {@link FuzzyWeightType}. The indicative/counter-indicative value of an
     * output value is associated with the attribute good/bad.
     */
    public TypedIOPort ethicsOutput;

    /**
     * Output for the aggregate ethics value over all features. The output is
     * of {@link FuzzyWeightType}. The indicative/counter-indicative value of an
     * output value is associated with the attribute good/bad.
     */
    public TypedIOPort ethicsOutputTotal;

    /**
     * Output for the encoded affordances values of the features. The output is of
     * type {@link BaseType#RECORD}, where the values are of type
     * {@link FuzzyWeightType}. The indicative/counter-indicative value of an
     * output value is associated with the attribute aid/obstacle.
     */
    public TypedIOPort affordancesOutput;

    /**
     * Output for the aggregate affordances value over all features. The output is
     * of {@link FuzzyWeightType}. The indicative/counter-indicative value of an
     * output value is associated with the attribute aid/obstacle.
     */
    public TypedIOPort affordancesOutputTotal;

    /**
     * Output for the encoded aesthetics values of the features. The output is of
     * type {@link BaseType#RECORD}, where the values are of type
     * {@link FuzzyWeightType}. The indicative/counter-indicative value of an
     * output value is associated with the attribute beautiful/ugly.
     */
    public TypedIOPort aestheticsOutput;

    /**
     * Output for the aggregate aestheticsvalue over all features. The output is
     * of {@link FuzzyWeightType}. The indicative/counter-indicative value of an
     * output value is associated with the attribute beautiful/ugly.
     */
    public TypedIOPort aestheticsOutputTotal;

    /**
     * Output for the encoded epistemics values of the features. The output is of
     * type {@link BaseType#RECORD}, where the values are of type
     * {@link FuzzyWeightType}. The indicative/counter-indicative value of an
     * output value is associated with the attribute realistic/unrealistic.
     */
    public TypedIOPort epistemicsOutput;

    /**
     * Output for the epistemics value over all features. The output is
     * of {@link FuzzyWeightType}. The indicative/counter-indicative value of an
     * output value is associated with the attribute realistic/unrealistic.
     */
    public TypedIOPort epistemicsOutputTotal;

    public TypedIORelation featureRelation;

    public DomainEncoder ethicsEncoder;
    public DomainEncoder affordancesEncoder;
    public DomainEncoder aestheticsEncoder;
    public DomainEncoder epistemicsEncoder;

    public Encoding(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace);
        init();
    }

    public Encoding(CompositeEntity container, String name) throws NameDuplicationException, IllegalActionException {
        super(container, name);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        this.featuresInput = createRecordInputPort(this, FEATURES_IN);
        this.featuresInput.setDisplayName(FEATURES_IN_DISP);

        this.featureRelation = new TypedIORelation(this, FEATURE_RELATION);
        this.featuresInput.link(featureRelation);

        this.ethicsOutput = createRecordOutputPort(this, DomainPort.ETHICS.outputName);
        this.ethicsOutput.setDisplayName(DomainPort.ETHICS.outputDisplayName);
        this.ethicsOutputTotal = createTypedOutputPort(this, DomainPort.ETHICS.outputNameTotal, FUZZY_WEIGHT);
        this.ethicsOutputTotal.setDisplayName(DomainPort.ETHICS.outputDisplayNameTotal);

        this.affordancesOutput = createRecordOutputPort(this, DomainPort.AFFORDANCES.outputName);
        this.affordancesOutput.setDisplayName(DomainPort.AFFORDANCES.outputDisplayName);
        this.affordancesOutputTotal = createTypedOutputPort(this, DomainPort.AFFORDANCES.outputNameTotal, FUZZY_WEIGHT);
        this.affordancesOutputTotal.setDisplayName(DomainPort.AFFORDANCES.outputDisplayNameTotal);

        this.aestheticsOutput = createRecordOutputPort(this, DomainPort.AESTHETICS.outputName);
        this.aestheticsOutput.setDisplayName(DomainPort.AESTHETICS.outputDisplayName);
        this.aestheticsOutputTotal = createTypedOutputPort(this, DomainPort.AESTHETICS.outputNameTotal, FUZZY_WEIGHT);
        this.aestheticsOutputTotal.setDisplayName(DomainPort.AESTHETICS.outputDisplayNameTotal);

        this.epistemicsOutput = createRecordOutputPort(this, DomainPort.EPISTEMICS.outputName);
        this.epistemicsOutput.setDisplayName(DomainPort.EPISTEMICS.outputDisplayName);
        this.epistemicsOutputTotal = createTypedOutputPort(this, DomainPort.EPISTEMICS.outputNameTotal, FUZZY_WEIGHT);
        this.epistemicsOutputTotal.setDisplayName(DomainPort.EPISTEMICS.outputDisplayNameTotal);

        this.ethicsEncoder = new LookupDomainEncoder(this, ETHICS_ENCODER);
        this.ethicsEncoder.setDisplayName(ETHICS_ENCODER_DISP);
        this.affordancesEncoder = new LookupDomainEncoder(this, AFFORDANCES_ENCODER);
        this.affordancesEncoder.setDisplayName(AFFORDANCES_ENCODER_DISP);
        this.aestheticsEncoder = new LookupDomainEncoder(this, AESTHETICS_ENCODER);
        this.aestheticsEncoder.setDisplayName(AESTHETICS_ENCODER_DISP);
        this.epistemicsEncoder = new LookupDomainEncoder(this, EPISTEMICS_ENCODER);
        this.epistemicsEncoder.setDisplayName(EPISTEMICS_ENCODER_DISP);

        this.ethicsEncoder.getFeatureInput().link(featureRelation);
        this.affordancesEncoder.getFeatureInput().link(featureRelation);
        this.aestheticsEncoder.getFeatureInput().link(featureRelation);
        this.epistemicsEncoder.getFeatureInput().link(featureRelation);

        connect(ethicsOutput, ethicsEncoder.getOutput());
        connect(ethicsOutputTotal, ethicsEncoder.getOutputAggregate());
        connect(affordancesOutput, affordancesEncoder.getOutput());
        connect(affordancesOutputTotal, affordancesEncoder.getOutputAggregate());
        connect(aestheticsOutput, aestheticsEncoder.getOutput());
        connect(aestheticsOutputTotal, aestheticsEncoder.getOutputAggregate());
        connect(epistemicsOutput, epistemicsEncoder.getOutput());
        connect(epistemicsOutputTotal, epistemicsEncoder.getOutputAggregate());
    }
}
