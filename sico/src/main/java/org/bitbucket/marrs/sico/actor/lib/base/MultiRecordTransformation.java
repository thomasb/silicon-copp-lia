package org.bitbucket.marrs.sico.actor.lib.base;

import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordInputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordOutputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createTypedOutputPort;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ptolemy.actor.TypedAtomicActor;
import ptolemy.actor.TypedIOPort;
import ptolemy.actor.TypedIOPort.RunTimeTypeCheckException;
import ptolemy.data.RecordToken;
import ptolemy.data.Token;
import ptolemy.data.type.BaseType;
import ptolemy.data.type.RecordType;
import ptolemy.data.type.Type;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

import com.google.common.collect.ImmutableList;

/**
 * This is a base class for actors that transform multiple input streams of
 * records into an output stream of records. It provides input ports, an output port
 * and an aggregate output port. For each entry in the input records a transformation
 * is applied with the values of the input records as arguments. All records are
 * processed independently. Subclasses can specify the value transformation and the
 * aggregate function that is used to create the aggregate output.
*/
// This class is not abstract in order to support the documentation system.
public class MultiRecordTransformation<T extends Token> extends TypedAtomicActor {

    private static final String OUT_DEFAULT = "output";
    private static final String AGGREGATE_OUT_DEFAULT = "outputAggregate";

    /**
     *  The input ports.  The inputs must be of {@link RecordType}. The type of
     *  the values of the record entries is checked at runtime.
     */
    private List<TypedIOPort> inputs = new ArrayList<>();

    /**
     * The output port. The type of the output is {@link RecordType} and the
     * type of the values in record entries is specified by subclasses.
     */
    public TypedIOPort output;

    /**
     * The aggregate output port. The type of the output is {@link RecordType}
     * and the type of the values in record entries is specified by subclasses.
     * It provides an aggregate output over all values in the output record.
     */
    public TypedIOPort outputAggregate;

    private List<Type> valueTypes;
    private Type outputAggregateType;

    /**
     * Constructor for internal use (cloning and documentation system).
     *
     * @param workspace the workspace
     *
     * @throws IllegalActionException if the actor cannot be contained
     *  by the proposed workspace
     * @throws NameDuplicationException if there is a naming conflict
     */
    public MultiRecordTransformation(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        this(workspace, Collections.emptyList(), BaseType.DOUBLE, BaseType.DOUBLE);
    }

    /**
     * Constructor for internal use (cloning and documentation system).
     *
     * @param workspace the workspace
     * @param inputNames the names of the input ports
     * @param aggregateOutputType the type of the aggregate output port
     * @param valueType the type of the values in the records in the input
     *  ports
     *
     * @throws IllegalActionException if the actor cannot be contained
     *  by the proposed workspace
     * @throws NameDuplicationException if there is a naming conflict
     */
    protected MultiRecordTransformation(Workspace workspace,
            List<String> inputNames,
            Type aggregateOutputType,
            Type valueType) throws IllegalActionException, NameDuplicationException {
        this(workspace, inputNames, aggregateOutputType, Collections.nCopies(inputNames.size(), valueType));
    }

    /**
     * Constructor for internal use (cloning and documentation system).
     *
     * @param workspace the workspace
     * @param inputNames the names of the input ports
     * @param aggregateOutputType the type of the aggregate output port
     * @param valueTypes the type of the values in the records in the input
     *  ports in the order they are defined in the inputNames argument
     *
     * @throws IllegalActionException if the actor cannot be contained
     *  by the proposed workspace
     * @throws NameDuplicationException if there is a naming conflict
     */
    protected MultiRecordTransformation(Workspace workspace,
            List<String> inputNames,
            Type aggregateOutputType,
            List<Type> valueTypes) throws IllegalActionException, NameDuplicationException {
        super(workspace);
        init(inputNames, aggregateOutputType, valueTypes);
    }

    /**
     * Construct an actor with the given container name.
     * <p>
     * The created {@code MultiRecordTransformation} has no input ports.
     *
     * @param container the container
     * @param name the name of this actor
     *
     * @throws IllegalActionException if the actor cannot be contained
     *  by the proposed container
     * @throws NameDuplicationException if there is a naming conflict
     */
    public MultiRecordTransformation(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        this(container, name, Collections.emptyList(), BaseType.DOUBLE, BaseType.DOUBLE);
    }

    /**
     * Construct an actor with the given container name, port names and types.
     * <p>
     * The order of the port names defines the index of the input ports in the
     * {@link #getInput(int)} method.
     *
     * @param container the container
     * @param name the name of this actor
     * @param inputNames the names of the input ports
     * @param aggregateOutputType the type of the aggregate output port
     * @param valueType the type of the values in the records in the input
     *  ports
     *
     * @throws IllegalActionException if the actor cannot be contained
     *  by the proposed container
     * @throws NameDuplicationException if the container already has an
     *  actor with this name
     */
    protected MultiRecordTransformation(CompositeEntity container,
            String name,
            List<String> inputNames,
            Type aggregateOutputType,
            Type valueType) throws IllegalActionException, NameDuplicationException {
        this(container, name, inputNames, aggregateOutputType, Collections.nCopies(inputNames.size(), valueType));
    }

    /**
     * Construct an actor with the given container name, port names and types.
     * <p>
     * The order of the port names defines the index of the input ports in the
     * {@link #getInput(int)} method.
     *
     * @param container the container
     * @param name the name of this actor
     * @param inputNames the names of the input ports
     * @param aggregateOutputType the type of the aggregate output port
     * @param valueTypes the type of the values in the records in the input
     *  ports in the order they are defined in the inputNames argument
     *
     * @throws IllegalActionException if the actor cannot be contained
     *  by the proposed container
     * @throws NameDuplicationException if the container already has an
     *  actor with this name
     */
    protected MultiRecordTransformation(CompositeEntity container,
            String name,
            List<String> inputNames,
            Type aggregateOutputType,
            List<Type> valueTypes) throws IllegalActionException, NameDuplicationException {
        super(container, name);
        init(inputNames, aggregateOutputType, valueTypes);
    }

    private void init(List<String> inputNames, Type aggregateOutputType, List<Type> valueTypeList) throws IllegalActionException, NameDuplicationException {
        for (String inputName : inputNames) {
            inputs.add(createRecordInputPort(this, inputName));
        }

        this.output = createRecordOutputPort(this, OUT_DEFAULT);
        this.outputAggregate = createTypedOutputPort(this, AGGREGATE_OUT_DEFAULT, aggregateOutputType);
        this.outputAggregateType = aggregateOutputType;
        this.valueTypes = valueTypeList;
    }

    /**
     * Defines a custom transformation for the values in the input records.
     * <p>
     * The returned transformation is applied to each value token in the input
     * records independently. The type of the input tokens is guaranteed to be
     * compatible with the value type specified in the constructor. The returned
     * Transformation must be able to handle {@link BaseType#NIL} values though.
     *
     * @return a {@link Transformation} for the tokens in the input records
     *
     * @throws IllegalActionException if an error occurs during the transformation
     */
    protected Transformation<Token, T> getTransformation() throws IllegalActionException {
        // This class is not abstract in order to support the documentation system.
        throw new UnsupportedOperationException("Override with custom implementation");
    }

    /**
     * Defines a custom aggregation function over the values in the output record.
     * <p>
     * The type of the aggregate value must be the type specified in the
     * constructor. An example is the average or the sum of of all values in the
     * output record.
     *
     * @param outputs the values of the output record.
     *
     * @return a token with the aggregate value. The type of the aggregate
     *  value must be the type specified in the constructor.
     *
     * @throws IllegalActionException if an error occurs during the transformation
     */
    protected Token aggregateOutput(Collection<T> outputs) throws IllegalActionException {
        // This class is not abstract in order to support the documentation system.
        throw new UnsupportedOperationException("Override with custom implementation");
    }

    @Override
    public void fire() throws IllegalActionException {
        super.fire();

        List<RecordToken> inputRecords = readInputRecords();

        Map<String, T> transformedMap = applyValueTransformation(inputRecords);

        output.send(0, asRecordToken(transformedMap));
        outputAggregate.send(0, checkedAggregateOutput(transformedMap.values()));
    }

    private List<RecordToken> readInputRecords() throws IllegalActionException {
        List<RecordToken> inputRecords = new ArrayList<>();
        for (TypedIOPort port : inputs) {
            RecordToken inputRecord = (RecordToken) port.get(0);
            inputRecords.add(inputRecord);
        }
        return inputRecords;
    }

    private Token checkedAggregateOutput(Collection<T> outputs) throws IllegalActionException {
        Token aggregateOutputToken = aggregateOutput(outputs);
        if (!outputAggregateType.isCompatible(aggregateOutputToken.getType())) {
            throw new RunTimeTypeCheckException(outputAggregate, aggregateOutputToken);
        }

        return aggregateOutputToken;
    }

    private Map<String, T> applyValueTransformation(List<RecordToken> inputRecords) throws IllegalActionException {
        Set<String> labels = new HashSet<>();
        for (RecordToken record : inputRecords) {
            labels.addAll(record.labelSet());
        }

        Transformation<Token, T> transformation = getTransformation();
        Map<String, T> transformed = new HashMap<>();
        for (String label : labels) {
            List<Token> values = getCecked(inputRecords, label);
            transformed.put(label, transformation.apply(label, values));
        }

        return transformed;
    }

    @SuppressWarnings("unchecked")
    private Token asRecordToken(Map<String, T> transformedMap) throws IllegalActionException {
        return new RecordToken((Map<String, Token>) transformedMap);
    }

    private List<Token> getCecked(List<RecordToken> inputRecords, String label) throws IllegalActionException {
        List<Token> values = new ArrayList<>();
        for (int i = 0; i < inputRecords.size(); i++) {
            RecordToken recordToken = inputRecords.get(i);
            Token inputValue = recordToken.get(label);
            if (inputValue == null) {
                values.add(Token.NIL);
            } else if (valueTypes.get(i).isCompatible(inputValue.getType())) {
                values.add(valueTypes.get(i).convert(inputValue));
            } else {
                throw new RunTimeTypeCheckException(inputs.get(i), inputValue);
            }
        }

        return values;
    }

    protected final TypedIOPort getInput(int index) {
        return inputs.get(index);
    }

    public final TypedIOPort getOutput() {
        return output;
    }

    public final TypedIOPort getOutputAggregate() {
        return outputAggregate;
    }

    @Override
    public Object clone(Workspace workspace) throws CloneNotSupportedException {
        MultiRecordTransformation<?> clone = (MultiRecordTransformation<?>) super.clone(workspace);
        clone.inputs = ImmutableList.copyOf(inputs);
        clone.valueTypes = ImmutableList.copyOf(valueTypes);
        clone.outputAggregateType = outputAggregateType;

        return clone;
    }

    /**
     * A {@code Transformation} represents a function that transforms a list of
     * values of type T to an output of type U.
     *
     * @param <T> input type
     * @param <U> output type
     */
    protected interface Transformation<T, U> {
        U apply(String label, List<T> arguments) throws IllegalActionException;
    }
}

