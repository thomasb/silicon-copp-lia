package org.bitbucket.marrs.sico.algo.respond;

import static java.lang.Math.max;


public final class IDTFunction {

    private final double weight;

    public IDTFunction(double weight) {
        this.weight = weight;
    }

    public double apply(double involvement, double distance) {
        double maximum = max(involvement, distance);
        double average = 0.5 * (involvement + distance);

        return weight * maximum + (1 - weight) * average;
    }
}
