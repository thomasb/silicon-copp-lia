package org.bitbucket.marrs.sico.data.token;

import static org.bitbucket.marrs.sico.data.type.FuzzyWeightType.FUZZY_WEIGHT;

import org.bitbucket.marrs.sico.data.type.FuzzyWeightType;

import ptolemy.data.DoubleToken;
import ptolemy.data.RecordToken;
import ptolemy.data.Token;
import ptolemy.data.type.Type;
import ptolemy.kernel.util.IllegalActionException;

public class FuzzyWeightToken extends RecordToken {

    @SuppressWarnings("hiding")
    public static final FuzzyWeightToken NIL;
    static {
        try {
            NIL = new FuzzyWeightToken(DoubleToken.ZERO, DoubleToken.ZERO) {
                @Override
                public boolean isNil() {
                    return true;
                }
            };
        } catch (Exception e) {
            // This should never happen.
            throw new RuntimeException(e);
        }
    }

    public FuzzyWeightToken(double positive, double negative) throws IllegalActionException {
        this(new DoubleToken(positive), new DoubleToken(negative));
    }

    public FuzzyWeightToken(Token positive, Token negative) throws IllegalActionException {
        this(DoubleToken.convert(positive), DoubleToken.convert(negative));
    }

    public FuzzyWeightToken(DoubleToken positive, DoubleToken negative) throws IllegalActionException {
        super(FuzzyWeightType.LABELS, new Token[] { checkRange(positive, "positive"), checkRange(negative, "negative") });
    }

    @Override
    public Type getType() {
        return FUZZY_WEIGHT;
    }

    public DoubleToken getPositiveWeightToken() {
        return ((DoubleToken) get(FuzzyWeightType.POSITIVE));
    }

    public DoubleToken getNegativeWeightToken() {
        return ((DoubleToken) get(FuzzyWeightType.NEGATIVE));
    }

    public double getPositiveWeight() {
        return getPositiveWeightToken().doubleValue();
    }

    public double getNegativeWeight() {
        return getNegativeWeightToken().doubleValue();
    }

    private static DoubleToken checkRange(DoubleToken token, String name) throws IllegalActionException {
        double value = token.doubleValue();
        if (value < 0.0 || 1.0 < value) {
            throw new IllegalActionException(name + " weight out of range: " + value);
        }

        return token;
    }
}
