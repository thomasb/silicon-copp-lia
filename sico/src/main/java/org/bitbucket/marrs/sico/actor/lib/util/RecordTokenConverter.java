package org.bitbucket.marrs.sico.actor.lib.util;

import static java.util.stream.Collectors.toMap;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import ptolemy.data.DoubleToken;
import ptolemy.data.IntToken;
import ptolemy.data.RecordToken;
import ptolemy.data.Token;
import ptolemy.kernel.util.IllegalActionException;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

public abstract class RecordTokenConverter<T> {

    private RecordTokenConverter() {
        //prevent further subtyping
    }

    public static RecordTokenConverter<Integer> intMapConverter() {
        return new RecordTokenConverter<Integer>() {
            @Override
            protected Integer convertToken(Token token) throws IllegalActionException {
                return IntToken.convert(token).intValue();
            }

            @Override
            protected Token convertValue(Integer value) {
                return new IntToken(value);
            }
        };
    }

    public static RecordTokenConverter<Double> doubleMapConverter() {
        return new RecordTokenConverter<Double>() {
            @Override
            protected Double convertToken(Token token) throws IllegalActionException {
                return DoubleToken.convert(token).doubleValue();
            }

            @Override
            protected Token convertValue(Double value) {
                return new DoubleToken(value);
            }
        };
    }

    public Map<String, T> convert(Token record) throws IllegalActionException {
        try {
            return convert((RecordToken) record);
        } catch (Exception e) {
            throw new IllegalActionException("Cannot cast token to RecordToken: " + record.getType());
        }
    }

    public Map<String, T> convert(RecordToken record) throws IllegalActionException {
        Set<String> labelSet = record.labelSet();
        Builder<String, T> builder = ImmutableMap.builder();
        for (String label : labelSet) {
            builder.put(label, convertToken(record.get(label)));
        }

        return builder.build();
    }

    public RecordToken convert(Map<String, T> map) throws IllegalActionException {
        Map<String, Token> recordMap = map.entrySet().stream()
                .collect(toMap(Entry::getKey, e -> convertValue(e.getValue())));

        return new RecordToken(recordMap);
    }

    protected abstract T convertToken(Token token) throws IllegalActionException;

    protected abstract Token convertValue(T value);
}
