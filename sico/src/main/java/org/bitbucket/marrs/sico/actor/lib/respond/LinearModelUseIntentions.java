package org.bitbucket.marrs.sico.actor.lib.respond;

import static org.bitbucket.marrs.sico.actor.lib.util.ParameterUtil.createStringParameter;
import static org.bitbucket.marrs.sico.actor.lib.util.ParameterUtil.stringValue;
import static org.bitbucket.marrs.sico.data.type.FuzzyWeightType.FUZZY_WEIGHT;

import java.util.List;
import java.util.Map;

import org.bitbucket.marrs.sico.actor.lib.base.DomainPort;
import org.bitbucket.marrs.sico.actor.lib.base.LinearModel;
import org.bitbucket.marrs.sico.data.token.FuzzyWeightToken;
import org.bitbucket.marrs.sico.data.type.FuzzyWeightType;

import ptolemy.actor.TypedIOPort;
import ptolemy.data.Token;
import ptolemy.data.expr.Parameter;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

import com.google.common.collect.ImmutableList;

/**
 * Actor that evaluates the use intentions of the agent based on a generalized
 * linear model.
 * <p>
 * The actor calculates the indicative and counter-indicative values for the
 * use intentions of the agent towards the features as indicative
 * based on the relevance, valence, similarity values as well as the encoded
 * values from the aesthetics domain. The values are calculated based on a
 * generalized linear model.
 */
public final class LinearModelUseIntentions extends LinearModel {

    public static final int MODEL_DIM = 10;

    private static final String RELEVANCE_IN = "relevanceInput";
    private static final String RELEVANCE_IN_DISP = "Relevance";
    private static final String VALENCE_IN = "valenceInput";
    private static final String VALENCE_IN_DISP = "Valence";
    private static final String SIMILARITY_IN = "similarityInput";
    private static final String SIMILARITY_IN_DISP = "Similarity";

    private static final List<String> INPUTS = ImmutableList.of(
            RELEVANCE_IN,
            VALENCE_IN,
            DomainPort.AESTHETICS.inputName,
            SIMILARITY_IN);

    private static final String UI_POS = "uiPos";
    private static final String UI_NEG = "uiNeg";
    private static final String SIM_UI_POS = "simUiPos";
    private static final String SIM_UI_NEG = "simUiNeg";
    private static final String DIS_UI_POS = "disUiPos";
    private static final String DIS_UI_NEG = "disUiNeg";
    private static final String BEA_UI_POS = "beaUiPos";
    private static final String BEA_UI_NEG = "beaUiNeg";
    private static final String UGL_UI_POS = "uglUiPos";
    private static final String UGL_UI_NEG = "uglUiNeg";

    private static final String POSTFIX = "Param";

    /**
     * Parameter defining the key of the parameter for indicative
     * use-intentions value in the {@link LinearModel#rowMappingInput}.
     */
    public Parameter uiPosParam;

    /**
     * Parameter defining the key of the parameter for counter-indicative
     * use-intentions value in the {@link LinearModel#rowMappingInput}.
     */
    public Parameter uiNegParam;

    /**
     * Parameter defining the key of the parameter for the product of the
     * similarity value and the indicative use-intentions value in the
     * {@link LinearModel#rowMappingInput}.
     */
    public Parameter simUiPosParam;

    /**
     * Parameter defining the key of the parameter for the product of the
     * similarity value and the counter-indicative use-intentions value in the
     * {@link LinearModel#rowMappingInput}.
     */
    public Parameter simUiNegParam;

    /**
     * Parameter defining the key of the parameter for the product of the
     * dissimilarity value and the indicative use-intentions value in the
     * {@link LinearModel#rowMappingInput}.
     */
    public Parameter disUiPosParam;

    /**
     * Parameter defining the key of the parameter for the product of the
     * dissimilarity value and the counter-indicative use-intentions value in the
     * {@link LinearModel#rowMappingInput}.
     */
    public Parameter disUiNegParam;

    /**
     * Parameter defining the key of the parameter for the product of the
     * beauty value and the indicative use-intentions value in the
     * {@link LinearModel#rowMappingInput}.
     */
    public Parameter beaUiPosParam;

    /**
     * Parameter defining the key of the parameter for the product of the
     * beauty value and the counter-indicative use-intentions value in the
     * {@link LinearModel#rowMappingInput}.
     */
    public Parameter beaUiNegParam;

    /**
     * Parameter defining the key of the parameter for the product of the
     * ugly value and the indicative use-intentions value in the
     * {@link LinearModel#rowMappingInput}.
     */
    public Parameter uglUiPosParam;

    /**
     * Parameter defining the key of the parameter for the product of the
     * ugly value and the counter-indicative use-intentions value in the
     * {@link LinearModel#rowMappingInput}.
     */
    public Parameter uglUiNegParam;

    /**
     * Input port for the relevance values of the features. The input port is
     * of type {@link BaseType#RECORD} with values of type
     * {@link FuzzyWeightType}.
     */
    public TypedIOPort relevanceInput;

    /**
     * Input port for the valence values of the features. The input port is
     * of type {@link BaseType#RECORD} with values of type
     * {@link FuzzyWeightType}.
     */
    public TypedIOPort valenceInput;

    /**
     * Input port for the encoded values of the features in the aesthetics domain.
     * The input port is of type {@link BaseType#RECORD} with values of type
     * {@link FuzzyWeightType} containing the encoded weights of the ethics
     * domains.
     */
    public TypedIOPort aestheticsInput;

    /**
     * Input port for the similarity values of the features. The input port is
     * of type {@link BaseType#RECORD} with values of type
     * {@link FuzzyWeightType}.
     */
    public TypedIOPort similarityInput;

    public LinearModelUseIntentions(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace, INPUTS, FUZZY_WEIGHT, MODEL_DIM);
        init();
    }

    public LinearModelUseIntentions(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        super(container, name, INPUTS, FUZZY_WEIGHT, MODEL_DIM);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        this.uiPosParam = createStringParameter(this, UI_POS + POSTFIX, UI_POS);
        this.uiNegParam = createStringParameter(this, UI_NEG + POSTFIX, UI_NEG);
        this.simUiPosParam = createStringParameter(this, SIM_UI_POS + POSTFIX, SIM_UI_POS);
        this.simUiNegParam = createStringParameter(this, SIM_UI_NEG + POSTFIX, SIM_UI_NEG);
        this.disUiPosParam = createStringParameter(this, DIS_UI_POS + POSTFIX, DIS_UI_POS);
        this.disUiNegParam = createStringParameter(this, DIS_UI_NEG + POSTFIX, DIS_UI_NEG);
        this.beaUiPosParam = createStringParameter(this, BEA_UI_POS + POSTFIX, BEA_UI_POS);
        this.beaUiNegParam = createStringParameter(this, BEA_UI_NEG + POSTFIX, BEA_UI_NEG);
        this.uglUiPosParam = createStringParameter(this, UGL_UI_POS + POSTFIX, UGL_UI_POS);
        this.uglUiNegParam = createStringParameter(this, UGL_UI_NEG + POSTFIX, UGL_UI_NEG);

        this.relevanceInput = getInput(0);
        this.relevanceInput.setDisplayName(RELEVANCE_IN_DISP);

        this.valenceInput = getInput(1);
        this.valenceInput.setDisplayName(VALENCE_IN_DISP);

        this.aestheticsInput = getInput(2);
        this.aestheticsInput.setDisplayName(DomainPort.AESTHETICS.inputDisplayName);

        this.similarityInput = getInput(3);
        this.similarityInput.setDisplayName(SIMILARITY_IN_DISP);
    }

    @Override
    protected double[] parseArgumentsOrdered(List<Token> args, Map<String, Integer> rowMapping) throws IllegalActionException {
        double[] orderedArguments = new double[MODEL_DIM];

        FuzzyWeightToken relevanceToken = FUZZY_WEIGHT.convert(args.get(0));
        FuzzyWeightToken valenceToken = FUZZY_WEIGHT.convert(args.get(1));
        FuzzyWeightToken similarityToken = FUZZY_WEIGHT.convert(args.get(3));
        FuzzyWeightToken aestheticsToken = FUZZY_WEIGHT.convert(args.get(2));

        double useIntentionsPos = relevanceToken.getPositiveWeight() * valenceToken.getPositiveWeight();
        double useIntentionsNeg = relevanceToken.getNegativeWeight() * valenceToken.getNegativeWeight();
        double beatuiful = aestheticsToken.getPositiveWeight();
        double ugly = aestheticsToken.getNegativeWeight();
        double similar = similarityToken.getPositiveWeight();
        double dissimilar = similarityToken.getNegativeWeight();

        orderedArguments[getChecked(rowMapping, stringValue(uiPosParam))] = useIntentionsPos;
        orderedArguments[getChecked(rowMapping, stringValue(uiNegParam))] = useIntentionsNeg;

        orderedArguments[getChecked(rowMapping, stringValue(simUiPosParam))] = useIntentionsPos * similar;
        orderedArguments[getChecked(rowMapping, stringValue(simUiNegParam))] = useIntentionsNeg * similar;
        orderedArguments[getChecked(rowMapping, stringValue(disUiPosParam))] = useIntentionsPos * dissimilar;
        orderedArguments[getChecked(rowMapping, stringValue(disUiNegParam))] = useIntentionsNeg * dissimilar;

        orderedArguments[getChecked(rowMapping, stringValue(beaUiPosParam))] = useIntentionsPos * beatuiful;
        orderedArguments[getChecked(rowMapping, stringValue(beaUiNegParam))] = useIntentionsNeg * beatuiful;
        orderedArguments[getChecked(rowMapping, stringValue(uglUiPosParam))] = useIntentionsPos * ugly;
        orderedArguments[getChecked(rowMapping, stringValue(uglUiNegParam))] = useIntentionsNeg * ugly;

        return orderedArguments;
    }
}
