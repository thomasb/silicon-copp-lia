package org.bitbucket.marrs.sico.actor.lib.util;

import ptolemy.actor.TypedIOPort;
import ptolemy.data.type.BaseType;
import ptolemy.data.type.Type;
import ptolemy.kernel.ComponentEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;

public class PortUtil {

    public static TypedIOPort createRecordInputPort(ComponentEntity<?> container, String name)
            throws IllegalActionException, NameDuplicationException {
        TypedIOPort port = new TypedIOPort(container, name, true, false);
        port.setMultiport(false);
        port.setTypeAtMost(BaseType.RECORD);

        return port;
    }

    public static TypedIOPort createRecordOutputPort(ComponentEntity<?> container, String name)
            throws IllegalActionException, NameDuplicationException {
        return createTypedOutputPort(container, name, BaseType.RECORD);
    }

    public static TypedIOPort createTypedInputPort(ComponentEntity<?> container, String name, Type type)
                    throws IllegalActionException, NameDuplicationException {
        TypedIOPort port = new TypedIOPort(container, name, true, false);
        port.setMultiport(false);
        port.setTypeEquals(type);

        return port;
    }

    public static TypedIOPort createTypedOutputPort(ComponentEntity<?> container, String name, Type type)
            throws IllegalActionException, NameDuplicationException {
        TypedIOPort port = new TypedIOPort(container, name, false, true);
        port.setMultiport(false);
        port.setTypeEquals(type);

        return port;
    }
}
