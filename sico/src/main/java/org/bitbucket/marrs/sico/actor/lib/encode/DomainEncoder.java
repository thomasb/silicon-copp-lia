package org.bitbucket.marrs.sico.actor.lib.encode;

import static org.bitbucket.marrs.sico.data.type.FuzzyWeightType.FUZZY_WEIGHT;

import org.bitbucket.marrs.sico.actor.lib.base.MultiRecordTransformation;
import org.bitbucket.marrs.sico.data.token.FuzzyWeightToken;

import ptolemy.actor.TypedIOPort;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

import com.google.common.collect.ImmutableList;

/**
 * Base class for actors that create an encoding of the features in a domain.
 * <p>
 * This base class provides a feature input.
 */
public class DomainEncoder extends MultiRecordTransformation<FuzzyWeightToken> {

    private static final String FEATURES_IN = "featuresInput";
    private static final String FEATURES_IN_DISP = "Features";

    /**
     * Input port for the features. The port is of type {@link BaseType#RECORD}
     * with values of type {@link BaseType#DOUBLE}.
     */
    public TypedIOPort featuresInput;

    public DomainEncoder(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace);
        init();
    }

    public DomainEncoder(CompositeEntity container, String name) throws NameDuplicationException, IllegalActionException {
        super(container, name, ImmutableList.of(FEATURES_IN), FUZZY_WEIGHT, BaseType.DOUBLE);
        init();
    }

    private void init() {
        this.featuresInput = getInput(0);
        this.featuresInput.setDisplayName(FEATURES_IN_DISP);
        this.featuresInput.setTypeAtMost(BaseType.RECORD);
    }

    public TypedIOPort getFeatureInput() {
        return featuresInput;
    }
}
