package org.bitbucket.marrs.sico.actor.lib.util;

import java.util.List;

import org.bitbucket.marrs.sico.selemca.entity.ConceptMeta;

import ptolemy.kernel.util.IllegalActionException;

public final class ConceptMetaUtil {

    public static ConceptMeta find(String relation, List<ConceptMeta> selfMeta) throws IllegalActionException {
        return selfMeta.stream()
                .filter(m -> m.getRelation().equals(relation))
                .findFirst()
                .orElseThrow(() -> new IllegalActionException("No metadata with relation " + relation + " for self"));
    }
}
