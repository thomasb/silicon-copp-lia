package org.bitbucket.marrs.sico.actor.lib.connect;

import static java.util.Collections.emptyList;

import java.lang.reflect.Type;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import org.bitbucket.marrs.sico.actor.lib.compare.Relevance;
import org.bitbucket.marrs.sico.actor.lib.util.JsonFileLookup;
import org.bitbucket.marrs.sico.selemca.entity.Association;
import org.bitbucket.marrs.sico.selemca.entity.AssociationMeta;
import org.bitbucket.marrs.sico.selemca.entity.ConceptMeta;

import ptolemy.kernel.util.IllegalActionException;

import com.google.common.reflect.TypeToken;

/**
 * FileAppraisalDataConnector provides data external data from files to the model.
 * <p>
 * The data in the files needs to be provided in JSON format. The data expected
 * includes:
 * <ul>
 *      <li>Actions associated with a feature</li>
 *      <li>The type of the actions</li>
 *      <li>Goals associated with actions</li>
 *      <li>Transition weights for the action - goal associations</li>
 *      <li>Ambitions weights for the goals</li>
 *      <li>Ambitions weights for the goals</li>
 *      <li>A weight matrix for the {@link Relevance} actor</li>
 *      <li>A row mapping associated with the weight matrix</li>
 * </ul>
 * Example files can be found in the <a href="file://data/">data folder</a>.
 */
final class FileDataSource implements DataSource {

    private static final Type CONCEPT_META_MAP_TYPE = new TypeToken<Map<String, List<ConceptMeta>>>() {/**/}.getType();
    private static final Type ASSOCIATION_MAP_TYPE = new TypeToken<Map<String, List<Association>>>() {/**/}.getType();
    private static final Type ASSOCIATION_META_MAP_TYPE = new TypeToken<Map<String, List<AssociationMeta>>>() {/**/}.getType();

    private static final String ASSOCIATRION_KEY_FORMAT = "[%s,%s]";

    private static final String ACTION_TYPE_RELATION = "action_type";
    private static final String GOAL_TYPE_RELATION = "goal_ambition";
    private static final String TRANSITION_WEIGTH_RELATION = "transition_weight";

    private static final String SELF_KEY = "self";

    private final JsonFileLookup<List<Association>, ?> actionsLookup;
    private final JsonFileLookup<List<ConceptMeta>, ?> actionMetaLookup;
    private final JsonFileLookup<List<Association>, ?> actionGoalsLookup;
    private final JsonFileLookup<List<AssociationMeta>, ?> actionGoalsMetaLookup;
    private final JsonFileLookup<List<ConceptMeta>, ?> goalMetaLookup;
    private final JsonFileLookup<List<ConceptMeta>, ?> selfLookup;

    FileDataSource(URI actionsFilePath,
            URI actionMetaFilePath,
            URI actionGoalsFilePath,
            URI actionGoalsMetaFilePath,
            URI goalMetaFilePath,
            URI selfFilePath) {
        this.actionsLookup = JsonFileLookup.create(actionsFilePath, ASSOCIATION_MAP_TYPE);
        this.actionMetaLookup = JsonFileLookup.create(actionMetaFilePath, CONCEPT_META_MAP_TYPE);
        this.actionGoalsLookup = JsonFileLookup.create(actionGoalsFilePath, ASSOCIATION_MAP_TYPE);
        this.actionGoalsMetaLookup = JsonFileLookup.create(actionGoalsMetaFilePath, ASSOCIATION_META_MAP_TYPE);
        this.goalMetaLookup = JsonFileLookup.create(goalMetaFilePath, CONCEPT_META_MAP_TYPE);
        this.selfLookup = JsonFileLookup.create(selfFilePath, CONCEPT_META_MAP_TYPE);
    }

    @Override
    public List<Association> queryActions(String featureName) {
        return actionsLookup.get(featureName).orElse(emptyList());
    }

    @Override
    public Optional<ConceptMeta> queryActionMeta(String actionName) throws IllegalActionException {
        Function<List<ConceptMeta>, Optional<ConceptMeta>> findAction = list -> list.stream()
                .filter(m -> m.getRelation().equalsIgnoreCase(ACTION_TYPE_RELATION))
                .findFirst();

        return actionMetaLookup.get(actionName)
                .map(findAction)
                .filter(Optional::isPresent)
                .map(Optional::get);
    }

    @Override
    public List<Association> queryGoals(String actionName) {
        return actionGoalsLookup.get(actionName).orElse(emptyList());
    }

    @Override
    public AssociationMeta queryActionGoalMeta(String actionName, String goalName) throws IllegalActionException {
        String key = String.format(ASSOCIATRION_KEY_FORMAT, actionName, goalName);

        return actionGoalsMetaLookup.get(key)
                .orElseThrow(() -> new IllegalActionException("No metadata for " + actionName + ", " + goalName))
                .stream()
                .filter(m -> m.getRelation().equalsIgnoreCase(TRANSITION_WEIGTH_RELATION))
                .findFirst()
                .orElseThrow(() -> new IllegalActionException("No metadata with relation " + TRANSITION_WEIGTH_RELATION + " for " + actionName + ", " + goalName));
    }

    @Override
    public Optional<ConceptMeta> queryGoalMeta(String goalName) throws IllegalActionException {
        Function<List<ConceptMeta>, Optional<ConceptMeta>> findAction = list -> list.stream()
                .filter(m -> m.getRelation().equalsIgnoreCase(GOAL_TYPE_RELATION))
                .findFirst();

        return goalMetaLookup.get(goalName)
                .map(findAction)
                .filter(Optional::isPresent)
                .map(Optional::get);
    }

    @Override
    public List<ConceptMeta> querySelf() throws IllegalActionException {
        return selfLookup.get(SELF_KEY)
                .orElseThrow(() -> new IllegalActionException("No data found for '" + SELF_KEY + "'"));
    }
}
