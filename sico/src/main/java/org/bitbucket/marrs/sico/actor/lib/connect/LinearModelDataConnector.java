package org.bitbucket.marrs.sico.actor.lib.connect;

import static com.google.common.base.Preconditions.checkState;
import static java.util.stream.Collectors.toMap;
import static org.bitbucket.marrs.sico.actor.lib.util.ConceptMetaUtil.find;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordOutputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createTypedOutputPort;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bitbucket.marrs.sico.selemca.entity.ConceptMeta;

import ptolemy.actor.TypedIOPort;
import ptolemy.data.DoubleMatrixToken;
import ptolemy.data.IntToken;
import ptolemy.data.RecordToken;
import ptolemy.data.Token;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * {@code LinearModelDataConnector} provides parameter data for
 * actors based on linear models.
 * <p>
 * The data can be either retrieved from files or from an epistemics engine.
 * <p>
 * If the data is retrieved from files, the following files must be present in
 * the data directory:
 * <ul>
 *      <li>self.json</li>
 * </ul>
 */
public class LinearModelDataConnector extends SwitchableDataSourceConnector {

    private static final Type DOUBLE_LIST_TYPE = new TypeToken<List<Double>>() {/**/}.getType();
    private static final Type STRING_TO_INT_MAP_TYPE = new TypeToken<Map<String, Integer>>() {/**/}.getType();

    private static final Gson GSON = new Gson();

    /**
     * Output port for the mapping of the row indexes of the weight matrix
     * provided on the {@link #weightMatrixOutput} port to the parameter names
     * in the linear model. The port is of type {@link BaseType#RECORD} where
     * the keys represent the parameter names and the values are the row indices
     * in the weight matrix.
     */
    public TypedIOPort rowMappingOutput;

    /**
     * Output port for the weight matrix of the linear model. The port is of
     * type {@link BaseType#DOUBLE_MATRIX}. The parameter mapping for the row
     * indices of the matrix is provided separately on the
     * {@link #rowMappingOutput} port.
     */
    public TypedIOPort weightMatrixOutput;

    private int rows;
    private String matrixRelation;
    private String rowMappingRelation;

    public LinearModelDataConnector(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace);
        init();
    }

    public LinearModelDataConnector(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        super(container, name);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        weightMatrixOutput = createTypedOutputPort(this, "weightMatrixOutput", BaseType.DOUBLE_MATRIX);
        rowMappingOutput = createRecordOutputPort(this, "rowMappingOutput");
    }

    @Override
    public void fire() throws IllegalActionException {
        checkState(rows != 0 && matrixRelation != null && rowMappingRelation != null, "Weight matrix parameters must be set");

        super.fire();

        List<ConceptMeta> selfMeta = getDataSource().querySelf();

        ConceptMeta matrixMeta = find(matrixRelation, selfMeta);
        List<Double> matrixValues = GSON.fromJson(matrixMeta.getValue(), DOUBLE_LIST_TYPE);
        checkState(matrixValues != null, "Weight matrix could not be read: %s", matrixMeta);
        double[] matrixValueArray = matrixValues.stream().mapToDouble(Double::valueOf).toArray();

        ConceptMeta rowMappingMeta = find(rowMappingRelation, selfMeta);
        Map<String, Integer> mappingValues = GSON.fromJson(rowMappingMeta.getValue(), STRING_TO_INT_MAP_TYPE);
        checkState(mappingValues != null, "Row mapping values could not be read: %s", rowMappingMeta);
        Map<String, Token> tokenMapping = mappingValues.entrySet().stream()
                .collect(toMap(Entry::getKey, e -> new IntToken(e.getValue())));

        weightMatrixOutput.send(0, new DoubleMatrixToken(matrixValueArray, rows, 2));
        rowMappingOutput.send(0, new RecordToken(tokenMapping));
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public void setMatrixRelation(String matrixRelation) {
        this.matrixRelation = matrixRelation;
    }

    public void setRowMappingRelation(String rowMappingRelation) {
        this.rowMappingRelation = rowMappingRelation;
    }

    @Override
    public Object clone(Workspace workspace) throws CloneNotSupportedException {
        LinearModelDataConnector clone = (LinearModelDataConnector) super.clone(workspace);
        clone.rows = rows;
        clone.matrixRelation = matrixRelation;
        clone.rowMappingRelation = rowMappingRelation;

        return clone;
    }
}
