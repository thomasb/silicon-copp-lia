package org.bitbucket.marrs.sico.algo.compare;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.lang.Math.signum;
import static java.util.Arrays.stream;
import static java.util.Comparator.naturalOrder;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.maxBy;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public final class ValenceFunction {

    public ValenceResult calculate(double aidWeight, double obstacleWeight, List<ActionGoalTransition> transitions) {
        // We can use:
        //   max(min(a, c), min(b, c)) = min(max(a,b), c)
        // and
        //   max(a, b, c) = max(a, max(b, c))
        // to handle affordances separately
        Map<Integer, Optional<Double>> totalTransitionWeight = transitions.stream().collect(
                groupingBy(this::valenceSignature, mapping(this::ruleStrength, maxBy(naturalOrder()))));

        double featureStrenght = max(aidWeight, obstacleWeight);

        double positiveWeight = totalTransitionWeight.containsKey(1) ?
                min(featureStrenght, totalTransitionWeight.get(1).get()) : 0.0;
        double negativeWeight = totalTransitionWeight.containsKey(-1) ?
                min(featureStrenght, totalTransitionWeight.get(-1).get()) : 0.0;

        return new ValenceResult(positiveWeight, negativeWeight);
    }

    private int valenceSignature(ActionGoalTransition transition) {
        return (int) signum(transition.getWeight() * transition.getGoalAmbition() * transition.getTruthValue());
    }

    private Double ruleStrength(ActionGoalTransition transition) {
        return absMin(transition.getWeight(), transition.getGoalAmbition(), transition.getTruthValue());
    }

    private double absMin(double... values) {
        return stream(values).map(Math::abs).min().orElse(0.0);
    }

    public static final class ValenceResult {
        private final Double positive;
        private final Double negative;

        private ValenceResult(Double positive, Double negative) {
            this.positive = positive;
            this.negative = negative;
        }

        public Double getPositive() {
            return positive;
        }

        public Double getNegative() {
            return negative;
        }
    }
}
