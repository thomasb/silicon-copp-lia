package org.bitbucket.marrs.sico.algo.compare;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toMap;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;

import org.bitbucket.marrs.sico.algo.compare.ActionGoalTransition.ActionType;
import org.bitbucket.marrs.sico.algo.util.RecursiveMeanCollector;

public final class UtilityFunction {

    private static final Function<Map.Entry<ActionType, Double>, Double> GET_VALUE = e -> e.getValue();

    public UtilityResult calculate(List<ActionGoalTransition> transitions) {
        if (transitions.isEmpty()) {
            return null;
        }

        Map<ActionType, Double> utilitesByAction = transitions.stream()
                .collect(groupingBy(ActionGoalTransition::getActionType)).entrySet().stream()
                .collect(toMap(Entry::getKey, this::calculateUtility));

        Map<String, Double> actionTendencies = utilitesByAction.entrySet().stream()
                .collect(groupingBy(e -> e.getKey().getType(), mapping(GET_VALUE, new RecursiveMeanCollector())));

        Entry<ActionType, Double> maxUtilityAction = utilitesByAction.entrySet().stream()
                .max((entry1, entry2) -> Double.compare(entry1.getValue(), entry2.getValue()))
                .get();

        return new UtilityResult(actionTendencies, maxUtilityAction.getKey().getName(), maxUtilityAction.getValue());
    }

    private double calculateUtility(Map.Entry<?, List<ActionGoalTransition>> transitionEntry) {
        return transitionEntry.getValue().stream()
                .map(transition -> transition.getWeight() * transition.getGoalAmbition())
                .collect(new RecursiveMeanCollector());
    }

    public static final class UtilityResult {

        private final Map<String, Double> actionTendencies;
        private final String maxUtilityAction;
        private final double maxUtility;

        private UtilityResult(Map<String, Double> actionTendencies, String maxUtilityAction, double maxUtility) {
            this.actionTendencies = actionTendencies;
            this.maxUtilityAction = maxUtilityAction;
            this.maxUtility = maxUtility;
        }

        public Map<String, Double> getActionTendencies() {
            return actionTendencies;
        }

        public String getMaxUtilityAction() {
            return maxUtilityAction;
        }

        public double getMaxUtility() {
            return maxUtility;
        }
    }
}
