package org.bitbucket.marrs.sico.actor.lib.respond;

import static org.bitbucket.marrs.sico.actor.lib.base.DomainPort.AESTHETICS;
import static org.bitbucket.marrs.sico.actor.lib.base.DomainPort.AFFORDANCES;
import static org.bitbucket.marrs.sico.actor.lib.base.DomainPort.EPISTEMICS;
import static org.bitbucket.marrs.sico.actor.lib.util.ParameterUtil.createStringParameter;
import static org.bitbucket.marrs.sico.actor.lib.util.ParameterUtil.stringValue;
import static org.bitbucket.marrs.sico.data.type.FuzzyWeightType.FUZZY_WEIGHT;

import java.util.List;
import java.util.Map;

import org.bitbucket.marrs.sico.actor.lib.base.DomainPort;
import org.bitbucket.marrs.sico.actor.lib.base.LinearModel;
import org.bitbucket.marrs.sico.data.token.FuzzyWeightToken;
import org.bitbucket.marrs.sico.data.type.FuzzyWeightType;

import ptolemy.actor.TypedIOPort;
import ptolemy.data.Token;
import ptolemy.data.expr.Parameter;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

import com.google.common.collect.ImmutableList;

/**
 * Actor that evaluates the engagement of the agent based on a generalized
 * linear model.
 * <p>
 * The actor calculates the involvement and distance of the agent as indicative
 * and counter-indicative engagement values based on the relevance, valence,
 * similarity values as well as the encoded values from the affordances,
 * aesthetics and epistemics domain. The values are calculated based on a
 * generalized linear model.
 */
public final class LinearModelEngagement extends LinearModel {

    public static final int MODEL_DIM = 26;

    private static final String RELEVANCE_IN = "relevanceInput";
    private static final String RELEVANCE_IN_DISP = "Relevance";
    private static final String VALENCE_IN = "valenceInput";
    private static final String VALENCE_IN_DISP = "Valence";
    private static final String SIMILARITY_IN = "similarityInput";
    private static final String SIMILARITY_IN_DISP = "Similarity";

    private static final List<String> INPUTS = ImmutableList.of(
            DomainPort.AFFORDANCES.inputName,
            DomainPort.AESTHETICS.inputName,
            DomainPort.EPISTEMICS.inputName,
            RELEVANCE_IN,
            VALENCE_IN,
            SIMILARITY_IN);

    private static final String AID = "aid";
    private static final String OBSTACLE = "obstacle";
    private static final String BEAUTYFUL = "beautiful";
    private static final String UGLY = "ugly";
    private static final String REALISTIC = "realistic";
    private static final String UNREALISTIC = "unrealistic";
    private static final String RELEVANT = "relevant";
    private static final String IRRELEVANT = "irrelevant";
    private static final String VALENCE_POS = "valencePos";
    private static final String VALENCE_NEG = "valenceNeg";
    private static final String SIM_VAL_POS = "simValPos";
    private static final String SIM_VAL_NEG = "simValNeg";
    private static final String DIS_VAL_POS = "disValPos";
    private static final String DIS_VAL_NEG = "disValNeg";
    private static final String SIM_REL = "simRel";
    private static final String SIM_IRR = "simIrr";
    private static final String DIS_REL = "disRel";
    private static final String DIS_IRR = "disIrr";
    private static final String BEA_VAL_POS = "beaValPos";
    private static final String BEA_VAL_NEG = "beaValNeg";
    private static final String UGL_VAL_POS = "uglValPos";
    private static final String UGL_VAL_NEG = "uglValNeg";
    private static final String BEA_REL = "beaRel";
    private static final String BEA_IRR = "beaIrr";
    private static final String UGL_REL = "uglRel";
    private static final String UGL_IRR = "uglIrr";

    private static final String POSTFIX = "Param";

    /**
     * Parameter defining the key of the parameter for indicative affordances
     * value in the {@link LinearModel#rowMappingInput}.
     */
    public Parameter aidParam;

    /**
     * Parameter defining the key of the parameter for counter-indicative
     * affordances value in the {@link LinearModel#rowMappingInput}.
     */
    public Parameter obstacleParam;

    /**
     * Parameter defining the key of the parameter for indicative aesthetics
     * value in the {@link LinearModel#rowMappingInput}.
     */
    public Parameter beautifulParam;

    /**
     * Parameter defining the key of the parameter for counter-indicative
     * aesthetics value in the {@link LinearModel#rowMappingInput}.
     */
    public Parameter uglyParam;

    /**
     * Parameter defining the key of the parameter for indicative epistemics
     * value in the {@link LinearModel#rowMappingInput}.
     */
    public Parameter realisticParam;

    /**
     * Parameter defining the key of the parameter for counter-indicative
     * epistemics value in the {@link LinearModel#rowMappingInput}.
     */
    public Parameter unrealisticParam;

    /**
     * Parameter defining the key of the parameter for indicative relevance
     * value in the {@link LinearModel#rowMappingInput}.
     */
    public Parameter relevantParam;

    /**
     * Parameter defining the key of the parameter for counter-indicative
     * relevance value in the {@link LinearModel#rowMappingInput}.
     */
    public Parameter irrelevantParam;

    /**
     * Parameter defining the key of the parameter for indicative valence
     * value in the {@link LinearModel#rowMappingInput}.
     */
    public Parameter valencePosParam;

    /**
     * Parameter defining the key of the parameter for counter-indicative
     * valence value in the {@link LinearModel#rowMappingInput}.
     */
    public Parameter valenceNegParam;

    /**
     * Parameter defining the key of the parameter for the product of
     * the similarity value and the indicative valence value in the
     * {@link LinearModel#rowMappingInput}.
     */
    public Parameter simValPosParam;

    /**
     * Parameter defining the key of the parameter for the product of
     * the similarity value and the counter-indicative valence value in the
     * {@link LinearModel#rowMappingInput}.
     */
    public Parameter simValNegParam;

    /**
     * Parameter defining the key of the parameter for the product of
     * the dissimilarity value and the indicative valence value in the
     * {@link LinearModel#rowMappingInput}.
     */
    public Parameter disValPosParam;

    /**
     * Parameter defining the key of the parameter for the product of
     * the dissimilarity value and the counter-indicative valence value in the
     * {@link LinearModel#rowMappingInput}.
     */
    public Parameter disValNegParam;

    /**
     * Parameter defining the key of the parameter for the product of
     * the similarity value and the relevance value in the
     * {@link LinearModel#rowMappingInput}.
     */
    public Parameter simRelParam;

    /**
     * Parameter defining the key of the parameter for the product of
     * the similarity value and the irrelevance value in the
     * {@link LinearModel#rowMappingInput}.
     */
    public Parameter simIrrParam;

    /**
     * Parameter defining the key of the parameter for the product of
     * the dissimilarity value and the relevance value in the
     * {@link LinearModel#rowMappingInput}.
     */
    public Parameter disRelParam;

    /**
     * Parameter defining the key of the parameter for the product of
     * the dissimilarity value and the irrelevance value in the
     * {@link LinearModel#rowMappingInput}.
     */
    public Parameter disIrrParam;

    /**
     * Parameter defining the key of the parameter for the product of
     * the beauty value and the indicative valence value in the
     * {@link LinearModel#rowMappingInput}.
     */
    public Parameter beaValPosParam;

    /**
     * Parameter defining the key of the parameter for the product of
     * the beauty value and the counter-indicative valence value in the
     * {@link LinearModel#rowMappingInput}.
     */
    public Parameter beaValNegParam;

    /**
     * Parameter defining the key of the parameter for the product of
     * the ugly value and the indicative valence value in the
     * {@link LinearModel#rowMappingInput}.
     */
    public Parameter uglValPosParam;

    /**
     * Parameter defining the key of the parameter for the product of
     * the ugly value and the counter-indicative valence value in the
     * {@link LinearModel#rowMappingInput}.
     */
    public Parameter uglValNegParam;

    /**
     * Parameter defining the key of the parameter for the product of
     * the beauty value and the relevance value in the
     * {@link LinearModel#rowMappingInput}.
     */
    public Parameter beaRelParam;

    /**
     * Parameter defining the key of the parameter for the product of
     * the beauty value and the irrelevance value in the
     * {@link LinearModel#rowMappingInput}.
     */
    public Parameter beaIrrParam;

    /**
     * Parameter defining the key of the parameter for the product of
     * the ugly value and the relevance value in the
     * {@link LinearModel#rowMappingInput}.
     */
    public Parameter uglRelParam;

    /**
     * Parameter defining the key of the parameter for the product of
     * the ugly value and the irrelevance value in the
     * {@link LinearModel#rowMappingInput}.
     */
    public Parameter uglIrrParam;

    /**
     * Input port for the encoded values of the features in the aesthetics
     * domain. The input port is of type {@link BaseType#RECORD} with values of
     * type {@link FuzzyWeightType} containing the encoded weights of the
     * aesthetics domains.
     */
    public TypedIOPort aestheticsInput;

    /**
     * Input port for the encoded values of the features in the epistemics
     * domain. The input port is of type {@link BaseType#RECORD} with values of
     * type {@link FuzzyWeightType} containing the encoded weights of the
     * epistemics domains.
     */
    public TypedIOPort epistemicsInput;

    /**
     * Input port for the encoded values of the features in the affordances
     * domain. The input port is of type {@link BaseType#RECORD} with values of
     * type {@link FuzzyWeightType} containing the encoded weights of the
     * affordances domains.
     */
    public TypedIOPort affordancesInput;

    /**
     * Input port for the relevance values of the features. The input port is
     * of type {@link BaseType#RECORD} with values of type
     * {@link FuzzyWeightType}.
     */
    public TypedIOPort relevanceInput;

    /**
     * Input port for the valence values of the features. The input port is
     * of type {@link BaseType#RECORD} with values of type
     * {@link FuzzyWeightType}.
     */
    public TypedIOPort valenceInput;

    /**
     * Input port for the similarity values of the features. The input port is
     * of type {@link BaseType#RECORD} with values of type
     * {@link FuzzyWeightType}.
     */
    public TypedIOPort similarityInput;

    public LinearModelEngagement(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace, INPUTS, FUZZY_WEIGHT, MODEL_DIM);
        init();
    }

    public LinearModelEngagement(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        super(container, name, INPUTS, FUZZY_WEIGHT, MODEL_DIM);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        this.aidParam = createStringParameter(this, AID + POSTFIX, AID);
        this.obstacleParam = createStringParameter(this, OBSTACLE + POSTFIX, OBSTACLE);
        this.beautifulParam = createStringParameter(this, BEAUTYFUL + POSTFIX, BEAUTYFUL);
        this.uglyParam = createStringParameter(this, UGLY + POSTFIX, UGLY);
        this.realisticParam = createStringParameter(this, REALISTIC + POSTFIX, REALISTIC);
        this.unrealisticParam = createStringParameter(this, UNREALISTIC + POSTFIX, UNREALISTIC);
        this.relevantParam = createStringParameter(this, RELEVANT + POSTFIX, RELEVANT);
        this.irrelevantParam = createStringParameter(this, IRRELEVANT + POSTFIX, IRRELEVANT);
        this.valencePosParam = createStringParameter(this, VALENCE_POS + POSTFIX, VALENCE_POS);
        this.valenceNegParam = createStringParameter(this, VALENCE_NEG + POSTFIX, VALENCE_NEG);

        this.simValPosParam = createStringParameter(this, SIM_VAL_POS + POSTFIX, SIM_VAL_POS);
        this.simValNegParam = createStringParameter(this, SIM_VAL_NEG + POSTFIX, SIM_VAL_NEG);
        this.disValPosParam = createStringParameter(this, DIS_VAL_POS + POSTFIX, DIS_VAL_POS);
        this.disValNegParam = createStringParameter(this, DIS_VAL_NEG + POSTFIX, DIS_VAL_NEG);
        this.simRelParam = createStringParameter(this, SIM_REL + POSTFIX, SIM_REL);
        this.simIrrParam = createStringParameter(this, SIM_IRR + POSTFIX, SIM_IRR);
        this.disRelParam = createStringParameter(this, DIS_REL + POSTFIX, DIS_REL);
        this.disIrrParam = createStringParameter(this, DIS_IRR + POSTFIX, DIS_IRR);

        this.beaValPosParam = createStringParameter(this, BEA_VAL_POS + POSTFIX, BEA_VAL_POS);
        this.beaValNegParam = createStringParameter(this, BEA_VAL_NEG + POSTFIX, BEA_VAL_NEG);
        this.uglValPosParam = createStringParameter(this, UGL_VAL_POS + POSTFIX, UGL_VAL_POS);
        this.uglValNegParam = createStringParameter(this, UGL_VAL_NEG + POSTFIX, UGL_VAL_NEG);
        this.beaRelParam = createStringParameter(this, BEA_REL + POSTFIX, BEA_REL);
        this.beaIrrParam = createStringParameter(this, BEA_IRR + POSTFIX, BEA_IRR);
        this.uglRelParam = createStringParameter(this, UGL_REL + POSTFIX, UGL_REL);
        this.uglIrrParam = createStringParameter(this, UGL_IRR + POSTFIX, UGL_IRR);

        this.affordancesInput = getInput(0);
        this.affordancesInput.setDisplayName(AFFORDANCES.inputDisplayName);

        this.aestheticsInput = getInput(1);
        this.aestheticsInput.setDisplayName(AESTHETICS.inputDisplayName);

        this.epistemicsInput = getInput(2);
        this.epistemicsInput.setDisplayName(EPISTEMICS.inputDisplayName);

        this.relevanceInput = getInput(3);
        this.relevanceInput.setDisplayName(RELEVANCE_IN_DISP);

        this.valenceInput = getInput(4);
        this.valenceInput.setDisplayName(VALENCE_IN_DISP);

        this.similarityInput = getInput(5);
        this.similarityInput.setDisplayName(SIMILARITY_IN_DISP);
    }

    @Override
    protected double[] parseArgumentsOrdered(List<Token> args, Map<String, Integer> rowMapping) throws IllegalActionException {
        double[] orderedArguments = new double[MODEL_DIM];

        FuzzyWeightToken affordancesToken = FUZZY_WEIGHT.convert(args.get(0));
        FuzzyWeightToken aestheticsToken = FUZZY_WEIGHT.convert(args.get(1));
        FuzzyWeightToken epistemicsToken = FUZZY_WEIGHT.convert(args.get(2));
        FuzzyWeightToken relevanceToken = FUZZY_WEIGHT.convert(args.get(3));
        FuzzyWeightToken valenceToken = FUZZY_WEIGHT.convert(args.get(4));
        FuzzyWeightToken similarityToken = FUZZY_WEIGHT.convert(args.get(5));

        double aid = affordancesToken.getPositiveWeight();
        double obstacle = affordancesToken.getNegativeWeight();
        double beautiful = aestheticsToken.getPositiveWeight();
        double ugly = aestheticsToken.getNegativeWeight();
        double realistic = epistemicsToken.getPositiveWeight();
        double unrealistic = epistemicsToken.getNegativeWeight();
        double relevant = relevanceToken.getPositiveWeight();
        double irrelevant = relevanceToken.getNegativeWeight();
        double valencePos = valenceToken.getPositiveWeight();
        double valenceNeg = valenceToken.getNegativeWeight();
        double similarity = similarityToken.getPositiveWeight();
        double dissimilarity = similarityToken.getNegativeWeight();

        orderedArguments[getChecked(rowMapping, stringValue(aidParam))] = aid;
        orderedArguments[getChecked(rowMapping, stringValue(obstacleParam))] = obstacle;
        orderedArguments[getChecked(rowMapping, stringValue(beautifulParam))] = beautiful;
        orderedArguments[getChecked(rowMapping, stringValue(uglyParam))] = ugly;
        orderedArguments[getChecked(rowMapping, stringValue(realisticParam))] = realistic;
        orderedArguments[getChecked(rowMapping, stringValue(unrealisticParam))] = unrealistic;
        orderedArguments[getChecked(rowMapping, stringValue(relevantParam))] = relevant;
        orderedArguments[getChecked(rowMapping, stringValue(irrelevantParam))] = irrelevant;
        orderedArguments[getChecked(rowMapping, stringValue(valencePosParam))] = valencePos;
        orderedArguments[getChecked(rowMapping, stringValue(valenceNegParam))] = valenceNeg;

        orderedArguments[getChecked(rowMapping, stringValue(simValPosParam))] = similarity * valencePos;
        orderedArguments[getChecked(rowMapping, stringValue(simValNegParam))] = similarity * valenceNeg;
        orderedArguments[getChecked(rowMapping, stringValue(disValPosParam))] = dissimilarity * valencePos;
        orderedArguments[getChecked(rowMapping, stringValue(disValNegParam))] = dissimilarity * valenceNeg;
        orderedArguments[getChecked(rowMapping, stringValue(simRelParam))] = similarity * relevant;
        orderedArguments[getChecked(rowMapping, stringValue(simIrrParam))] = similarity * irrelevant;
        orderedArguments[getChecked(rowMapping, stringValue(disRelParam))] = dissimilarity * relevant;
        orderedArguments[getChecked(rowMapping, stringValue(disIrrParam))] = dissimilarity * irrelevant;

        orderedArguments[getChecked(rowMapping, stringValue(beaValPosParam))] = beautiful * valencePos;
        orderedArguments[getChecked(rowMapping, stringValue(beaValNegParam))] = beautiful * valenceNeg;
        orderedArguments[getChecked(rowMapping, stringValue(uglValPosParam))] = ugly * valencePos;
        orderedArguments[getChecked(rowMapping, stringValue(uglValNegParam))] = ugly * valenceNeg;
        orderedArguments[getChecked(rowMapping, stringValue(beaRelParam))] = beautiful * relevant;
        orderedArguments[getChecked(rowMapping, stringValue(beaIrrParam))] = beautiful * irrelevant;
        orderedArguments[getChecked(rowMapping, stringValue(uglRelParam))] = ugly * relevant;
        orderedArguments[getChecked(rowMapping, stringValue(uglIrrParam))] = ugly * irrelevant;

        return orderedArguments;
    }
}
