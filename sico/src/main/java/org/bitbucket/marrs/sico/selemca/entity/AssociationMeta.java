package org.bitbucket.marrs.sico.selemca.entity;


/**
 * Metadata for an association. Defined as a quadruple: concept1 - concept2 - relation - value
 * There can be any number of metadata items for an association.
 */
public final class AssociationMeta {

    private Concept concept1;
    private Concept concept2;
    private String relation;
    private String value;

    public AssociationMeta() {
        // Support instantiation by reflection
    }

    public AssociationMeta(Concept concept1, Concept concept2, String relation, String value) {
        this.concept1 = concept1;
        this.concept2 = concept2;
        this.relation = relation;
        this.value = value;
    }

    public Concept getConcept1() {
        return concept1;
    }

    public Concept getConcept2() {
        return concept2;
    }

    public String getRelation() {
        return relation;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s %s", concept1, concept2, relation, value);
    }
}
