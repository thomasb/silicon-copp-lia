package org.bitbucket.marrs.sico.actor.lib.respond;

import static org.bitbucket.marrs.sico.actor.lib.util.DoubleWeightCalculator.calulateAverage;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createTypedInputPort;
import static org.bitbucket.marrs.sico.data.type.FuzzyWeightType.FUZZY_WEIGHT;

import java.util.Collection;
import java.util.List;

import org.bitbucket.marrs.sico.actor.lib.base.MultiRecordTransformation;
import org.bitbucket.marrs.sico.algo.respond.SatisfactionFunction;
import org.bitbucket.marrs.sico.data.token.FuzzyWeightToken;

import ptolemy.actor.TypedIOPort;
import ptolemy.data.DoubleToken;
import ptolemy.data.Token;
import ptolemy.data.type.BaseType;
import ptolemy.data.type.Type;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

import com.google.common.collect.ImmutableList;

/**
 * Calculates the satisfaction value for features.
 * <p>
 * Satisfaction is calculated as a weighted average of involvement-distance
 * tradeoff an use intentions for the feature. The weight is retrieved from an
 * external source.
 * <p>
 * The result is a double value between 0 and 1 for each feature from the input
 * record.
 */
public final class Satisfaction extends MultiRecordTransformation<DoubleToken> {

    private static final String USE_INTENTIONS_IN = "useIntentionsInput";
    private static final String USE_INTENTIONS_IN_DISP = "Use intentions";
    private static final String TRADEOFF_IN = "tradeoffInput";
    private static final String TRADEOFF_IN_DISP = "Tradeoff";
    private static final String WEIGHT_IN = "weightInput";
    private static final List<String> INPUTS = ImmutableList.of(USE_INTENTIONS_IN, TRADEOFF_IN);
    private static final List<Type> INPUT_TYPES = ImmutableList.of(FUZZY_WEIGHT, BaseType.DOUBLE);

    private static final String SATISFACTION_OUT_DISP = "Satisfaction";
    private static final String SATISFACTION_OUT_TOTAL_DISP = "Satisfaction total";

    /**
     * Input port for the use intentions. The input is a record with fuzzy
     * double weights, where the indicative value represents intention to use,
     * counter-indicative represents intention to not use a feature.
     */
    public TypedIOPort useIntentionsInput;

    /**
     * Input port for the involvement-distance values. The input is a record
     * with double values.
     */
    public TypedIOPort tradeoffInput;

    /**
     * Input port for the weight used in the satisfaction calculation. The input
     * is of type double.
     */
    public TypedIOPort weightInput;

    public Satisfaction(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace, INPUTS, BaseType.DOUBLE, INPUT_TYPES);
        init();
    }

    public Satisfaction(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        super(container, name, INPUTS, BaseType.DOUBLE, INPUT_TYPES);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        this.useIntentionsInput = getInput(0);
        this.useIntentionsInput.setDisplayName(USE_INTENTIONS_IN_DISP);
        this.tradeoffInput = getInput(1);
        this.tradeoffInput.setDisplayName(TRADEOFF_IN_DISP);

        this.weightInput = createTypedInputPort(this, WEIGHT_IN, BaseType.DOUBLE);

        getOutput().setDisplayName(SATISFACTION_OUT_DISP);
        getOutputAggregate().setDisplayName(SATISFACTION_OUT_TOTAL_DISP);
    }

    @Override
    protected Transformation<Token, DoubleToken> getTransformation() throws IllegalActionException {
        DoubleToken weightToken = DoubleToken.convert(weightInput.get(0));
        SatisfactionFunction satisfaction = new SatisfactionFunction(weightToken.doubleValue());

        return (label, values) -> {
            FuzzyWeightToken useIntentionsToken = FUZZY_WEIGHT.convert(values.get(0));
            double avgUseIntentions = 0.5 * (useIntentionsToken.getPositiveWeight() - useIntentionsToken.getNegativeWeight());
            Token idtToken = values.get(1);
            double idt = idtToken.isNil() ? 0.0 : DoubleToken.convert(values.get(1)).doubleValue();

            return new DoubleToken(satisfaction.apply(avgUseIntentions, idt));
        };
    }

    @Override
    protected Token aggregateOutput(Collection<DoubleToken> outputs) {
        return calulateAverage(outputs);
    }
}
