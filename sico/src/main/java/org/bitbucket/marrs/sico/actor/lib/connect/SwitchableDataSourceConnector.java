package org.bitbucket.marrs.sico.actor.lib.connect;

import static org.bitbucket.marrs.sico.actor.lib.util.ParameterUtil.createStringParameter;
import static org.bitbucket.marrs.sico.actor.lib.util.ParameterUtil.stringValue;

import java.net.URISyntaxException;
import java.nio.file.Paths;

import ptolemy.actor.TypedAtomicActor;
import ptolemy.data.expr.ChoiceParameter;
import ptolemy.data.expr.Parameter;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

/**
 * Base class for actors that retrieve data from either files or an epistemics
 * engine.
 */
public class SwitchableDataSourceConnector extends TypedAtomicActor {

    private static final String DATA_SOURCE = "dataSourceParameter";
    private static final String DATA_SOURCE_DISP = "Data source";

    private static final String QUERY_URL = "epistemicsUrlParameter";
    private static final String QUERY_URL_DISP = "Epistemics Url";
    private static final String DATA_DIR = "dataDirParameter";
    private static final String DATA_DIR_DISP = "Data Directory";

    private static final String DEFAULT_QUERY_URL = "http://localhost:8080/epistemics/";
    private static final String DEFAULT_DATA_DIR = "data/";

    /**
     * Parameter for the URL of the epistemics engine. Defaults to
     * "http://localhost:8080/epistemics".
     */
    public Parameter epistemicsUrlParameter;

    /**
     * Parameter for the path to the file containing data for the associations
     * of actions with features. Defaults to "data/".
     */
    public Parameter dataDirParameter;

    /**
     * Parameter to choose which data source to use.
     */
    public ChoiceParameter dataSourceParameter;

    private DataSource dataSource;

    public SwitchableDataSourceConnector(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace);
        init();
    }

    public SwitchableDataSourceConnector(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        super(container, name);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        this.dataSourceParameter = new ChoiceParameter(this, DATA_SOURCE, DataSourceFactory.class);
        this.dataSourceParameter.setDisplayName(DATA_SOURCE_DISP);

        this.epistemicsUrlParameter = createStringParameter(this, QUERY_URL, DEFAULT_QUERY_URL);
        this.epistemicsUrlParameter.setDisplayName(QUERY_URL_DISP);

        this.dataDirParameter = createStringParameter(this, DATA_DIR, DEFAULT_DATA_DIR);
        this.dataDirParameter.setDisplayName(DATA_DIR_DISP);
    }

    @Override
    public void initialize() throws IllegalActionException {
        super.initialize();

        DataSourceFactory dataSourceFactory = (DataSourceFactory) dataSourceParameter.getChosenValue();
        try {
            dataSource = dataSourceFactory.create(this);
        } catch (Exception e) {
            throw new IllegalActionException(this, e, "Could not connect to data source.");
        }
    }

    protected DataSource getDataSource() {
        return dataSource;
    }

    @Override
    public Object clone(Workspace workspace) throws CloneNotSupportedException {
        SwitchableDataSourceConnector clone = (SwitchableDataSourceConnector) super.clone(workspace);
        clone.dataSource = dataSource;

        return clone;
    }

    private enum DataSourceFactory {
        FILE {
            @Override
            DataSource create(SwitchableDataSourceConnector connector) throws URISyntaxException {
                String baseDir = stringValue(connector.dataDirParameter);

                return new FileDataSource(Paths.get(baseDir, "actions.json").toUri(),
                    Paths.get(baseDir, "action_meta.json").toUri(),
                    Paths.get(baseDir, "action_goals.json").toUri(),
                    Paths.get(baseDir, "action_goals_meta.json").toUri(),
                    Paths.get(baseDir, "goal_meta.json").toUri(),
                    Paths.get(baseDir, "self.json").toUri());
            }
        }, SELEMCA {
            @Override
            DataSource create(SwitchableDataSourceConnector connector) throws URISyntaxException {
                return new EpistemicsDataSource(stringValue(connector.epistemicsUrlParameter));
            }
        };

        abstract DataSource create(SwitchableDataSourceConnector connector) throws Exception;
    }
}
