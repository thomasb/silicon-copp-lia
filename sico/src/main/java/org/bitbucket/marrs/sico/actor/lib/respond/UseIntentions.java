package org.bitbucket.marrs.sico.actor.lib.respond;

import static org.bitbucket.marrs.sico.actor.lib.base.DomainPort.AESTHETICS;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordInputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordOutputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createTypedOutputPort;
import static org.bitbucket.marrs.sico.data.type.FuzzyWeightType.FUZZY_WEIGHT;

import org.bitbucket.marrs.sico.actor.lib.connect.LinearModelDataConnector;
import org.bitbucket.marrs.sico.data.type.FuzzyWeightType;

import ptolemy.actor.TypedCompositeActor;
import ptolemy.actor.TypedIOPort;
import ptolemy.actor.lib.Discard;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

/**
 * Actor that evaluates the use intentions of the agent.
 * <p>
 * The actor calculates the indicative and counter-indicative values for the
 * use intentions of the agent towards the features as indicative
 * based on the relevance, valence, similarity values as well as the encoded
 * values from the aesthetics domain.
 */
public final class UseIntentions extends TypedCompositeActor {

    public static final int MODEL_DIM = 10;

    private static final String FEATURES_IN = "featuresInput";
    private static final String FEATURES_IN_DISP = "Features";
    private static final String SIMILARITY_IN = "similarityInput";
    private static final String SIMILARITY_IN_DISP = "Similarity";
    private static final String RELEVANCE_IN = "relevanceInput";
    private static final String RELEVANCE_IN_DISP = "Relevance";
    private static final String VALENCE_IN = "valenceInput";
    private static final String VALENCE_IN_DISP = "Valence";

    private static final String USE_INTENTIONS_OUT = "useIntentionsOutput";
    private static final String USE_INTENTIONS_OUT_DISP = "Use Intentions";
    private static final String USE_INTENTIONS_OUT_TOTAL = "useIntentionsOutputTotal";
    private static final String USE_INTENTIONS_OUT_TOTAL_DISP = "Use Intentions total";

    private static final String FEATURE_SINK = "featureSink";
    private static final String FEATURE_SINK_DISP = "Discard";
    private static final String USE_INTENTIONS_CONNECTOR_ACTOR = "useIntentionsConnector";
    private static final String USE_INTENTIONS_CONNECTOR_ACTOR_DISP = "Epistemics Connector";
    private static final String USE_INTENTIONS_ACTOR = "useIntentions";
    private static final String USE_INTENTIONS_ACTOR_DISP = "Use intentions";

    private static final String WEIGHT_MATRIX_PROPERTY = "use_intentions_weight_matrix";
    private static final String ROW_MAPPING_PROPERTY = "use_intentions_row_mapping";

    public TypedIOPort featuresInput;

    /**
     * Input port for the relevance values of the features. The input port is
     * of type {@link BaseType#RECORD} with values of type
     * {@link FuzzyWeightType}.
     */
    public TypedIOPort relevanceInput;

    /**
     * Input port for the valence values of the features. The input port is
     * of type {@link BaseType#RECORD} with values of type
     * {@link FuzzyWeightType}.
     */
    public TypedIOPort valenceInput;

    /**
     * Input port for the encoded values of the features in the aesthetics domain.
     * The input port is of type {@link BaseType#RECORD} with values of type
     * {@link FuzzyWeightType} containing the encoded weights of the ethics
     * domains.
     */
    public TypedIOPort aestheticsInput;

    /**
     * Input port for the similarity values of the features. The input port is
     * of type {@link BaseType#RECORD} with values of type
     * {@link FuzzyWeightType}.
     */
    public TypedIOPort similarityInput;

    /**
     * Output port for the use intentions values. The output is of type
     * {@link BaseType#RECORD} with values of type {@link FuzzyWeightType}. The
     * indicative values represent intentions to use a feature, the
     * counter-indicative values represent intentions to avoid using a feature.
     */
    public TypedIOPort useIntentionsOutput;

    /**
     * Output port for the aggregate engagement value. The output is of type
     * {@link FuzzyWeightType}. The indicative values represent intentions to
     * use a feature, the counter-indicative values represent intentions to
     * avoid using a feature.
     */
    public TypedIOPort useIntentionsOutputTotal;

    public Discard featureSink;
    public LinearModelDataConnector useIntentionsConnector;
    public LinearModelUseIntentions useIntentions;

    public UseIntentions(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace);
        init();
    }

    public UseIntentions(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        super(container, name);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        featuresInput = createRecordInputPort(this, FEATURES_IN);
        featuresInput.setDisplayName(FEATURES_IN_DISP);

        aestheticsInput = createRecordInputPort(this, AESTHETICS.inputName);
        aestheticsInput.setDisplayName(AESTHETICS.inputDisplayName);

        relevanceInput = createRecordInputPort(this, RELEVANCE_IN);
        relevanceInput.setDisplayName(RELEVANCE_IN_DISP);
        valenceInput = createRecordInputPort(this, VALENCE_IN);
        valenceInput.setDisplayName(VALENCE_IN_DISP);

        similarityInput = createRecordInputPort(this, SIMILARITY_IN);
        similarityInput.setDisplayName(SIMILARITY_IN_DISP);

        useIntentionsOutput = createRecordOutputPort(this, USE_INTENTIONS_OUT);
        useIntentionsOutput.setDisplayName(USE_INTENTIONS_OUT_DISP);
        useIntentionsOutputTotal = createTypedOutputPort(this, USE_INTENTIONS_OUT_TOTAL, FUZZY_WEIGHT);
        useIntentionsOutputTotal.setDisplayName(USE_INTENTIONS_OUT_TOTAL_DISP);

        featureSink = new Discard(this, FEATURE_SINK);
        featureSink.setDisplayName(FEATURE_SINK_DISP);

        useIntentionsConnector = new LinearModelDataConnector(this, USE_INTENTIONS_CONNECTOR_ACTOR);
        useIntentionsConnector.setRows(MODEL_DIM);
        useIntentionsConnector.setMatrixRelation(WEIGHT_MATRIX_PROPERTY);
        useIntentionsConnector.setRowMappingRelation(ROW_MAPPING_PROPERTY);
        useIntentionsConnector.setDisplayName(USE_INTENTIONS_CONNECTOR_ACTOR_DISP);

        useIntentions = new LinearModelUseIntentions(this, USE_INTENTIONS_ACTOR);
        useIntentions.setDisplayName(USE_INTENTIONS_ACTOR_DISP);

        connect(featuresInput, featureSink.input);
        connect(aestheticsInput, useIntentions.aestheticsInput);
        connect(relevanceInput, useIntentions.relevanceInput);
        connect(valenceInput, useIntentions.valenceInput);
        connect(similarityInput, useIntentions.similarityInput);
        connect(useIntentionsConnector.weightMatrixOutput, useIntentions.weightMatrixInput);
        connect(useIntentionsConnector.rowMappingOutput, useIntentions.rowMappingInput);
        connect(useIntentionsOutput, useIntentions.output);
        connect(useIntentionsOutputTotal, useIntentions.outputAggregate);
    }
}
