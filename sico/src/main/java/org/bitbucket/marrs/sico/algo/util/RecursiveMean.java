package org.bitbucket.marrs.sico.algo.util;

import static java.lang.Double.compare;
import static java.lang.Math.abs;
import static java.lang.Math.signum;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.groupingBy;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public final class RecursiveMean {

    public static double calculateMean(List<Double> weightValues) {
        return mean(weightValues.stream());
    }

    private static double mean(Stream<Double> weightValues) {
        List<Double> sizeAndWeightedMean = weightValues
                .collect(groupingBy(v -> signum(v)))
                .entrySet().stream()
                .map(entry -> entry.getKey().equals(0.0) ?
                        asList(0.0, 0.0) :
                        asList(entrySize(entry), recursiveMean(entry.getValue())))
                .reduce(asList(0.0, 0.0), RecursiveMean::accumulateSizeAndWeightedMean);

        return sizeAndWeightedMean.get(1) / sizeAndWeightedMean.get(0);
    }

    private static Double recursiveMean(List<Double> values) {
        return values.stream()
                .map(value -> value)
                .sorted((v1, v2) -> compare(abs(v1), abs(v2)))
                .reduce((acc, value) -> 0.5 * (acc + value))
                .get();
    }

    private static List<Double> accumulateSizeAndWeightedMean(List<Double> acc, List<Double> sizeAndMean) {
        double weightedMeanAcc = acc.get(1) + sizeAndMean.get(0) * sizeAndMean.get(1);
        double sizeAcc = acc.get(0) + sizeAndMean.get(0);

        return asList(sizeAcc, weightedMeanAcc);
    }

    private static double entrySize(Map.Entry<Double, List<Double>> entry) {
        return entry.getValue().size();
    }
}
