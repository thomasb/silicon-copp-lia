package org.bitbucket.marrs.sico.actor.lib.compare;

import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordInputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordOutputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createTypedOutputPort;
import static org.bitbucket.marrs.sico.data.type.FuzzyWeightType.FUZZY_WEIGHT;

import org.bitbucket.marrs.sico.actor.lib.base.DomainPort;
import org.bitbucket.marrs.sico.actor.lib.connect.SimilarityDataConnector;
import org.bitbucket.marrs.sico.actor.lib.encode.Encoding;
import org.bitbucket.marrs.sico.data.type.FuzzyWeightType;

import ptolemy.actor.TypedCompositeActor;
import ptolemy.actor.TypedIOPort;
import ptolemy.actor.lib.Discard;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;

public final class Similarity extends TypedCompositeActor {

    private static final String FEATURES_IN = "featuresInput";
    private static final String FEATURES_IN_DISP = "Features";
    private static final String SIMILARITY_OUT = "similarityOutput";
    private static final String SIMILARITY_OUT_DISP = "Similarity";
    private static final String SIMILARITY_TOTAL_OUT = "similarityOutputTotal";
    private static final String SIMILARITY_TOTAL_OUT_DISP = "Similarity Total";

    private static final String FEATURE_SINK = "featureSink";
    private static final String FEATURE_SINK_DISP = "Discard";
    private static final String SIMILARITY_DATA_CONNECTOR = "similarityDataConnector";
    private static final String SIMILARITY_DATA_CONNECTOR_DISP = "Epistemics Connector";
    private static final String SELF_ENCODER_ACTOR = "selfEncoding";
    private static final String SELF_ENCODER_ACTOR_DISP = "Encoding Self";
    private static final String DISTANCE_ACTOR = "similarityDistance";
    private static final String DISTANCE_ACTOR_DISP = "Similarity Distance";

    /**
     * Input port for the features. The port is of type {@link BaseType#RECORD}
     * with values of type {@link BaseType#DOUBLE}.
     */
    public TypedIOPort featuresInput;

    /**
     * Input port for the feature values in the ethic domain. The port is of
     * type {@link BaseType#RECORD} with values of type {@link FuzzyWeightType}.
     */
    public TypedIOPort ethicsInput;

    /**
     * Input port for the feature values in the affordances domain. The port is
     * of type {@link BaseType#RECORD} with values of type {@link FuzzyWeightType}.
     */
    public TypedIOPort affordancesInput;

    /**
     * Input port for the feature values in the aesthetics domain. The port is
     * of type {@link BaseType#RECORD} with values of type {@link FuzzyWeightType}.
     */
    public TypedIOPort aestheticsInput;

    /**
     * Input port for the feature values in the epistemics domain. The port is
     * of type {@link BaseType#RECORD} with values of type {@link FuzzyWeightType}.
     */
    public TypedIOPort epistemicsInput;

    /**
     * Output port for the similarity values. The output is of type
     * {@link BaseType#RECORD} with values of type {@link FuzzyWeightType}. The
     * indicative values represents similarity, the counter-indicative values
     * represents dissimilarity.
     */
    public TypedIOPort similarityOutput;

    /**
     * Output port for the aggregate similarity value. The output is of type
     * {@link FuzzyWeightType}. The indicative value represents the total
     * similarity, the counter-indicative value represents total dissimilarity.
     */
    public TypedIOPort similarityOutputTotal;

    public SimilarityDataConnector similarityDataConnector;
    public Discard featureSink;
    public Encoding selfEncoding;
    public SimilarityDistance similarityDistance;

    public Similarity(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        super(container, name);

        this.featuresInput = createRecordInputPort(this, FEATURES_IN);
        this.featuresInput.setDisplayName(FEATURES_IN_DISP);

        this.ethicsInput = createRecordInputPort(this, DomainPort.ETHICS.inputName);
        this.ethicsInput.setDisplayName(DomainPort.ETHICS.inputDisplayName);
        this.affordancesInput = createRecordInputPort(this, DomainPort.AFFORDANCES.inputName);
        this.affordancesInput.setDisplayName(DomainPort.AFFORDANCES.inputDisplayName);
        this.aestheticsInput = createRecordInputPort(this, DomainPort.AESTHETICS.inputName);
        this.aestheticsInput.setDisplayName(DomainPort.AESTHETICS.inputDisplayName);
        this.epistemicsInput = createRecordInputPort(this, DomainPort.EPISTEMICS.inputName);
        this.epistemicsInput.setDisplayName(DomainPort.EPISTEMICS.inputDisplayName);

        this.similarityOutput = createRecordOutputPort(this, SIMILARITY_OUT);
        this.similarityOutput.setDisplayName(SIMILARITY_OUT_DISP);
        this.similarityOutputTotal = createTypedOutputPort(this, SIMILARITY_TOTAL_OUT, FUZZY_WEIGHT);
        this.similarityOutputTotal.setDisplayName(SIMILARITY_TOTAL_OUT_DISP);

        this.featureSink = new Discard(this, FEATURE_SINK);
        this.featureSink.setDisplayName(FEATURE_SINK_DISP);

        this.similarityDataConnector = new SimilarityDataConnector(this, SIMILARITY_DATA_CONNECTOR);
        this.similarityDataConnector.setDisplayName(SIMILARITY_DATA_CONNECTOR_DISP);

        this.selfEncoding = new Encoding(this, SELF_ENCODER_ACTOR);
        this.selfEncoding.setDisplayName(SELF_ENCODER_ACTOR_DISP);
        this.similarityDistance = new SimilarityDistance(this, DISTANCE_ACTOR);
        this.similarityDistance.setDisplayName(DISTANCE_ACTOR_DISP);

        connect(featuresInput, featureSink.input);

        connect(similarityDataConnector.featuresSelfOutput, selfEncoding.featuresInput);
        connect(similarityDataConnector.similarityWeightsOutput, similarityDistance.simmilarityWeightInput);
        connect(similarityDataConnector.dissimilarityWeightsOutput, similarityDistance.dissimmilarityWeightInput);

        connect(selfEncoding.ethicsOutput, similarityDistance.ethicsInputSelf);
        connect(selfEncoding.affordancesOutput, similarityDistance.affordancesInputSelf);
        connect(selfEncoding.aestheticsOutput, similarityDistance.aestheticsInputSelf);
        connect(selfEncoding.epistemicsOutput, similarityDistance.epistemicsInputSelf);

        connect(ethicsInput, similarityDistance.ethicsInput);
        connect(affordancesInput, similarityDistance.affordancesInput);
        connect(aestheticsInput, similarityDistance.aestheticsInput);
        connect(epistemicsInput, similarityDistance.epistemicsInput);

        connect(similarityDistance.getSimilarityOutput(), similarityOutput);
        connect(similarityDistance.getSimilarityOutputTotal(), similarityOutputTotal);
    }
}
