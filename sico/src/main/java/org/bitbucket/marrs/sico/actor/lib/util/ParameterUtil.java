package org.bitbucket.marrs.sico.actor.lib.util;

import ptolemy.data.expr.Parameter;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.NamedObj;

public final class ParameterUtil {

    public static Parameter createStringParameter(NamedObj container, String name) throws IllegalActionException, NameDuplicationException {
        return createStringParameter(container, name, null);
    }

    public static Parameter createStringParameter(NamedObj container, String name, String defaultValue) throws IllegalActionException, NameDuplicationException {
        Parameter parameter = new Parameter(container, name);
        parameter.setStringMode(true);
        parameter.setExpression(defaultValue);

        return parameter;
    }

    public static String stringValue(Parameter parameter) {
        String value = parameter.getExpression();
        if (value == null || value.isEmpty()) {
            throw new IllegalStateException("Parameter \"" + parameter.getName() + "\" is not set");
        }

        return value;
    }
}
