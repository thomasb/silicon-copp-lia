package org.bitbucket.marrs.sico.actor.lib.compare;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.bitbucket.marrs.sico.actor.lib.base.DomainPort.AFFORDANCES;
import static org.bitbucket.marrs.sico.actor.lib.util.FuzzyWeightCalculator.calculateAverage;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordInputPort;
import static org.bitbucket.marrs.sico.data.type.FuzzyWeightType.FUZZY_WEIGHT;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.bitbucket.marrs.sico.actor.lib.base.MultiRecordTransformation;
import org.bitbucket.marrs.sico.algo.compare.ActionGoalTransition;
import org.bitbucket.marrs.sico.algo.compare.ValenceFunction;
import org.bitbucket.marrs.sico.algo.compare.ValenceFunction.ValenceResult;
import org.bitbucket.marrs.sico.data.token.FuzzyWeightToken;
import org.bitbucket.marrs.sico.data.type.FuzzyWeightType;

import ptolemy.actor.TypedIOPort;
import ptolemy.data.ArrayToken;
import ptolemy.data.ObjectToken;
import ptolemy.data.RecordToken;
import ptolemy.data.StringToken;
import ptolemy.data.Token;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

import com.google.common.collect.ImmutableList;

public class Valence extends MultiRecordTransformation<FuzzyWeightToken> {

    private static final ValenceFunction VALENCE_FUNCTION = new ValenceFunction();

    private static final String TRANSITIONS_IN = "transitionsInput";
    private static final String TRANSITIONS_IN_DISP = "Transitions";
    private static final String MAX_UTIL_ACTION_IN = "maxUtilityActionInput";
    private static final String MAX_UTIL_ACTION_IN_DISP = "Max Utility Actions";
    private static final List<String> INPUTS = ImmutableList.of(AFFORDANCES.inputName);

    /**
     * Input port for the encoded values of the features in the affordances
     * domain. The input port is of type {@link BaseType#RECORD} with values of
     * type {@link FuzzyWeightType} containing the encoded weights of the
     * affordances domain.
     */
    public TypedIOPort affordancesInput;

    /**
     * Input port for transition weights from actions to goals. The input is of
     * type {@link BaseType#RECORD} where the keys are action names and the
     * values are {@link ActionGoalTransition} objects. The contained actions
     * are limited to the actions that have maximum utility for at least one
     * feature.
     */
    public TypedIOPort transitionsInput;

    /**
     * Input port for actions with maximum utility for a feature. The input
     * is of type {@link BaseType#RECORD} where the keys are feature names and
     * the values the names of the action that maximize the utility for that
     * feature.
     */
    public TypedIOPort maxUtilityActionInput;

    public Valence(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace, INPUTS, FUZZY_WEIGHT, FUZZY_WEIGHT);
        init();
    }

    public Valence(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        super(container, name, INPUTS, FUZZY_WEIGHT, FUZZY_WEIGHT);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        affordancesInput = getInput(0);
        affordancesInput.setDisplayName(AFFORDANCES.inputDisplayName);

        transitionsInput = createRecordInputPort(this, TRANSITIONS_IN);
        transitionsInput.setDisplayName(TRANSITIONS_IN_DISP);

        maxUtilityActionInput = createRecordInputPort(this, MAX_UTIL_ACTION_IN);
        maxUtilityActionInput.setDisplayName(MAX_UTIL_ACTION_IN_DISP);
    }

    @Override
    protected Transformation<Token, FuzzyWeightToken> getTransformation() throws IllegalActionException {
        RecordToken transitionsRecord = (RecordToken) transitionsInput.get(0);
        RecordToken maxUtilityActionRecord = (RecordToken) maxUtilityActionInput.get(0);

        return (label, args) -> {
            List<ActionGoalTransition> transitions = readTransitions(transitionsRecord, maxUtilityActionRecord, label);
            FuzzyWeightToken affordances = FUZZY_WEIGHT.convert(args.get(0));

            ValenceResult valenceResult = VALENCE_FUNCTION
                    .calculate(affordances.getPositiveWeight(), affordances.getNegativeWeight(), transitions);

            return new FuzzyWeightToken(valenceResult.getPositive(), valenceResult.getNegative());
        };
    }

    private List<ActionGoalTransition> readTransitions(RecordToken transitionsRecord,
            RecordToken maxUtilityActionRecord,
            String label) throws IllegalActionException {
        if (!maxUtilityActionRecord.labelSet().contains(label)) {
            return Collections.emptyList();
        }

        String actionName = StringToken.convert(maxUtilityActionRecord.get(label)).stringValue();
        Token[] transitionTokens = ((ArrayToken) transitionsRecord.get(actionName)).arrayValue();

        return stream(transitionTokens)
                .map(t -> (ObjectToken) t)
                .map(ObjectToken::getValue)
                .map(obj -> (ActionGoalTransition) obj)
                .collect(toList());
    }

    @Override
    protected Token aggregateOutput(Collection<FuzzyWeightToken> outputs) throws IllegalActionException {
        return calculateAverage(outputs);
    }
}
