package org.bitbucket.marrs.sico.algo.compare;

import static java.lang.Double.parseDouble;
import static java.lang.Math.max;

import java.util.ArrayList;
import java.util.Map;
import java.util.stream.DoubleStream;

import org.bitbucket.marrs.sico.selemca.entity.Association;
import org.bitbucket.marrs.sico.selemca.entity.AssociationData;
import org.bitbucket.marrs.sico.selemca.entity.Concept;
import org.bitbucket.marrs.sico.selemca.entity.ConceptMeta;

public final class ActionGoalTransition {

    private final Concept action;
    private final Concept goal;
    private final double truthValue;
    private final String actionType;
    private final double transitionWeight;
    private final double goalAmbition;

    public ActionGoalTransition(AssociationData actionGoalAssociation,
            Map<String, ConceptMeta> actionMetadata,
            Map<String, ConceptMeta> goalMetadata,
            double featureActionTruthValue) {
        this.action = actionGoalAssociation.getAction(actionMetadata.keySet());

        Association association = actionGoalAssociation.getAssociation();
        this.goal = association.getOtherConcept(action);
        this.truthValue = aggregateTruthValue(association.getTruthValue(), featureActionTruthValue);
        this.actionType = actionMetadata.get(action.getName()).getValue();
        this.transitionWeight = parseDouble(actionGoalAssociation.getMetadata().getValue());
        this.goalAmbition = parseDouble(goalMetadata.get(goal.getName()).getValue());
    }

    private double aggregateTruthValue(double... truthValues) {
        double totalUntruth = DoubleStream.of(truthValues).map(val -> 1 - val).sum();

        return max(0, 1 - totalUntruth);
    }

    public Concept getAction() {
        return action;
    }

    public String getActionName() {
        return action.getName();
    }

    public Concept getGoal() {
        return goal;
    }

    public Concept getGoalName() {
        return goal;
    }

    public double getTruthValue() {
        return truthValue;
    }

    public ActionType getActionType() {
        return new ActionType(action.getName(), actionType);
    }

    public double getWeight() {
        return transitionWeight;
    }

    public double getGoalAmbition() {
        return goalAmbition;
    }

    public static final class ActionType extends ArrayList<String> {

        public ActionType(String name, String type) {
            super(2);
            add(name);
            add(type);
        }

        public String getName() {
            return get(0);
        }

        public String getType() {
            return get(1);
        }
    }
}
