package org.bitbucket.marrs.sico.actor.lib.compare;

import static org.bitbucket.marrs.sico.actor.lib.util.ParameterUtil.createStringParameter;
import static org.bitbucket.marrs.sico.actor.lib.util.ParameterUtil.stringValue;
import static org.bitbucket.marrs.sico.data.type.FuzzyWeightType.FUZZY_WEIGHT;

import java.util.List;
import java.util.Map;

import org.bitbucket.marrs.sico.actor.lib.base.DomainPort;
import org.bitbucket.marrs.sico.actor.lib.base.LinearModel;
import org.bitbucket.marrs.sico.data.token.FuzzyWeightToken;
import org.bitbucket.marrs.sico.data.type.FuzzyWeightType;

import ptolemy.actor.TypedIOPort;
import ptolemy.data.DoubleToken;
import ptolemy.data.RecordToken;
import ptolemy.data.Token;
import ptolemy.data.expr.Parameter;
import ptolemy.data.type.BaseType;
import ptolemy.data.type.Type;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

import com.google.common.collect.ImmutableList;

public final class Relevance extends LinearModel {

    public static final int MODEL_DIM = 7;

    private static final RecordToken EMPTY_RECORD = new RecordToken();

    private static final String ACTION_TENDENCIES_IN = "actionTendenciesInput";
    private static final String ACTION_TENDENCIES_IN_DISP = "Action Tendencies";

    private static final List<String> INPUTS = ImmutableList.of(DomainPort.ETHICS.inputName, ACTION_TENDENCIES_IN);
    private static final List<Type> INPUT_TYPES = ImmutableList.of(FUZZY_WEIGHT, BaseType.RECORD);

    private static final String GOOD = "good";
    private static final String BAD = "bad";

    private static final String NAME_POSTFIX = "Param";

    /**
     * Parameter defining the key of the parameter for indicative ethics value
     * in the {@link LinearModel#rowMappingInput}.
     */
    public Parameter goodParam;

    /**
     * Parameter defining the key of the parameter for counter-indicative ethics
     * value in the {@link LinearModel#rowMappingInput}.
     */
    public Parameter badParam;

    /**
     * Input port for the encoded values of the features in the ethics domain.
     * The input port is of type {@link BaseType#RECORD} with values of type
     * {@link FuzzyWeightType} containing the encoded weights of the ethics
     * domain.
     */
    public TypedIOPort ethicsInput;

    /**
     * Input port for action tendencies of the features. The output is of type
     * {@link BaseType#RECORD} where the keys are feature names and the values
     * are of type {@link BaseType#RECORD} containing the action tendencies by
     * name of the action type.
     */
    public TypedIOPort actionTendenciesInput;

    public Relevance(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace, INPUTS, INPUT_TYPES, MODEL_DIM);
        init();
    }

    public Relevance(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        super(container, name, INPUTS, INPUT_TYPES, MODEL_DIM);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        this.goodParam = createStringParameter(this, GOOD + NAME_POSTFIX, GOOD);
        this.badParam = createStringParameter(this, BAD + NAME_POSTFIX, BAD);

        this.ethicsInput = getInput(0);
        this.ethicsInput.setDisplayName(DomainPort.ETHICS.inputDisplayName);

        this.actionTendenciesInput = getInput(1);
        this.actionTendenciesInput.setDisplayName(ACTION_TENDENCIES_IN_DISP);
    }

    @Override
    protected double[] parseArgumentsOrdered(List<Token> args, Map<String, Integer> rowMapping) throws IllegalActionException {
        FuzzyWeightToken ethicsToken = FUZZY_WEIGHT.convert(args.get(0));
        RecordToken actionTendenciesRecord = args.get(1).isNil() ? EMPTY_RECORD : (RecordToken) args.get(1);

        double[] orderedArguments = new double[MODEL_DIM];

        orderedArguments[getChecked(rowMapping, stringValue(goodParam))] = ethicsToken.getPositiveWeight();
        orderedArguments[getChecked(rowMapping, stringValue(badParam))] = ethicsToken.getNegativeWeight();

        for (String label : actionTendenciesRecord.labelSet()) {
            orderedArguments[getChecked(rowMapping, label)] = DoubleToken.convert(actionTendenciesRecord.get(label)).doubleValue();
        }

        return orderedArguments;
    }
}
