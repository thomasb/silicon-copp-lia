package org.bitbucket.marrs.sico.selemca.entity;

import java.io.Serializable;

/**
 * Node in a concept-graph.
 * A concept is identified by its name and had a truth value.
 * The equals and hashCode are overwritten to work on only the identifier(s).
 */
public final class Concept implements Serializable {
    private String name;
    private double truthValue;

    public Concept() {
        // Support instantiation by reflection
    }

    public Concept(String name, double truthValue) {
        this.name = name;
        this.truthValue = truthValue;
    }

    public String getName() {
        return name;
    }

    public double getTruthValue() {
        return truthValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Concept)) return false;

        Concept concept = (Concept) o;

        if (!name.equals(concept.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return String.format("%s", name);
    }

}
