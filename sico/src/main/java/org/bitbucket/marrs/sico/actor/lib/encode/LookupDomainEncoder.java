package org.bitbucket.marrs.sico.actor.lib.encode;

import static com.google.common.base.Preconditions.checkArgument;
import static org.bitbucket.marrs.sico.actor.lib.util.FuzzyWeightCalculator.calculateAverage;
import static org.bitbucket.marrs.sico.actor.lib.util.ParameterUtil.createStringParameter;
import static org.bitbucket.marrs.sico.actor.lib.util.ParameterUtil.stringValue;

import java.lang.reflect.Type;
import java.net.URI;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.bitbucket.marrs.sico.actor.lib.util.JsonFileLookup;
import org.bitbucket.marrs.sico.data.token.FuzzyWeightToken;

import ptolemy.data.DoubleToken;
import ptolemy.data.Token;
import ptolemy.data.expr.Parameter;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

import com.google.gson.reflect.TypeToken;

/**
 * A {@code LookupDomainEncoder} reads the encoded values of a feature from a file.
 * <p>
 * The encoder retrieves the indicative and counter-indicative value for its
 * domain from the text file specified in the "Lookup file" parameter and
 * multiplies them with the feature weights provided in the feature input.
 * <p>
 * If a feature is not found in the file, then there is no entry for it
 * contained in the output record of the actor.
 * <p>
 * The file must contain a JSON object with feature names as propertiy names
 * and an array of double containing the indicative and counter-indicative
 * values for the feature in this order.
 */
public final class LookupDomainEncoder extends DomainEncoder {

    private static final Type DOUBLE_MAP_TYPE = new TypeToken<Map<String, List<Double>>>() {/**/}.getType();

    private static final String LOOKUP_FILE = "fileNameParameter";
    private static final String LOOKUP_FILE_DISP = "Lookup file";

    private JsonFileLookup<List<Double>, ?> lookup;

    /**
     * Parameter specifying the path of the file used to lookup the domain
     * values of the features.
     */
    public Parameter fileNameParameter;

    public LookupDomainEncoder(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace);
        init();
    }

    public LookupDomainEncoder(CompositeEntity container, String name) throws NameDuplicationException, IllegalActionException {
        super(container, name);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        this.fileNameParameter = createStringParameter(this, LOOKUP_FILE);
        this.fileNameParameter.setDisplayName(LOOKUP_FILE_DISP);
    }

    @Override
    public void initialize() throws IllegalActionException {
        super.initialize();

        URI lookupURI = Paths.get(stringValue(fileNameParameter)).toUri();
        lookup = JsonFileLookup.create(lookupURI, this::createFuzzyWeightFromValue, DOUBLE_MAP_TYPE);
    }

    private List<Double> createFuzzyWeightFromValue(List<Double> values) {
        checkArgument(values.size() == 2, "Invalid JSON: values must be arrays of two values");

        Double positive = values.get(0);
        Double negative = values.get(1);

        checkArgument(0.0 <= positive && positive <= 1.0, "Failed parsing input file: Domain values must be between 0.0 and 1.0, was %s", positive);
        checkArgument(0.0 <= negative && negative <= 1.0, "Failed parsing input file: Domain values must be between 0.0 and 1.0, was %s", negative);

        return values;
    }

    @Override
    protected Transformation<Token, FuzzyWeightToken> getTransformation() throws IllegalActionException {
        return (label, args) -> {
            Optional<List<Double>> weightsList = lookup.get(label);
            if (!weightsList.isPresent()) {
                return FuzzyWeightToken.NIL;
            }

            double featureWeight = DoubleToken.convert(args.get(0)).doubleValue();

            return new FuzzyWeightToken(featureWeight * weightsList.get().get(0), featureWeight * weightsList.get().get(1));
        };
    }

    @Override
    protected Token aggregateOutput(Collection<FuzzyWeightToken> outputs) throws IllegalActionException {
        return calculateAverage(outputs);
    }
}
