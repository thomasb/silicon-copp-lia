package org.bitbucket.marrs.sico.actor.lib.connect;

import static com.google.common.base.Preconditions.checkState;
import static org.bitbucket.marrs.sico.actor.lib.util.ConceptMetaUtil.find;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createTypedOutputPort;

import java.util.List;

import org.bitbucket.marrs.sico.selemca.entity.ConceptMeta;

import ptolemy.actor.TypedIOPort;
import ptolemy.data.DoubleToken;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

import com.google.gson.Gson;

/**
 * {@code WeightDataConnector} provides weight data.
 * <p>
 * The data can be either retrieved from files or from an epistemics engine.
 * <p>
 * If the data is retrieved from files, the following files must be present in
 * the data directory:
 * <ul>
 *      <li>self.json</li>
 * </ul>
 */
public class WeightDataConnector extends SwitchableDataSourceConnector {

    private static final Gson GSON = new Gson();

    /**
     * Output port for the weight. The port is of type {@link BaseType#DOUBLE}.
     */
    public TypedIOPort weightOutput;

    private String weightRelation;

    public WeightDataConnector(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace);
        init();
    }

    public WeightDataConnector(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        super(container, name);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        weightOutput = createTypedOutputPort(this, "weightMatrixOutput", BaseType.DOUBLE);
    }

    @Override
    public void fire() throws IllegalActionException {
        checkState(weightRelation != null, "Weight relationmust be set");

        super.fire();

        List<ConceptMeta> selfMeta = getDataSource().querySelf();

        ConceptMeta weightMeta = find(weightRelation, selfMeta);
        Double weightValue = GSON.fromJson(weightMeta.getValue(), Double.class);

        weightOutput.send(0, new DoubleToken(weightValue));
    }

    public void setWeightRelation(String weightRelation) {
        this.weightRelation = weightRelation;
    }

    @Override
    public Object clone(Workspace workspace) throws CloneNotSupportedException {
        WeightDataConnector clone = (WeightDataConnector) super.clone(workspace);
        clone.weightRelation = weightRelation;

        return clone;
    }
}
