package org.bitbucket.marrs.sico.actor.lib.connect;

import static org.bitbucket.marrs.sico.actor.lib.util.ConceptMetaUtil.find;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordOutputPort;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.bitbucket.marrs.sico.selemca.entity.ConceptMeta;

import ptolemy.actor.TypedIOPort;
import ptolemy.data.DoubleToken;
import ptolemy.data.RecordToken;
import ptolemy.data.Token;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * {@code SimilarityDataConnector} provides data necessary for the similarity
 * evaluation.
 * <p>
 * The data can be either retrieved from files or from an epistemics engine.
 * <p>
 * If the data is retrieved from files, the following files must be present in
 * the data directory:
 * <ul>
 *      <li>self.json</li>
 * </ul>
 */
public final class SimilarityDataConnector extends SwitchableDataSourceConnector {

    private static final Gson GSON = new Gson();
    private static final Type DOUBLE_MAP_TYPE = new TypeToken<Map<String, Double>>() {/**/}.getType();

    private static final String FEATURES_OUTPUT = "featuresOutput";
    private static final String FEATURES_OUTPUT_DISP = "Features";
    private static final String SIMILARITY_WEIGHTS_OUTPUT = "similarityWeightsOutput";
    private static final String SIMILARITY_WEIGHTS_OUTPUT_DISP = "Similarity Weights";
    private static final String DISSIMILARITY_WEIGHTS_OUTPUT = "dissimilarityWeightsOutput";
    private static final String DISSIMILARITY_WEIGHTS_OUTPUT_DISP = "Dissimilarity Weights";

    private static final String FEATURES_RELATION = "features";
    private static final String SIMILARITY_WEIGHTS_RELATION = "similarity_weights";
    private static final String DISSIMILARITY_WEIGHTS_RELATION = "dissimilarity_weights";

    /**
     * Output port for feature - action associations. The output is of type
     * {@link BaseType#RECORD} where the keys are feature names and the values
     * are arrays of JSON strings that describe the associations of features
     * with actions. The JSON strings contain a JSON object describing the
     * feature in the property "concept1", a JSON object describing the action
     * in the property "concept2" and the truth value of the association in the
     * property "truthValue".
     */
    public TypedIOPort featuresSelfOutput;

    /**
     * Output port for data about the actions. The output is of type
     * {@link BaseType#RECORD} where the keys are action names and the values
     * are JSON strings that hold data about the actions. The JSON strings
     * contain a JSON object describing the action in the property "concept",
     * the name of the data in the property "relation" and a string
     * representation of the value of the data in the property "value".
     */
    public TypedIOPort similarityWeightsOutput;

    /**
     * Output port for action - goal associations. The output is of type
     * {@link BaseType#RECORD} where the keys are action names and the values
     * are arrays of JSON string tuples that describe the associations of
     * actions with the goals.
     * <p>
     * The first entry in the tuple is a JSON object that contains a JSON
     * object describing the action in the property "concept1", a JSON object
     * describing the goal in the property "concept2" and the truth value of
     * the association in the property "truthValue".
     * <p>
     * The second entry in the tuple is a JSON object that contains a JSON
     * object describing data related to the association. It contains a JSON
     * object describing the action in the property "concept1", a JSON object
     * describing the goal in the property "concept2", the name of the data in
     * the property "relation" and a string representation of the value of the
     * data in the property "value".
     */
    public TypedIOPort dissimilarityWeightsOutput;

    public SimilarityDataConnector(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace);
        init();
    }

    public SimilarityDataConnector(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        super(container, name);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        featuresSelfOutput = createRecordOutputPort(this, FEATURES_OUTPUT);
        featuresSelfOutput.setDisplayName(FEATURES_OUTPUT_DISP);
        similarityWeightsOutput = createRecordOutputPort(this, SIMILARITY_WEIGHTS_OUTPUT);
        similarityWeightsOutput.setDisplayName(SIMILARITY_WEIGHTS_OUTPUT_DISP);
        dissimilarityWeightsOutput = createRecordOutputPort(this, DISSIMILARITY_WEIGHTS_OUTPUT);
        dissimilarityWeightsOutput.setDisplayName(DISSIMILARITY_WEIGHTS_OUTPUT_DISP);
    }

    @Override
    public void fire() throws IllegalActionException {
        super.fire();

        List<ConceptMeta> selfMeta = getDataSource().querySelf();

        String featuresJson = find(FEATURES_RELATION, selfMeta).getValue();
        Map<String, Double> featuresMap = GSON.fromJson(featuresJson, DOUBLE_MAP_TYPE);

        String similarityWeightsJson = find(SIMILARITY_WEIGHTS_RELATION, selfMeta).getValue();
        Map<String, Double> similarityWeightsMap = GSON.fromJson(similarityWeightsJson, DOUBLE_MAP_TYPE);

        String dissimilarityWeightsJson = find(DISSIMILARITY_WEIGHTS_RELATION, selfMeta).getValue();
        Map<String, Double> dissimilarityWeightsMap = GSON.fromJson(dissimilarityWeightsJson, DOUBLE_MAP_TYPE);

        featuresSelfOutput.send(0, toRecord(featuresMap));
        similarityWeightsOutput.send(0, toRecord(similarityWeightsMap));
        dissimilarityWeightsOutput.send(0, toRecord(dissimilarityWeightsMap));
    }

    private RecordToken toRecord(Map<String, Double> map) throws IllegalActionException {
        Map<String, Token> recordMap = map.entrySet().stream()
                .collect(Collectors.toMap(Entry::getKey, this::valueToToken));

        return new RecordToken(recordMap);
    }

    private Token valueToToken(Entry<String, Double> entry) {
        Double value = entry.getValue();

        return new DoubleToken(value);
    }
}
