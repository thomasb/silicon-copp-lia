package org.bitbucket.marrs.sico.actor.lib.respond;

import static org.bitbucket.marrs.sico.actor.lib.util.DoubleWeightCalculator.calulateAverage;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createTypedInputPort;
import static org.bitbucket.marrs.sico.data.type.FuzzyWeightType.FUZZY_WEIGHT;

import java.util.Collection;

import org.bitbucket.marrs.sico.actor.lib.base.MultiRecordTransformation;
import org.bitbucket.marrs.sico.algo.respond.IDTFunction;
import org.bitbucket.marrs.sico.data.token.FuzzyWeightToken;
import org.bitbucket.marrs.sico.data.type.FuzzyWeightType;

import ptolemy.actor.NoTokenException;
import ptolemy.actor.TypedIOPort;
import ptolemy.data.DoubleToken;
import ptolemy.data.Token;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

import com.google.common.collect.ImmutableList;

/**
 * Calculates the Involvement-Distance Tradeoff from engagement.
 * <p>
 * Involvement-Distance Tradeoff describes the tradeoff between the indicative
 * (involvement) and counter-indicative (distance) components of engagement.
 * <p>
 * The output value is a double value between 0 and 1.
 */
public final class IDT extends MultiRecordTransformation<DoubleToken> {

    private static final String ENGAGEMENT_IN = "engagementInput";
    private static final String ENGAGEMENT_IN_DISP = "Engagement";
    private static final String WEIGHT_IN = "weightInput";
    private static final String WEIGHT_IN_DISP = "Weight";

    private static final String IDT_OUT_DISP = "Tradeoff";
    private static final String IDT_OUT_TOTAL_DISP = "Tradeoff Total";

    /**
     * Input port for the engagement values of features. The port is of type
     * {@link BaseType#RECORD}. The values are of type {@link FuzzyWeightType}.
     */
    public TypedIOPort engagementInput;

    /**
     * Input port for tradeoff weight. The port is of type
     * {@link BaseType.DoubleType}.
     */
    public TypedIOPort weightInput;

    public IDT(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace, ImmutableList.of(ENGAGEMENT_IN), BaseType.DOUBLE, FUZZY_WEIGHT);
        init();
    }

    public IDT(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        super(container, name, ImmutableList.of(ENGAGEMENT_IN), BaseType.DOUBLE, FUZZY_WEIGHT);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        weightInput = createTypedInputPort(this, WEIGHT_IN, BaseType.DOUBLE);
        weightInput.setDisplayName(WEIGHT_IN_DISP);

        engagementInput = getInput(0);
        engagementInput.setDisplayName(ENGAGEMENT_IN_DISP);

        getOutput().setDisplayName(IDT_OUT_DISP);
        getOutputAggregate().setDisplayName(IDT_OUT_TOTAL_DISP);
    }

    @Override
    protected Transformation<Token, DoubleToken> getTransformation() throws NoTokenException, IllegalActionException {
        DoubleToken weightToken = DoubleToken.convert(weightInput.get(0));
        IDTFunction idt = new IDTFunction(weightToken.doubleValue());

        return (label, args) -> {
            FuzzyWeightToken inputToken = FUZZY_WEIGHT.convert(args.get(0));
            double involvement = inputToken.getPositiveWeight();
            double distance = inputToken.getNegativeWeight();

            return new DoubleToken(idt.apply(involvement, distance));
        };
    }

    @Override
    protected Token aggregateOutput(Collection<DoubleToken> outputs) {
        return calulateAverage(outputs);
    }

    public TypedIOPort getEngagementInput() {
        return engagementInput;
    }

    public TypedIOPort getTradeoffOutput() {
        return getOutput();
    }

    public TypedIOPort getTradeoffOutputTotal() {
        return getOutputAggregate();
    }
}
