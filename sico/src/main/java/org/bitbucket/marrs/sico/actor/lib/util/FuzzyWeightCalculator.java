package org.bitbucket.marrs.sico.actor.lib.util;

import static java.util.stream.Collectors.averagingDouble;
import static java.util.stream.Collectors.toList;
import static org.bitbucket.marrs.sico.data.type.FuzzyWeightType.NEGATIVE;
import static org.bitbucket.marrs.sico.data.type.FuzzyWeightType.POSITIVE;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;

import org.bitbucket.marrs.sico.data.token.FuzzyWeightToken;

import ptolemy.data.DoubleToken;
import ptolemy.data.RecordToken;
import ptolemy.kernel.util.IllegalActionException;

public final class FuzzyWeightCalculator {

    public static FuzzyWeightToken calculateAverage(Collection<FuzzyWeightToken> fuzzyWeights) throws IllegalActionException {
        double positiveAvg = calculateAverage(fuzzyWeights, POSITIVE);
        double negativeAvg = calculateAverage(fuzzyWeights, NEGATIVE);

        return new FuzzyWeightToken(positiveAvg, negativeAvg);
    }

    public static double calculateAverage(Collection<FuzzyWeightToken> fuzzyWeights, String field) {
        ToDoubleFunction<RecordToken> getFieldWeight = weights -> ((DoubleToken) weights.get(field)).doubleValue();

        return fuzzyWeights.stream().collect(averagingDouble(getFieldWeight));
    }

    public static FuzzyWeightToken calculateAverage(RecordToken fuzzyWeightsRecord) throws IllegalActionException {
        List<FuzzyWeightToken> values = fuzzyWeightsRecord.labelSet().stream()
                .map((Function<String, FuzzyWeightToken>) label -> (FuzzyWeightToken) fuzzyWeightsRecord.get(label))
                .collect(toList());

        return calculateAverage(values);
    }
}
