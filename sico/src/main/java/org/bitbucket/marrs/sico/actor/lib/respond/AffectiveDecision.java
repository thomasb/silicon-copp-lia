package org.bitbucket.marrs.sico.actor.lib.respond;

import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordInputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createTypedOutputPort;

import org.bitbucket.marrs.sico.actor.lib.connect.WeightDataConnector;
import org.bitbucket.marrs.sico.data.type.FuzzyWeightType;

import ptolemy.actor.TypedCompositeActor;
import ptolemy.actor.TypedIOPort;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

/**
 * Actor that chooses an action based on use intentions and involvement-distance
 * tradeoff input.
 * <p>
 * The actor calculates the expected satisfaction of the agent for each feature
 * and chooses the action associated with the feature that provides the highest
 * satisfaction values from the Actions input.
 */
public final class AffectiveDecision extends TypedCompositeActor {

    private static final String USE_INTENTIONS_IN = "useIntentionsInput";
    private static final String USE_INTENTIONS_IN_DISP = "Use intentions";
    private static final String TRADEOFF_IN = "tradeoffInput";
    private static final String TRADEOFF_IN_DISP = "Tradeoff";
    private static final String ACTIONS_IN = "actionsInput";
    private static final String ACTIONS_IN_DISP = "Actions";

    private static final String ACTION_OUT = "actionOutput";
    private static final String ACTION_OUT_DISP = "Action";

    private static final String SATISFACTION_CONNECTOR = "satisfactionConnector";
    private static final String SATISFACTION_CONNECTOR_DISP = "Epistemics Connector";
    private static final String SATISFACTION_ACTOR = "satisfaction";
    private static final String SATISFACTION_ACTOR_DISP = "Satisfaction";
    private static final String DECISION_ACTOR = "decision";
    private static final String DECISION_ACTOR_DISP = "Decision";

    private static final String SATISFACTION_WEIGHT_PROPERTY = "satisfaction_weight";

    /**
     * Input port for the use intentions values. The input is of type
     * {@link BaseType#RECORD} with values of type {@link FuzzyWeightType}.
     */
    public TypedIOPort useIntentionsInput;

    /**
     * Input port for the involvement-distance tradeoff values. The input is of
     * type {@link BaseType#RECORD} with values of type {@link BaseType#DOUBLE}.
     */
    public TypedIOPort tradeoffInput;

    /**
     * Input port for actions with maximum utility for a feature. The output
     * is of type {@link BaseType#RECORD} where the keys are feature names and
     * the values the names of the action that maximize the utility for that
     * feature.
     */
    public TypedIOPort actionsInput;

    /**
     * Output port for selected action. The output is of type
     * {@link BaseType#STRING}.
     */
    public TypedIOPort actionOutput;

    public WeightDataConnector satisfactionConnector;
    public Satisfaction satisfaction;
    public MaxSatisfactionDecision decision;

    public AffectiveDecision(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace);
        init();
    }

    public AffectiveDecision(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        super(container, name);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        useIntentionsInput = createRecordInputPort(this, USE_INTENTIONS_IN);
        useIntentionsInput.setDisplayName(USE_INTENTIONS_IN_DISP);
        tradeoffInput = createRecordInputPort(this, TRADEOFF_IN);
        tradeoffInput.setDisplayName(TRADEOFF_IN_DISP);
        actionsInput = createRecordInputPort(this, ACTIONS_IN);
        actionsInput.setDisplayName(ACTIONS_IN_DISP);

        actionOutput = createTypedOutputPort(this, ACTION_OUT, BaseType.STRING);
        actionOutput.setDisplayName(ACTION_OUT_DISP);

        satisfactionConnector = new WeightDataConnector(this, SATISFACTION_CONNECTOR);
        satisfactionConnector.setDisplayName(SATISFACTION_CONNECTOR_DISP);
        satisfactionConnector.setWeightRelation(SATISFACTION_WEIGHT_PROPERTY);

        satisfaction = new Satisfaction(this, SATISFACTION_ACTOR);
        satisfaction.setDisplayName(SATISFACTION_ACTOR_DISP);

        decision = new MaxSatisfactionDecision(this, DECISION_ACTOR);
        decision.setDisplayName(DECISION_ACTOR_DISP);

        connect(satisfactionConnector.weightOutput, satisfaction.weightInput);
        connect(useIntentionsInput, satisfaction.useIntentionsInput);
        connect(tradeoffInput, satisfaction.tradeoffInput);

        connect(actionsInput, decision.actionInput);
        connect(satisfaction.output, decision.satisfactionInput);

        connect(actionOutput, decision.actionOutput);
    }
}
