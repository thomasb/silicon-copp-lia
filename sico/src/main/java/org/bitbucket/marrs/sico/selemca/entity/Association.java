package org.bitbucket.marrs.sico.selemca.entity;


/**
 * Relation between 2 concepts in a concept-graph.
 * A relation is undirected, is identified by its 2 concepts and had a truth value.
 * The equals and hashCode are overwritten to work on only the identifier(s).
 */
public final class Association {

    private Concept concept1;
    private Concept concept2;
    private double truthValue;

    public Association() {
        // Support instantiation by reflection
    }

    public Association(Concept concept1, Concept concept2, double truthValue) {
        this.concept1 = concept1;
        this.concept2 = concept2;
        this.truthValue = truthValue;
    }

    public Concept getConcept1() {
        return concept1;
    }

    public Concept getConcept2() {
        return concept2;
    }

    public double getTruthValue() {
        return truthValue;
    }

    public Concept getOtherConcept(Concept concept) {
        if (concept.equals(concept1)) {
            return concept2;
        }
        if (concept.equals(concept2)) {
            return concept1;
        }
        throw new IllegalArgumentException(String.format("Concept %s not part of association %s", concept, this));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Association)) return false;

        Association that = (Association) o;

        if (!concept1.equals(that.concept1)) return false;
        if (!concept2.equals(that.concept2)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = concept1.hashCode();
        result = 31 * result + concept2.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return String.format("%s -%.2f- %s", concept1, truthValue, concept2);
    }
}
