package org.bitbucket.marrs.sico.actor.lib.connect;

import java.util.List;
import java.util.Optional;

import org.bitbucket.marrs.sico.selemca.entity.Association;
import org.bitbucket.marrs.sico.selemca.entity.AssociationMeta;
import org.bitbucket.marrs.sico.selemca.entity.ConceptMeta;

import ptolemy.kernel.util.IllegalActionException;

interface DataSource {

    List<Association> queryActions(String featureName) throws IllegalActionException;

    Optional<ConceptMeta> queryActionMeta(String actionName) throws IllegalActionException;

    List<Association> queryGoals(String featureName) throws IllegalActionException;

    AssociationMeta queryActionGoalMeta(String actionName, String goalName) throws IllegalActionException;

    Optional<ConceptMeta> queryGoalMeta(String actionName) throws IllegalActionException;

    List<ConceptMeta> querySelf() throws IllegalActionException;

}
