package org.bitbucket.marrs.sico.actor.lib.compare;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordInputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordOutputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.RecordTokenConverter.doubleMapConverter;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

import org.bitbucket.marrs.sico.algo.compare.ActionGoalTransition;
import org.bitbucket.marrs.sico.algo.compare.UtilityFunction;
import org.bitbucket.marrs.sico.algo.compare.UtilityFunction.UtilityResult;
import org.bitbucket.marrs.sico.selemca.entity.AssociationData;
import org.bitbucket.marrs.sico.selemca.entity.ConceptMeta;

import ptolemy.actor.TypedAtomicActor;
import ptolemy.actor.TypedIOPort;
import ptolemy.data.ArrayToken;
import ptolemy.data.ObjectToken;
import ptolemy.data.RecordToken;
import ptolemy.data.StringToken;
import ptolemy.data.Token;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

public class Utility extends TypedAtomicActor {

    private static final UtilityFunction UTILITY_FUNCTION = new UtilityFunction();

    private static final Gson GSON = new Gson();
    private static final Type ASSOCIATION_COLLECTION = new TypeToken<Collection<AssociationData>>(){/**/}.getType();

    private static final String FEATURE_TO_ACTION_IN = "featureToActionInput";
    private static final String FEATURE_TO_ACTION_IN_DISP = "Feature - Actions";
    private static final String ACTION_IN = "actionInput";
    private static final String ACTION_IN_DISP = "Actions";
    private static final String ACTION_TO_GOAL_IN = "actionToGoalInput";
    private static final String ACTION_TO_GOAL_IN_DISP = "Action - Goals";
    private static final String GOAL_IN = "goalInput";
    private static final String GOAL_IN_DISP = "Goals";

    private static final String TRANSITIONS_OUT = "transitionsOutput";
    private static final String TRANSITIONS_OUT_DISP = "Transitions";
    private static final String MAX_UTIL_ACTION_OUT = "maxUtilityActionOutput";
    private static final String MAX_UTIL_ACTION_OUT_DISP = "Max Utility Actions";
    private static final String ACTION_TENDENCIES_OUT = "actionTendenciesOutput";
    private static final String ACTION_TENDENCIES_OUT_DISP = "Action Tendencies";

    /**
     * Input port for feature - action associations. The port is of type
     * {@link BaseType#RECORD} where the keys are feature names and the values
     * are arrays of JSON strings that describe the associations of features
     * with actions. The JSON strings contain a JSON object describing the
     * feature in the property "concept1", a JSON object describing the action
     * in the property "concept2" and the truth value of the association in the
     * property "truthValue".
     */
    public TypedIOPort featureToActionInput;

    /**
     * Input port for data about the actions. The port is of type
     * {@link BaseType#RECORD} where the keys are action names and the values
     * are JSON strings that hold data about the actions. The JSON strings
     * contain a JSON object describing the action in the property "concept",
     * the name of the data in the property "relation" and a string
     * representation of the value of the data in the property "value".
     */
    public TypedIOPort actionInput;

    /**
     * Input port for action - goal associations. The port is of type
     * {@link BaseType#RECORD} where the keys are action names and the values
     * are arrays of JSON string tuples that describe the associations of
     * actions with the goals.
     * <p>
     * The first entry in the tuple is a JSON object that contains a JSON
     * object describing the action in the property "concept1", a JSON object
     * describing the goal in the property "concept2" and the truth value of
     * the association in the property "truthValue".
     * <p>
     * The second entry in the tuple is a JSON object that contains a JSON
     * object describing data related to the association. It contains a JSON
     * object describing the action in the property "concept1", a JSON object
     * describing the goal in the property "concept2", the name of the data in
     * the property "relation" and a string representation of the value of the
     * data in the property "value".
     */
    public TypedIOPort actionToGoalInput;

    /**
     * Input port for data about the goals. The port is of type
     * {@link BaseType#RECORD} where the keys are goal names and the values
     * are arrays of JSON strings that hold data about the goals. The JSON
     * strings contain a JSON object describing the goal in the property
     * "concept", the name of the data in the property "relation" and a string
     * representation of the value of the data in the property "value".
     */
    public TypedIOPort goalInput;

    /**
     * Output port for transitions from actions to goals. The output is of type
     * {@link BaseType#RECORD} where the keys are action names and the values
     * are arrays of {@link ActionGoalTransition} object tokens. The contained
     * actions are restricted to the actions that have maximum utility for at
     * least one feature.
     */
    public TypedIOPort transitionsOutput;

    /**
     * Output port for actions with maximum utility for a feature. The output
     * is of type {@link BaseType#RECORD} where the keys are feature names and
     * the values the names of the action that maximize the utility for that
     * feature.
     */
    public TypedIOPort maxUtilityActionOutput;

    /**
     * Output port for action tendencies of the features. The output is of type
     * {@link BaseType#RECORD} where the keys are feature names and the values
     * are of type {@link BaseType#RECORD} containing the action tendencies by
     * name of the action type.
     */
    public TypedIOPort actionTendenciesOutput;

    public Utility(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace);
        init();
    }

    public Utility(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        super(container, name);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        featureToActionInput = createRecordInputPort(this, FEATURE_TO_ACTION_IN);
        featureToActionInput.setDisplayName(FEATURE_TO_ACTION_IN_DISP);

        actionInput = createRecordInputPort(this, ACTION_IN);
        actionInput.setDisplayName(ACTION_IN_DISP);

        actionToGoalInput = createRecordInputPort(this, ACTION_TO_GOAL_IN);
        actionToGoalInput.setDisplayName(ACTION_TO_GOAL_IN_DISP);

        goalInput = createRecordInputPort(this, GOAL_IN);
        goalInput.setDisplayName(GOAL_IN_DISP);

        transitionsOutput = createRecordOutputPort(this, TRANSITIONS_OUT);
        transitionsOutput.setDisplayName(TRANSITIONS_OUT_DISP);

        maxUtilityActionOutput = createRecordOutputPort(this, MAX_UTIL_ACTION_OUT);
        maxUtilityActionOutput.setDisplayName(MAX_UTIL_ACTION_OUT_DISP);

        actionTendenciesOutput = createRecordOutputPort(this, ACTION_TENDENCIES_OUT);
        actionTendenciesOutput.setDisplayName(ACTION_TENDENCIES_OUT_DISP);
    }

    @Override
    public void fire() throws IllegalActionException {
        super.fire();

        RecordToken featureToActions = (RecordToken) featureToActionInput.get(0);
        Map<String, Collection<AssociationData>> featureActionAssociations = parse(featureToActions, ASSOCIATION_COLLECTION);

        RecordToken actions = (RecordToken) actionInput.get(0);
        Map<String, ConceptMeta> actionMeta = parse(actions, ConceptMeta.class);

        RecordToken actionsToGoals = (RecordToken) actionToGoalInput.get(0);
        Map<String, Collection<AssociationData>> actionGoalTransitions = parse(actionsToGoals, ASSOCIATION_COLLECTION);

        RecordToken goals = (RecordToken) goalInput.get(0);
        Map<String, ConceptMeta> goalMeta = parse(goals, ConceptMeta.class);

        Map<String, UtilityResult> featureUtilities = new HashMap<>();
        List<ActionGoalTransition> allTransitions = new ArrayList<>();
        Set<String> maxActionNames = new HashSet<>();
        for (String featureName : featureActionAssociations.keySet()) {
            List<ActionGoalTransition> transitions = featureActionAssociations.get(featureName).stream()
                    .flatMap(resolveTransitions(actionGoalTransitions, actionMeta, goalMeta))
                    .collect(toList());
            if (transitions.isEmpty()) {
                continue;
            }

            allTransitions.addAll(transitions);

            UtilityResult utilityResult = UTILITY_FUNCTION.calculate(transitions);
            featureUtilities.put(featureName, utilityResult);
            maxActionNames.add(utilityResult.getMaxUtilityAction());
        }

        Map<String, Token> featureActions = featureUtilities.entrySet().stream()
                .collect(toMap(Entry::getKey, e -> new StringToken(e.getValue().getMaxUtilityAction())));
        maxUtilityActionOutput.send(0, new RecordToken(featureActions));

        Map<String, Token> actionTendencies = featureUtilities.entrySet().stream()
                .collect(toMap(Entry::getKey, this::convertToRecord));
        actionTendenciesOutput.send(0, new RecordToken(actionTendencies));

        Map<String, Token> actionTransitions = allTransitions.stream()
                .filter(transition -> maxActionNames.contains(transition.getActionName()))
                .collect(groupingBy(ActionGoalTransition::getActionName, mapping(this::createObjectToken, toList())))
                .entrySet().stream()
                .collect(toMap(Entry::getKey, this::createArrayTokenFromValue));
        transitionsOutput.send(0, new RecordToken(actionTransitions));
    }

    private Function<AssociationData, Stream<ActionGoalTransition>> resolveTransitions(
            Map<String, Collection<AssociationData>> actionGoalTransitions,
            Map<String, ConceptMeta> actionMeta,
            Map<String, ConceptMeta> goalMeta) {
        return association -> {
                String actionName = association.getAction(actionMeta.keySet()).getName();
                double truthValue = association.getAssociation().getTruthValue();

                return actionGoalTransitions.get(actionName).stream()
                        .map(transition -> new ActionGoalTransition(transition, actionMeta, goalMeta, truthValue));
        };
    }

    private <T> Map<String, T> parse(RecordToken record, Class<T> clazz) throws IllegalActionException {
        return parse(record, json -> GSON.fromJson(json, clazz));
    }

    private <T> Map<String, T> parse(RecordToken record, Type type) throws IllegalActionException {
        return parse(record, json -> GSON.fromJson(json, type));
    }

    private <T> Map<String, T> parse(RecordToken record, Function<String, T> parse) throws IllegalActionException {
        Map<String, T> result = new HashMap<>();
        for (String label : record.labelSet()) {
            String json = StringToken.convert(record.get(label)).stringValue();
            result.put(label, parse.apply(json));
        }

        return result;
    }

    private Token convertToRecord(Map.Entry<String, UtilityResult> utilitesEntry) {
        try {
            return doubleMapConverter().convert(utilitesEntry.getValue().getActionTendencies());
        } catch (IllegalActionException ex) {
            throw new RuntimeException(ex);
        }
    }

    private Token createObjectToken(ActionGoalTransition trans) {
        try {
            return new ObjectToken(trans, ActionGoalTransition.class);
        } catch (IllegalActionException e) {
            // This should never happen
            throw new RuntimeException(e);
        }
    }

    private Token createArrayTokenFromValue(Map.Entry<String, List<Token>> entry) {
        List<Token> list = entry.getValue();
        try {
            return new ArrayToken(list.toArray(new Token[0]));
        } catch (IllegalActionException e) {
            // This should never happen
            throw new RuntimeException(e);
        }
    }
}
