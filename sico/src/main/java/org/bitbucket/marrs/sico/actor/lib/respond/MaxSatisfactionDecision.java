package org.bitbucket.marrs.sico.actor.lib.respond;

import static java.lang.Double.compare;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordInputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createTypedOutputPort;

import java.util.Optional;

import ptolemy.actor.TypedAtomicActor;
import ptolemy.actor.TypedIOPort;
import ptolemy.data.DoubleToken;
import ptolemy.data.RecordToken;
import ptolemy.data.StringToken;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

/**
 * Chooses an action based on the expected satisfaction of features.
 * <p>
 * From the satisfaction input the feature with the maximum satisfaction is
 * selected and the related action is retrieved from an externally provided
 * action-to-feature map.
 * <p>
 * The result is the name of the selected action.
 */
public final class MaxSatisfactionDecision extends TypedAtomicActor {

    private static final String SATISFACTION_IN = "satisfactionInput";
    private static final String SATISFACTION_IN_DISP = "Satisfaction";
    private static final String ACTION_IN = "actionInput";
    private static final String ACTION_IN_DISP = "Feature actions";
    private static final String ACTION_OUT = "actionOutput";
    private static final String ACTION_OUT_DISP = "Action";

    /**
     * Input port for the satisfaction values of features. The input is of type
     * {@link BaseType#RECORD} with values of type {@link BaseType#DOUBLE}.
     */
    public TypedIOPort satisfactionInput;

    /**
     * Input port for the actions related to a feature. The input is of type
     * {@link BaseType#RECORD} and maps feature names to action names, i.e. keys
     * and values are of type {@link BaseType#STRING}.
     */
    public TypedIOPort actionInput;

    /**
     * Output port for selected action. The output is of type
     * {@link BaseType#STRING}.
     */
    public TypedIOPort actionOutput;

    public MaxSatisfactionDecision(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace);
        init();
    }

    public MaxSatisfactionDecision(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        super(container, name);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        satisfactionInput = createRecordInputPort(this, SATISFACTION_IN);
        satisfactionInput.setDisplayName(SATISFACTION_IN_DISP);
        actionInput = createRecordInputPort(this, ACTION_IN);
        actionInput.setDisplayName(ACTION_IN_DISP);

        actionOutput = createTypedOutputPort(this, ACTION_OUT, BaseType.STRING);
        actionOutput.setDisplayName(ACTION_OUT_DISP);
    }

    @Override
    public void fire() throws IllegalActionException {
        try {
            doFire();
        } catch (WrappedPtolemyException e) {
            throw (IllegalActionException) e.getCause();
        }
    }

    private void doFire() throws IllegalActionException {
        super.fire();

        RecordToken satisfactions = (RecordToken) satisfactionInput.get(0);

        Optional<StringToken> action = satisfactions.labelSet().parallelStream()
                .max((left, right) -> compareValues(satisfactions, left, right))
                .map(this::getAction);

        actionOutput.send(0, action.orElse(StringToken.NIL));
    }

    private int compareValues(RecordToken satisfactions, String left, String right) {
        double leftValue = getValue(satisfactions, left);
        double rightValue = getValue(satisfactions, right);

        return compare(leftValue, rightValue);
    }

    private double getValue(RecordToken satisfactions, String label) {
        try {
            return DoubleToken.convert(satisfactions.get(label)).doubleValue();
        } catch (IllegalActionException e) {
            throw new WrappedPtolemyException(e);
        }
    }

    private StringToken getAction(String feature) {
        try {
            RecordToken actionsMap = (RecordToken) actionInput.get(0);

            return StringToken.convert(actionsMap.get(feature));
        } catch (IllegalActionException e) {
            throw new WrappedPtolemyException(e);
        }
    }

    private static class WrappedPtolemyException extends RuntimeException {
        // Wrap checked exception
        WrappedPtolemyException(IllegalActionException e) {
            super(e);
        }
    }
}
