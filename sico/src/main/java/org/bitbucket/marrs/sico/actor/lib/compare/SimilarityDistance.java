package org.bitbucket.marrs.sico.actor.lib.compare;

import static java.util.stream.Collectors.toList;
import static org.bitbucket.marrs.sico.actor.lib.util.FuzzyWeightCalculator.calculateAverage;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordInputPort;
import static org.bitbucket.marrs.sico.data.type.FuzzyWeightType.FUZZY_WEIGHT;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import org.bitbucket.marrs.sico.actor.lib.base.DomainPort;
import org.bitbucket.marrs.sico.actor.lib.base.MultiRecordTransformation;
import org.bitbucket.marrs.sico.algo.compare.SimilarityDistanceFunction;
import org.bitbucket.marrs.sico.data.token.FuzzyWeightToken;
import org.bitbucket.marrs.sico.data.type.FuzzyWeightType;

import ptolemy.actor.TypedIOPort;
import ptolemy.data.DoubleToken;
import ptolemy.data.RecordToken;
import ptolemy.data.Token;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.common.collect.ImmutableSet;

/**
 * Similarity distance calculates the similarity and dissimilarity between the
 * features input and a feature input for self.
 * <p>
 * For features present both in the input feature set and the feature set of
 * self, distance is calculated based on euclidean distance between the
 * indicative and counter-indicative values of the features, and are fed with
 * externally provided weights into similarity and dissimilarity.
 * <p>
 * Features that are only present in one of the feature set feed only into
 * dissimilarity.
 * <p>
 * The output is a token of {@link FuzzyWeightType} containing similarity as
 * indicative and dissimilarity as counter-indicative value.
 */
public final class SimilarityDistance extends MultiRecordTransformation<FuzzyWeightToken> {

    private static final List<String> WEIGHT_LABELS = ImmutableList.of(
            "ethicsIndicative", "ethicsCounter",
            "affordancesIndicative", "affordancesCounter",
            "aestheticsIndicative", "aestheticsCounter",
            "epistemicsIndicative", "epistemicsCounter");
    private static final Set<String> WEIGHT_LABEL_SET = ImmutableSet.copyOf(WEIGHT_LABELS);

    private static final String SELF_SUFFIX = " Self";

    private static final List<String> INPUTS;
    static {
        List<String> inputPortNames = DomainPort.inputNames();

        Builder<String> inputs = new ImmutableList.Builder<>();
        inputs.addAll(inputPortNames);
        inputs.addAll(inputPortNames.stream()
                .map(name -> name + "Self")
                .collect(toList()));

        INPUTS = inputs.build();
    }

    private static final String SIM_WEIGHT_IN = "similarityWeightInput";
    private static final String SIM_WEIGHT_IN_DISP = "Similarity Weights";

    private static final String DISSIM_WEIGHT_IN = "dissimilarityWeightInput";
    private static final String DISSIM_WEIGHT_IN_DISP = "Dissimilarity Weights";

    /**
     * Input port for ethics values. The input port is of type
     * {@link BaseType#RECORD} with values of type {@link FuzzyWeightType}.
     */
    public TypedIOPort ethicsInput;

    /**
     * Input port for ethics values of the agent itself. The input port is of
     * type {@link BaseType#RECORD} with values of type {@link FuzzyWeightType}.
     */
    public TypedIOPort ethicsInputSelf;

    /**
     * Input port for affordances values. The input port is of type
     * {@link BaseType#RECORD} with values of type {@link FuzzyWeightType}.
     */
    public TypedIOPort affordancesInput;

    /**
     * Input port for affordances values of the agent itself. The input port is
     * of type {@link BaseType#RECORD} with values of type
     * {@link FuzzyWeightType}.
     */
    public TypedIOPort affordancesInputSelf;

    /**
     * Input port for aesthetics values. The input port is of type
     * {@link BaseType#RECORD} with values of type {@link FuzzyWeightType}.
     */
    public TypedIOPort aestheticsInput;

    /**
     * Input port for aesthetics values of the agent itself. The input port is
     * of type {@link BaseType#RECORD} with values of type
     * {@link FuzzyWeightType}.
     */
    public TypedIOPort aestheticsInputSelf;

    /**
     * Input port for epistemics values. The input port is of type
     * {@link BaseType#RECORD} with values of type {@link FuzzyWeightType}.
     */
    public TypedIOPort epistemicsInput;

    /**
     * Input port for epistemics values of the agent itself. The input port is
     * of type {@link BaseType#RECORD} with values of type
     * {@link FuzzyWeightType}.
     */
    public TypedIOPort epistemicsInputSelf;

    /**
     * Input port for the parameters used to calculate the similarity. The
     * input port is of type {@link BaseType#RECORD} with values of type
     * {@link BaseType#DOUBLE}.
     */
    public TypedIOPort simmilarityWeightInput;

    /**
     * Input port for the parameters used to calculate the dissimilarity. The
     * input port is of type {@link BaseType#RECORD} with values of type
     * {@link BaseType#DOUBLE}.
     */
    public TypedIOPort dissimmilarityWeightInput;

    public SimilarityDistance(CompositeEntity container, String name) throws NameDuplicationException, IllegalActionException {
        super(container, name, INPUTS, FUZZY_WEIGHT, FUZZY_WEIGHT);

        ethicsInput = getInput(DomainPort.ETHICS.ordinal());
        ethicsInput.setDisplayName(DomainPort.ETHICS.inputDisplayName);

        affordancesInput = getInput(DomainPort.AFFORDANCES.ordinal());
        affordancesInput.setDisplayName(DomainPort.AFFORDANCES.inputDisplayName);

        aestheticsInput = getInput(DomainPort.AESTHETICS.ordinal());
        aestheticsInput.setDisplayName(DomainPort.AESTHETICS.inputDisplayName);

        epistemicsInput = getInput(DomainPort.EPISTEMICS.ordinal());
        epistemicsInput.setDisplayName(DomainPort.EPISTEMICS.inputDisplayName);

        int portCount = DomainPort.values().length;
        ethicsInputSelf = getInput(DomainPort.ETHICS.ordinal() + portCount);
        ethicsInput.setDisplayName(DomainPort.AESTHETICS.inputDisplayName + SELF_SUFFIX);

        affordancesInputSelf = getInput(DomainPort.AFFORDANCES.ordinal() + portCount);
        affordancesInput.setDisplayName(DomainPort.AFFORDANCES.inputDisplayName + SELF_SUFFIX);

        aestheticsInputSelf = getInput(DomainPort.AESTHETICS.ordinal() + portCount);
        aestheticsInput.setDisplayName(DomainPort.AESTHETICS.inputDisplayName + SELF_SUFFIX);

        epistemicsInputSelf = getInput(DomainPort.EPISTEMICS.ordinal() + portCount);
        epistemicsInput.setDisplayName(DomainPort.EPISTEMICS.inputDisplayName + SELF_SUFFIX);

        simmilarityWeightInput = createRecordInputPort(this, SIM_WEIGHT_IN);
        simmilarityWeightInput.setDisplayName(SIM_WEIGHT_IN_DISP);

        dissimmilarityWeightInput = createRecordInputPort(this, DISSIM_WEIGHT_IN);
        dissimmilarityWeightInput.setDisplayName(DISSIM_WEIGHT_IN_DISP);
    }

    @Override
    protected Transformation<Token, FuzzyWeightToken> getTransformation() throws IllegalActionException {
        RecordToken similarityWeightRecord = (RecordToken) simmilarityWeightInput.get(0);
        RecordToken dissimilarityWeightRecord = (RecordToken) dissimmilarityWeightInput.get(0);

        double[] similarityWeights = readWeightData(similarityWeightRecord);
        SimilarityDistanceFunction similarityNorm = SimilarityDistanceFunction.forFeatureIntersection(similarityWeights);

        double[] dissimilarityWeights = readWeightData(dissimilarityWeightRecord);
        SimilarityDistanceFunction dissimilarityNorm = SimilarityDistanceFunction.forFeatureUnion(dissimilarityWeights);

        return (label, args) -> {
                FuzzyWeightToken ethics = FUZZY_WEIGHT.convert(args.get(DomainPort.ETHICS.ordinal()));
                FuzzyWeightToken aesthetics = FUZZY_WEIGHT.convert(args.get(DomainPort.AESTHETICS.ordinal()));
                FuzzyWeightToken affordances = FUZZY_WEIGHT.convert(args.get(DomainPort.AFFORDANCES.ordinal()));
                FuzzyWeightToken epistemics = FUZZY_WEIGHT.convert(args.get(DomainPort.EPISTEMICS.ordinal()));

                int portCount = DomainPort.values().length;
                FuzzyWeightToken ethicsSelf = FUZZY_WEIGHT.convert(args.get(DomainPort.ETHICS.ordinal() + portCount));
                FuzzyWeightToken affordancesSelf = FUZZY_WEIGHT.convert(args.get(DomainPort.AFFORDANCES.ordinal() + portCount));
                FuzzyWeightToken aestheticsSelf = FUZZY_WEIGHT.convert(args.get(DomainPort.AESTHETICS.ordinal() + portCount));
                FuzzyWeightToken epistemicsSelf = FUZZY_WEIGHT.convert(args.get(DomainPort.EPISTEMICS.ordinal() + portCount));

                RealVector other = readData(ethics, aesthetics, affordances, epistemics);
                RealVector self = readData(ethicsSelf, aestheticsSelf, affordancesSelf, epistemicsSelf);

                boolean isFeatureOfOther = isFeature(ethics, aesthetics, affordances, epistemics);
                boolean isFeatureOfSelf = isFeature(ethicsSelf, aestheticsSelf, affordancesSelf, epistemicsSelf);

                double similarity = 1.0 - similarityNorm.apply(other, self, isFeatureOfOther, isFeatureOfSelf);
                double dissimilarity = dissimilarityNorm.apply(other, self, isFeatureOfOther, isFeatureOfSelf);

                return new FuzzyWeightToken(similarity, dissimilarity);
        };
    }

    private double[] readWeightData(RecordToken similarityWeightRecord) throws IllegalActionException {
        if (!WEIGHT_LABEL_SET.equals(similarityWeightRecord.labelSet())) {
            throw new IllegalActionException("Missing weight parameters, expected: " + WEIGHT_LABELS + ", was: " + similarityWeightRecord.labelSet());
        }

        return WEIGHT_LABELS.stream()
                .map(label -> similarityWeightRecord.get(label))
                .mapToDouble(token -> ((DoubleToken) token).doubleValue())
                .toArray();
    }

    private RealVector readData(FuzzyWeightToken ethics,
            FuzzyWeightToken aesthetics,
            FuzzyWeightToken affordances,
            FuzzyWeightToken epistemics) {
        double[] data = { ethics.getPositiveWeight(), ethics.getNegativeWeight(),
                affordances.getPositiveWeight(), affordances.getNegativeWeight(),
                aesthetics.getPositiveWeight(), aesthetics.getNegativeWeight(),
                epistemics.getPositiveWeight(), epistemics.getNegativeWeight() };

        return new ArrayRealVector(data, false);
    }

    private boolean isFeature(FuzzyWeightToken ethics,
            FuzzyWeightToken aesthetics,
            FuzzyWeightToken affordances,
            FuzzyWeightToken epistemics) {
        boolean allNil = ethics.equals(FuzzyWeightToken.NIL) &&
                affordances.equals(FuzzyWeightToken.NIL) &&
                aesthetics.equals(FuzzyWeightToken.NIL) &&
                epistemics.equals(FuzzyWeightToken.NIL);

        return !allNil;
    }

    @Override
    protected Token aggregateOutput(Collection<FuzzyWeightToken> outputs) throws IllegalActionException {
        return calculateAverage(outputs);
    }

    public TypedIOPort getSimilarityOutput() {
        return output;
    }

    public TypedIOPort getSimilarityOutputTotal() {
        return getOutputAggregate();
    }
}
