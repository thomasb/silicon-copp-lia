package org.bitbucket.marrs.sico.data.type;

import org.bitbucket.marrs.sico.data.token.FuzzyWeightToken;

import ptolemy.data.DoubleToken;
import ptolemy.data.RecordToken;
import ptolemy.data.Token;
import ptolemy.data.type.BaseType;
import ptolemy.data.type.RecordType;
import ptolemy.data.type.Type;
import ptolemy.kernel.util.IllegalActionException;

import com.google.common.collect.ImmutableSet;

public class FuzzyWeightType extends RecordType {

    public static final String POSITIVE = "indicative";
    public static final String NEGATIVE = "counter-indicative";

    public static final String[] LABELS = { POSITIVE, NEGATIVE };
    public static final Type[] TYPES = { BaseType.DOUBLE, BaseType.DOUBLE };
    private static final ImmutableSet<String> LABEL_SET = ImmutableSet.of(POSITIVE, NEGATIVE);

    public static final FuzzyWeightType FUZZY_WEIGHT = new FuzzyWeightType();

    private FuzzyWeightType() {
        super(LABELS, TYPES);
    }

    @Override
    public boolean isCompatible(Type type) {
        return super.isCompatible(type) && ((RecordType) type).labelSet().equals(LABEL_SET);
    }

    @Override
    public FuzzyWeightToken convert(Token token) throws IllegalActionException {
        if (token instanceof FuzzyWeightToken) {
            return (FuzzyWeightToken) token;
        }

        if (token.isNil()) {
            return FuzzyWeightToken.NIL;
        }

        RecordToken recordToken = (RecordToken) token;
        if (!LABEL_SET.equals(recordToken.labelSet())) {
            throw new IllegalActionException("Cannot convert to FuzzyWeightToken, illegal label set: " + recordToken.labelSet());
        }

        DoubleToken positive = DoubleToken.convert(recordToken.get(POSITIVE));
        DoubleToken negative = DoubleToken.convert(recordToken.get(NEGATIVE));

        return new FuzzyWeightToken(positive, negative);
    }
}
