package org.bitbucket.marrs.sico.actor.lib.compare;

import static org.bitbucket.marrs.sico.actor.lib.base.DomainPort.AFFORDANCES;
import static org.bitbucket.marrs.sico.actor.lib.base.DomainPort.ETHICS;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordInputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordOutputPort;

import org.bitbucket.marrs.sico.actor.lib.connect.AppraisalDataConnector;
import org.bitbucket.marrs.sico.data.type.FuzzyWeightType;

import ptolemy.actor.TypedCompositeActor;
import ptolemy.actor.TypedIOPort;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.ComponentRelation;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

/**
 * Composite actor for the appraisal process.
 * <p>
 * The actor calculates Relevance and Valence based on the features of the
 * other agent, the encoded values of the ethics and affordances domains.
 * <p>
 * As a byproduct the actor provides the action with the highest utility value
 * for each feature.
 */
public final class Appraisal extends TypedCompositeActor {

    private static final String FEATURES_IN = "featuresInput";
    private static final String FEATURES_IN_DISP = "Features";

    private static final String VALENCE_OUT_TOTAL = "valenceOutputTotal";
    private static final String VALENCE_OUT_TOTAL_DISP = "Valence total";
    private static final String VALENCE_OUT = "valenceOutput";
    private static final String VALENCE_OUT_DISP = "Valence";

    private static final String RELEVANCE_OUT = "relevanceOutput";
    private static final String RELEVANCE_OUT_DISP = "Relevance";
    private static final String RELEVANCE_OUT_TOTAL = "relevanceOutputTotal";
    private static final String RELEVANCE_OUT_TOTAL_DISP = "Relevance total";

    private static final String MAX_UTILITY_ACTIONS_OUTPUT = "maxUtilityActionsOutput";
    private static final String MAX_UTILITY_ACTIONS_OUTPUT_DISP = "Actions";

    private static final String UTILITY_ACTOR = "utility";
    private static final String UTILITY_ACTOR_DISP = "Utility";
    private static final String RELEVANCE_ACTOR = "relevance";
    private static final String RELEVANCE_ACTOR_DISP = "Relevance";
    private static final String VALENCE_ACTOR = "valence";
    private static final String VALENCE_ACTOR_DISP = "Valence";
    private static final String CONNECTOR = "connector";
    private static final String CONNECTOR_DISP = "Epistemics connector";

    /**
     * Input port for the features. The port is of type {@link BaseType#RECORD}
     * with values of type {@link BaseType#DOUBLE}.
     */
    public TypedIOPort featuresInput;

    /**
     * Input port for the encoded values of the features in the ethics domain.
     * The input port is of type {@link BaseType#RECORD} with values of type
     * {@link FuzzyWeightType} containing the encoded weights of the ethics
     * domain.
     */
    public TypedIOPort ethicsInput;

    /**
     * Input port for the encoded values of the features in the affordances
     * domain. The input port is of type {@link BaseType#RECORD} with values of
     * type {@link FuzzyWeightType} containing the encoded weights of the
     * affordances domains.
     */
    public TypedIOPort affordancesInput;

    /**
     * Output port for the valence values of the features. The output port is of
     * type {@link BaseType#RECORD} with values of type {@link FuzzyWeightType}.
     */
    public TypedIOPort valenceOutput;

    /**
     * Output port for the aggregate valence value. The output port is of type
     * {@link FuzzyWeightType}.
     */
    public TypedIOPort valenceOutputTotal;

    /**
     * Output port for the relevance values of the features. The output port is of
     * type {@link BaseType#RECORD} with values of type {@link FuzzyWeightType}.
     */
    public TypedIOPort relevanceOutput;

    /**
     * Output port for the aggregate relevance value. The output port is of type
     * {@link FuzzyWeightType}.
     */
    public TypedIOPort relevanceOutputTotal;

    /**
     * Output port for the actions with maximum utility for each feature. The output
     * port is of type {@link BaseType#RECORD} with values of type
     * {@link BaseType#STRING}. The values represent the action names.
     */
    public TypedIOPort maxUtilityActionsOutput;

    public AppraisalDataConnector dataConnector;
    public Utility utility;
    public Relevance relevance;
    public Valence valence;

    public Appraisal(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace);
        init();
    }

    public Appraisal(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        super(container, name);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        featuresInput = createRecordInputPort(this, FEATURES_IN);
        featuresInput.setDisplayName(FEATURES_IN_DISP);

        ethicsInput = createRecordInputPort(this, ETHICS.inputName);
        ethicsInput.setDisplayName(ETHICS.inputDisplayName);

        affordancesInput = createRecordInputPort(this, AFFORDANCES.inputName);
        affordancesInput.setDisplayName(AFFORDANCES.inputDisplayName);

        valenceOutput = createRecordOutputPort(this, VALENCE_OUT);
        valenceOutput.setDisplayName(VALENCE_OUT_DISP);
        valenceOutputTotal = createRecordOutputPort(this, VALENCE_OUT_TOTAL);
        valenceOutputTotal.setDisplayName(VALENCE_OUT_TOTAL_DISP);

        relevanceOutput = createRecordOutputPort(this, RELEVANCE_OUT);
        relevanceOutput.setDisplayName(RELEVANCE_OUT_DISP);
        relevanceOutputTotal = createRecordOutputPort(this, RELEVANCE_OUT_TOTAL);
        relevanceOutputTotal.setDisplayName(RELEVANCE_OUT_TOTAL_DISP);

        maxUtilityActionsOutput = createRecordOutputPort(this, MAX_UTILITY_ACTIONS_OUTPUT);
        maxUtilityActionsOutput.setDisplayName(MAX_UTILITY_ACTIONS_OUTPUT_DISP);

        dataConnector = new AppraisalDataConnector(this, CONNECTOR);
        dataConnector.setRows(7);
        dataConnector.setMatrixRelation("relevance_weight_matrix");
        dataConnector.setRowMappingRelation("relevance_row_mapping");
        dataConnector.setDisplayName(CONNECTOR_DISP);

        utility = new Utility(this, UTILITY_ACTOR);
        utility.setDisplayName(UTILITY_ACTOR_DISP);

        relevance = new Relevance(this, RELEVANCE_ACTOR);
        relevance.setDisplayName(RELEVANCE_ACTOR_DISP);
        valence = new Valence(this, VALENCE_ACTOR);
        valence.setDisplayName(VALENCE_ACTOR_DISP);

        connect(featuresInput, dataConnector.featuresInput);
        connect(dataConnector.featureToActionOutput, utility.featureToActionInput);
        connect(dataConnector.actionOutput, utility.actionInput);
        connect(dataConnector.actionToGoalOutput, utility.actionToGoalInput);
        connect(dataConnector.goalOutput, utility.goalInput);

        ComponentRelation maxUtilityActionConn = connect(utility.maxUtilityActionOutput, valence.maxUtilityActionInput);
        connect(affordancesInput, valence.affordancesInput);
        connect(utility.transitionsOutput, valence.transitionsInput);
        connect(valence.output, valenceOutput);
        connect(valence.outputAggregate, valenceOutputTotal);

        connect(ethicsInput, relevance.ethicsInput);
        connect(utility.actionTendenciesOutput, relevance.actionTendenciesInput);
        connect(dataConnector.rowMappingOutput, relevance.rowMappingInput);
        connect(dataConnector.weightMatrixOutput, relevance.weightMatrixInput);
        connect(relevance.output, relevanceOutput);
        connect(relevance.outputAggregate, relevanceOutputTotal);

        maxUtilityActionsOutput.link(maxUtilityActionConn);
    }
}
