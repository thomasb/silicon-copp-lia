package org.bitbucket.marrs.sico.actor.lib.respond;

import static org.bitbucket.marrs.sico.actor.lib.base.DomainPort.AESTHETICS;
import static org.bitbucket.marrs.sico.actor.lib.base.DomainPort.AFFORDANCES;
import static org.bitbucket.marrs.sico.actor.lib.base.DomainPort.EPISTEMICS;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordInputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordOutputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createTypedOutputPort;
import static org.bitbucket.marrs.sico.data.type.FuzzyWeightType.FUZZY_WEIGHT;

import org.bitbucket.marrs.sico.actor.lib.connect.LinearModelDataConnector;
import org.bitbucket.marrs.sico.actor.lib.connect.WeightDataConnector;
import org.bitbucket.marrs.sico.data.type.FuzzyWeightType;

import ptolemy.actor.TypedCompositeActor;
import ptolemy.actor.TypedIOPort;
import ptolemy.actor.lib.Discard;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.ComponentRelation;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

/**
 * Actor that evaluates the engagement of the agent.
 * <p>
 * The actor calculates the involvement and distance of the agent as indicative
 * and counter-indicative engagement values based on the relevance, valence,
 * similarity values as well as the encoded values from the affordances,
 * aesthetics and epistemics domain.  From the involvement and distance a
 * tradeoff value is calculated.
 */
public final class Engagement extends TypedCompositeActor {

    public static final int MODEL_DIM = 26;

    private static final String FEATURES_IN = "featuresInput";
    private static final String FEATURES_IN_DISP = "Features";
    private static final String RELEVANCE_IN = "relevanceInput";
    private static final String RELEVANCE_IN_DISP = "Relevance";
    private static final String VALENCE_IN = "valenceInput";
    private static final String VALENCE_IN_DISP = "Valence";
    private static final String SIMILARITY_IN = "similarityInput";
    private static final String SIMILARITY_IN_DISP = "Similarity";

    private static final String ENGAGEMENT_OUT = "engagementOutput";
    private static final String ENGAGEMENT_OUT_DISP = "Engagement";
    private static final String ENGAGEMENT_OUT_TOTAL = "engagementOutputTotal";
    private static final String ENGAGEMENT_OUT_TOTAL_DISP = "Engagement total";

    private static final String TRADEOFF_OUT = "tradeoffOutput";
    private static final String TRADEOFF_OUT_DISP = "Tradeoff";
    private static final String TRADEOFF_OUT_TOTAL = "tradeoffOutputTotal";
    private static final String TRADEOFF_OUT_TOTAL_DISP = "Tradeoff total";

    private static final String FEATURE_SINK = "featureSink";
    private static final String FEATURE_SINK_DISP = "Discard";
    private static final String ENGAGEMENT_CONNECTOR = "engagementConnector";
    private static final String ENGAGEMENT_CONNECTOR_DISP = "Epistemics Connector";
    private static final String IDT_CONNECTOR = "idtConnector";
    private static final String IDT_CONNECTOR_DISP = "Epistemics Connector";
    private static final String ENGAGEMENT_ACTOR = "engagement";
    private static final String ENGAGEMENT_ACTOR_DISP = "Engagement";
    private static final String IDT_ACTOR = "tradeoff";
    private static final String IDT_ACTOR_DISP = "Tradeoff";

    private static final String WEIGHT_MATRIX_PROPERTY = "engagement_weight_matrix";
    private static final String ROW_MAPPING_PROPERTY = "engagement_row_mapping";
    private static final String IDT_WEIGHT_PROPERTY = "idt_weight";

    /**
     * Input port for the features. The port is of type {@link BaseType#RECORD}
     * with values of type {@link BaseType#DOUBLE}.
     */
    public TypedIOPort featuresInput;

    /**
     * Input port for the encoded values of the features in the aesthetics
     * domain. The input port is of type {@link BaseType#RECORD} with values of
     * type {@link FuzzyWeightType} containing the encoded weights of the
     * aesthetics domains.
     */
    public TypedIOPort aestheticsInput;

    /**
     * Input port for the encoded values of the features in the epistemics
     * domain. The input port is of type {@link BaseType#RECORD} with values of
     * type {@link FuzzyWeightType} containing the encoded weights of the
     * epistemics domains.
     */
    public TypedIOPort epistemicsInput;

    /**
     * Input port for the encoded values of the features in the affordances
     * domain. The input port is of type {@link BaseType#RECORD} with values of
     * type {@link FuzzyWeightType} containing the encoded weights of the
     * affordances domains.
     */
    public TypedIOPort affordancesInput;

    /**
     * Input port for the relevance values of the features. The input port is
     * of type {@link BaseType#RECORD} with values of type
     * {@link FuzzyWeightType}.
     */
    public TypedIOPort relevanceInput;

    /**
     * Input port for the valence values of the features. The input port is
     * of type {@link BaseType#RECORD} with values of type
     * {@link FuzzyWeightType}.
     */
    public TypedIOPort valenceInput;

    /**
     * Input port for the similarity values of the features. The input port is
     * of type {@link BaseType#RECORD} with values of type
     * {@link FuzzyWeightType}.
     */
    public TypedIOPort similarityInput;

    /**
     * Output port for the engagement values. The output is of type
     * {@link BaseType#RECORD} with values of type {@link FuzzyWeightType}. The
     * indicative values represent involvement, the counter-indicative values
     * represent distance.
     */
    public TypedIOPort engagementOutput;

    /**
     * Output port for the aggregate engagement value. The output is of type
     * {@link FuzzyWeightType}. The indicative values represent involvement, the
     * counter-indicative values represent distance.
     */
    public TypedIOPort engagementOutputTotal;

    /**
     * Output port for the involvement-distance tradeoff values. The output is
     * of type {@link BaseType#RECORD} with values of type
     * {@link BaseType#DOUBLE}.
     */
    public TypedIOPort tradeoffOutput;

    /**
     * Output port for the aggregate involvement-distance tradeoff value. The
     * output is of type {@link BaseType#DOUBLE}.
     */
    public TypedIOPort tradeoffOutputTotal;

    public Discard featureSink;
    public LinearModelDataConnector engagementConnector;
    public WeightDataConnector idtConnector;
    public LinearModelEngagement engagement;
    public IDT tradeoff;

    public Engagement(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace);
        init();
    }

    public Engagement(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        super(container, name);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        featuresInput = createRecordInputPort(this, FEATURES_IN);
        featuresInput.setDisplayName(FEATURES_IN_DISP);

        aestheticsInput = createRecordInputPort(this, AESTHETICS.inputName);
        aestheticsInput.setDisplayName(AESTHETICS.inputDisplayName);
        epistemicsInput = createRecordInputPort(this, EPISTEMICS.inputName);
        epistemicsInput.setDisplayName(EPISTEMICS.inputDisplayName);
        affordancesInput = createRecordInputPort(this, AFFORDANCES.inputName);
        affordancesInput.setDisplayName(AFFORDANCES.inputDisplayName);

        relevanceInput = createRecordInputPort(this, RELEVANCE_IN);
        relevanceInput.setDisplayName(RELEVANCE_IN_DISP);
        valenceInput = createRecordInputPort(this, VALENCE_IN);
        valenceInput.setDisplayName(VALENCE_IN_DISP);
        similarityInput = createRecordInputPort(this, SIMILARITY_IN);
        similarityInput.setDisplayName(SIMILARITY_IN_DISP);

        engagementOutput = createRecordOutputPort(this, ENGAGEMENT_OUT);
        engagementOutput.setDisplayName(ENGAGEMENT_OUT_DISP);
        engagementOutputTotal = createTypedOutputPort(this, ENGAGEMENT_OUT_TOTAL, FUZZY_WEIGHT);
        engagementOutputTotal.setDisplayName(ENGAGEMENT_OUT_TOTAL_DISP);

        tradeoffOutput = createRecordOutputPort(this, TRADEOFF_OUT);
        tradeoffOutput.setDisplayName(TRADEOFF_OUT_DISP);
        tradeoffOutputTotal = createTypedOutputPort(this, TRADEOFF_OUT_TOTAL, BaseType.DOUBLE);
        tradeoffOutputTotal.setDisplayName(TRADEOFF_OUT_TOTAL_DISP);

        featureSink = new Discard(this, FEATURE_SINK);
        featureSink.setDisplayName(FEATURE_SINK_DISP);

        engagementConnector = new LinearModelDataConnector(this, ENGAGEMENT_CONNECTOR);
        engagementConnector.setDisplayName(ENGAGEMENT_CONNECTOR_DISP);
        engagementConnector.setRows(MODEL_DIM);
        engagementConnector.setMatrixRelation(WEIGHT_MATRIX_PROPERTY);
        engagementConnector.setRowMappingRelation(ROW_MAPPING_PROPERTY);
        engagement = new LinearModelEngagement(this, ENGAGEMENT_ACTOR);
        engagement.setDisplayName(ENGAGEMENT_ACTOR_DISP);

        idtConnector = new WeightDataConnector(this, IDT_CONNECTOR);
        idtConnector.setDisplayName(IDT_CONNECTOR_DISP);
        idtConnector.setWeightRelation(IDT_WEIGHT_PROPERTY);
        tradeoff = new IDT(this, IDT_ACTOR);
        tradeoff.setDisplayName(IDT_ACTOR_DISP);

        connect(featuresInput, featureSink.input);

        connect(affordancesInput, engagement.affordancesInput);
        connect(aestheticsInput, engagement.aestheticsInput);
        connect(epistemicsInput, engagement.epistemicsInput);
        connect(relevanceInput, engagement.relevanceInput);
        connect(valenceInput, engagement.valenceInput);
        connect(similarityInput, engagement.similarityInput);

        connect(idtConnector.weightOutput, tradeoff.weightInput);
        connect(engagementConnector.weightMatrixOutput, engagement.weightMatrixInput);
        connect(engagementConnector.rowMappingOutput, engagement.rowMappingInput);

        ComponentRelation connect = connect(engagement.output, tradeoff.getEngagementInput());
        engagementOutput.link(connect);

        connect(engagement.outputAggregate, engagementOutputTotal);
        connect(tradeoff.getTradeoffOutput(), tradeoffOutput);
        connect(tradeoff.getTradeoffOutputTotal(), tradeoffOutputTotal);
    }
}
