package org.bitbucket.marrs.sico.actor.lib.base;

import static org.bitbucket.marrs.sico.actor.lib.util.FuzzyWeightCalculator.calculateAverage;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordInputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createTypedInputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.RecordTokenConverter.intMapConverter;
import static org.bitbucket.marrs.sico.data.type.FuzzyWeightType.FUZZY_WEIGHT;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.bitbucket.marrs.sico.data.token.FuzzyWeightToken;

import ptolemy.actor.TypedIOPort;
import ptolemy.data.DoubleMatrixToken;
import ptolemy.data.RecordToken;
import ptolemy.data.Token;
import ptolemy.data.type.BaseType;
import ptolemy.data.type.Type;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

/**
 * This is a base class for actors that transform input ports by a linear
 * transformation based on a given weight matrix.
*/
// This class is not abstract in order to support the documentation system.
public class LinearModel extends MultiRecordTransformation<FuzzyWeightToken> {

    private static final String WEIGHT_MATRIX_INPUT = "weightMatrixInput";
    private static final String WEIGHT_MATRIX_INPUT_DISP = "Weight Matrix";

    private static final String ROW_MAPPING_INPUT = "rowMappingInput";
    private static final String ROW_MAPPING_INPUT_DISP = "Row Mapping";

    /**
     * Input port for the weight matrix of the linear model. The input port is
     * of type {@link BaseType#DOUBLE_MATRIX}. The parameter mapping for the
     * row indices of the matrix is provided separately on the
     * {@link #rowMappingInput} port.
     */
    public TypedIOPort weightMatrixInput;

    /**
     * Input port for the mapping of the row indexes of the weight matrix
     * provided on the {@link #weightMatrixInput} port to the parameter names
     * in the linear model. The input port is
     * of type {@link BaseType#RECORD} where the keys represent the parameter
     * names and the values are the row indices in the weight matrix.
     */
    public TypedIOPort rowMappingInput;

    private int dimension;

    /**
     * Constructor for internal use (cloning and documentation system).
     *
     * @param workspace the workspace
     *
     * @throws IllegalActionException if the actor cannot be contained
     *  by the proposed workspace
     * @throws NameDuplicationException if there is a naming conflict
     */
    public LinearModel(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        this(workspace, Collections.emptyList(), Collections.emptyList(), 0);
    }

    /**
     * Constructor for internal use (cloning and documentation system).
     *
     * @param workspace the workspace
     * @param inputNames the names of the input ports
     * @param valueType the type of the values in the records in the input
     *  ports
     * @param dimension the expected dimension (number of rows) of the linear
     *  model
     *
     * @throws IllegalActionException if the actor cannot be contained
     *  by the proposed workspace
     * @throws NameDuplicationException if there is a naming conflict
     */
    protected LinearModel(Workspace workspace,
            List<String> inputNames,
            Type valueType,
            int dimension) throws IllegalActionException, NameDuplicationException {
        this(workspace, inputNames, Collections.nCopies(inputNames.size(), valueType), dimension);
    }

    /**
     * Constructor for internal use (cloning and documentation system).
     *
     * @param workspace the workspace
     * @param inputNames the names of the input ports
     * @param valueTypes the types of the values in the records in the input
     *  ports in the order they are defined in the inputNames argument
     * @param dimension the expected dimension (number of rows) of the linear
     *  model
     *
     * @throws IllegalActionException if the actor cannot be contained
     *  by the proposed workspace
     * @throws NameDuplicationException if there is a naming conflict
     */
    protected LinearModel(Workspace workspace,
            List<String> inputNames,
            List<Type> valueTypes,
            int dimension) throws IllegalActionException, NameDuplicationException {
        super(workspace, inputNames, FUZZY_WEIGHT, valueTypes);

        this.dimension = dimension;

        init();
    }

    /**
     * Construct an actor with the given container name.
     * <p>
     * The created {@code LinearModel} has no input ports.
     *
     * @param container the container
     * @param name the name of this actor
     *
     * @throws IllegalActionException if the actor cannot be contained
     *  by the proposed container
     * @throws NameDuplicationException if there is a naming conflict
     */
    public LinearModel(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        this(container, name, Collections.emptyList(), Collections.emptyList(), 0);
    }

    /**
     * Construct an actor with the given container name, port names and types.
     * <p>
     * The order of the port names defines the index of the input ports in the
     * {@link #getInput(int)} method.
     *
     * @param container the container
     * @param name the name of the actor
     * @param inputNames the names of the input ports
     * @param valueType the type of the values in the records in the input
     *  ports
     * @param dimension the expected dimension (number of rows) of the linear
     *  model
     *
     * @throws IllegalActionException if the actor cannot be contained
     *  by the proposed workspace
     * @throws NameDuplicationException if there is a naming conflict
     */
    protected LinearModel(CompositeEntity container,
            String name,
            List<String> inputNames,
            Type valueType,
            int dimension) throws IllegalActionException, NameDuplicationException {
        this(container, name, inputNames, Collections.nCopies(inputNames.size(), valueType), dimension);
    }

    /**
     * Construct an actor with the given container name, port names and types.
     * <p>
     * The order of the port names defines the index of the input ports in the
     * {@link #getInput(int)} method.
     *
     * @param container the container
     * @param name the name of the actor
     * @param inputNames the names of the input ports
     * @param valueTypes the types of the values in the records in the input
     *  ports in the order they are defined in the inputNames argument
     * @param dimension the expected dimension (number of rows) of the linear
     *  model
     *
     * @throws IllegalActionException if the actor cannot be contained
     *  by the proposed workspace
     * @throws NameDuplicationException if there is a naming conflict
     */
    protected LinearModel(CompositeEntity container,
            String name,
            List<String> inputNames,
            List<Type> valueTypes,
            int dimension) throws IllegalActionException, NameDuplicationException {
        super(container, name, inputNames, FUZZY_WEIGHT, valueTypes);

        this.dimension = dimension;

        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        weightMatrixInput = createTypedInputPort(this, WEIGHT_MATRIX_INPUT, BaseType.DOUBLE_MATRIX);
        weightMatrixInput.setDisplayName(WEIGHT_MATRIX_INPUT_DISP);

        rowMappingInput = createRecordInputPort(this, ROW_MAPPING_INPUT);
        rowMappingInput.setDisplayName(ROW_MAPPING_INPUT_DISP);
    }

    @Override
    protected Transformation<Token, FuzzyWeightToken> getTransformation() throws IllegalActionException {
        Map<String, Integer> rowMapping = intMapConverter().convert((RecordToken) rowMappingInput.get(0));

        DoubleMatrixToken weightMatrixToken = DoubleMatrixToken.convert(weightMatrixInput.get(0));
        RealMatrix weightMatrix = new Array2DRowRealMatrix(weightMatrixToken.doubleMatrix(), false);

        checkDimensions(weightMatrix, rowMapping);

        return (label, args) -> {
                double[] orderedArguments = parseArgumentsOrdered(args, rowMapping);
                RealVector argumentVector = new ArrayRealVector(orderedArguments, false);

                RealVector result = weightMatrix.preMultiply(argumentVector);

                return new FuzzyWeightToken(result.getEntry(0), result.getEntry(1));
        };
    }

    /**
     * Convert the argument Tokens in the provided {@code List} to double
     * values in the order provided by the rowMapping.
     *
     * @param args list of Tokens to be parsed
     * @param rowMapping the mapping from the port name to the index in the
     *  output array
     *
     * @return an array of double that contains the parsed argument values
     *
     * @throws IllegalActionException if an error appears during parsing of
     *  the argument tokens
     */
    protected double[] parseArgumentsOrdered(List<Token> args, Map<String, Integer> rowMapping) throws IllegalActionException {
        // This class is not abstract in order to support the documentation system.
        throw new UnsupportedOperationException("Override with custom implementation");
    }

    private void checkDimensions(RealMatrix weightMatrix, Map<String, Integer> rowMapping) throws IllegalActionException {
        if (weightMatrix.getColumnDimension() != 2) {
            throw new IllegalActionException(this, "Weight matrix must have 2 columns, was: " + weightMatrix.getColumnDimension());
        }

        if (weightMatrix.getRowDimension() != dimension) {
            throw new IllegalActionException(this, "Weight matrix must have " + dimension + " rows, was: " + weightMatrix.getRowDimension());
        }

        if (rowMapping.size() != weightMatrix.getRowDimension()) {
            throw new IllegalActionException(this, "Row mapping does not match matrix row dimensions [" + weightMatrix.getRowDimension() + "]: " + rowMapping);
        }
    }

    @Override
    protected Token aggregateOutput(Collection<FuzzyWeightToken> outputs) throws IllegalActionException {
        return calculateAverage(outputs);
    }

    @Override
    public Object clone(Workspace workspace) throws CloneNotSupportedException {
        LinearModel clone = (LinearModel) super.clone(workspace);
        clone.dimension = dimension;

        return clone;
    }

    protected final int getChecked(Map<String, Integer> rowMapping, String key) throws IllegalActionException {
        Integer reference = rowMapping.get(key);
        if (reference == null) {
            throw new IllegalActionException(this, "Key '" + key + "' is not present in the row mapping");
        }

        return reference;
    }
}
